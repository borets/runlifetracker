INSERT INTO fields VALUES (1, 'Castilla', 'Colombia', 'CA:CN');
INSERT INTO fields VALUES (2, 'Chichimene', 'Colombia', 'CH:CHSW');
INSERT INTO fields VALUES (3, 'Apiay', 'Colombia', 'AP');
SELECT setval('fields_id_seq', max(id)) FROM fields;

INSERT INTO wells VALUES (1, 'AP-031', 3);
INSERT INTO wells VALUES (2, 'AP-077', 3);
INSERT INTO wells VALUES (3, 'CHSW-044', 2);
INSERT INTO wells VALUES (4, 'CN-044', 1);
SELECT setval('wells_id_seq', max(id)) FROM wells;

INSERT INTO clients VALUES (1, 'Ecopetrol', 'Colombia', '123456789-10');
SELECT setval('clients_id_seq', max(id)) FROM clients;

INSERT INTO contracts VALUES (1, 1, 'SOA', '2015-04-08', 'Direct/Indirect', FALSE);
INSERT INTO contracts VALUES (2, 1, 'SCC', '2014-06-09', 'Direct/Indirect', FALSE);
SELECT setval('contracts_id_seq', max(id)) FROM contracts;

INSERT INTO users values (1, 'Pedro Juan', 'Alvarez Sanchez', 'admin.alvarez@borets.com', 0, '123', FALSE);
INSERT INTO users values (2, 'Pedro Juan', 'Alvarez Sanchez', 'pedro.alvarez@borets.com', 2, '123', FALSE);
INSERT INTO users values (3, 'Pedro Analyst', 'Alvarez Sanchez', 'analyst.alvarez@borets.com', 1, '123', FALSE);
SELECT setval('users_id_seq', max(id)) FROM users;

INSERT INTO usersfields values (2, 1);
INSERT INTO usersfields values (2, 3); 

INSERT INTO contractsfields values (1, 1); 
INSERT INTO contractsfields values (2, 2);

INSERT INTO installations VALUES (1,3,2,'ESP','2015-07-31','Installation note',TRUE,
									'538','CS','BRT-NL',187,'TA','Mixed',3,'S/N BCP0123, BCP0345',
									'562','SS','BRT','Induction','330','2375','88',2,'S/N M2015088455, BCM0234',
									'GRC','New','6','G123456','Sensor note',
									'ZTS','Cable note',8800,'New','2','G',TRUE,
									'2015-07-31 00:00:00',NULL,'Initial upload','Initial upload');
SELECT setval('installations_id_seq', max(id)) FROM installations;

INSERT INTO lcevents VALUES ('LcEvent',1,1,'2015-08-15',0,'Start event',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
SELECT setval('lcevents_id_seq', max(id)) FROM lcevents;
