DROP TABLE IF EXISTS installations CASCADE;
CREATE TABLE installations (
  id         			SERIAL PRIMARY KEY,
  well_id    			INTEGER NOT NULL,
  contract_id			INTEGER NOT NULL,
  lifttype	 			VARCHAR(32) NOT NULL,
  instdate	 			DATE NOT NULL,
  note					VARCHAR(200),
  isfailure				BOOLEAN,
  
  pumpsseries			VARCHAR(10),
  pumpsmetallurgy		VARCHAR(10),
  pumpsbrand			VARCHAR(15),
  pumpsstagesq			INTEGER,
  pumpsabrasionres		VARCHAR(10),
  pumpsflowtype			VARCHAR(10),
  pumpsqty				INTEGER,
  pumpsnote				VARCHAR(200),
  
  
  motorsseries			VARCHAR(10),
  motorsmetallurgy		VARCHAR(10),
  motorsbrand			VARCHAR(15),
  motorstype			VARCHAR(10),
  motorshp				DECIMAL,
  motorsvolts			DECIMAL,
  motorsamps			DECIMAL,
  motorsqty				INTEGER,
  motorsnote			VARCHAR(200),
  
  sensorbrand			VARCHAR(15), 
  sensorreuse			VARCHAR(15),
  sensorvariables		VARCHAR(15),
  sensorsn				VARCHAR(15),
  sensornote			VARCHAR(200),
  
  cablebrand			VARCHAR(20),
  cablenote				VARCHAR(200),
  cablelength			INTEGER,
  cablereuse			VARCHAR(20),
  cablexarea			VARCHAR(20),
  cablearmour			VARCHAR(20),
  cableisconcap			BOOLEAN,
  
  createddate 			TIMESTAMP NOT NULL,
  updateddate			TIMESTAMP NOT NULL,
  createdby				VARCHAR(50) NOT NULL,
  updatedby				VARCHAR(50) NOT NULL
  
);
ALTER TABLE installations ADD CONSTRAINT fk_installations_well FOREIGN KEY (well_id) REFERENCES wells (id);
ALTER TABLE installations ADD CONSTRAINT fk_installations_contract FOREIGN KEY (contract_id) REFERENCES contracts (id);


DROP TABLE IF EXISTS lcevents CASCADE;
CREATE TABLE lcevents (
	eventclass			VARCHAR(20),
	id					SERIAL PRIMARY KEY,
	installation_id		INTEGER NOT NULL,
	eventdate			DATE NOT NULL,
	eventtype			VARCHAR(20) NOT NULL,
	eventnote			VARCHAR(200),
	createddate 		TIMESTAMP NOT NULL,
	updateddate			TIMESTAMP NOT NULL,
	createdby			VARCHAR(50) NOT NULL,
	updatedby			VARCHAR(50) NOT NULL,
	pullreason			INTEGER,
	rca_rootcause		VARCHAR(20),
	rca_fcomponent		VARCHAR(20),
	client_class		VARCHAR(20)
);
ALTER TABLE lcevents ADD CONSTRAINT fk_lcevents_installation FOREIGN KEY (installation_id) REFERENCES installations (id);