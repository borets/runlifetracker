package ru.shaposhnik.runlifetracker.web.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import ru.shaposhnik.runlifetracker.model.FieldTechnician;
import ru.shaposhnik.runlifetracker.repository.FieldTechnicianRepository;

public class FieldTechnicianFormatter implements Formatter<FieldTechnician> {

    @Autowired
    private FieldTechnicianRepository fieldTechnicianRepository;


    public FieldTechnicianFormatter() {
        super();
    }
    @Override
    public FieldTechnician parse(final String text, final Locale locale) throws ParseException {
    	System.out.println("FieldTechnicianFormatter from string fired: "+text);
        final int technician_id = Integer.valueOf(text);
        return this.fieldTechnicianRepository.findFieldTechnicianById(technician_id);
    }

    @Override
    public String print(final FieldTechnician technician, final Locale locale) {
    	System.out.println("FieldTechnicianFormatter to string fired: "+technician.toString()+"(id: "+technician.getId()+")");
        return (technician != null ? String.valueOf(technician.getId()):"");
    }

}
