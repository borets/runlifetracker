/*
 * =============================================================================
 * 
 *   Copyright (c) 2011-2014, The THYMELEAF team (http://www.thymeleaf.org)
 * 
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * 
 * =============================================================================
 */
package ru.shaposhnik.runlifetracker.web.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import ru.shaposhnik.runlifetracker.model.Installation;
import ru.shaposhnik.runlifetracker.repository.InstallationRepository;

public class InstallationFormatter implements Formatter<Installation> {

    @Autowired
    private InstallationRepository installationRepository;


    public InstallationFormatter() {
        super();
    }
    @Override
    public Installation parse(final String text, final Locale locale) throws ParseException {
        final int installation_id = Integer.valueOf(text);
        return this.installationRepository.findInstallationById(installation_id);
    }

    @Override
    public String print(final Installation installation, final Locale locale) {
        return (installation != null ? String.valueOf(installation.getId()):"0");
    }

}
