package ru.shaposhnik.runlifetracker.web.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import ru.shaposhnik.runlifetracker.model.Well;
import ru.shaposhnik.runlifetracker.repository.WellRepository;

public class WellFormatter implements Formatter<Well> {

    @Autowired
    private WellRepository wellRepository;


    public WellFormatter() {
        super();
    }
    @Override
    public Well parse(final String text, final Locale locale) throws ParseException {
    	System.out.println("WellFormatter from	 string fired: "+text);
        final int well_id = Integer.valueOf(text);
        return this.wellRepository.findWellById(well_id);
    }

    @Override
    public String print(final Well well, final Locale locale) {
    	System.out.println("WellFormatter to string fired: "+well.toString());
        return (well != null ? String.valueOf(well.getId()):"");
    }

}
