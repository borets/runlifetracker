/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.repository.FieldRepository;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;

// http://fruzenshtein.com/spring-mvc-validator-initbinder/

@Component
public class FieldValidator implements Validator {

	@Override
    public boolean supports(Class<?> clazz) {
        return Field.class.isAssignableFrom(clazz);
    }
	
	private final FieldRepository fieldRepository;
	
	@Autowired
	FieldValidator(FieldRepository fr){
		this.fieldRepository=fr;
	}
	
	@Override
    public void validate(Object obj, Errors errors) {
    	
    	Field field = (Field) obj;
        String name = field.getName();
        if (!StringUtils.hasLength(name)) {
            errors.rejectValue("name", "name", "Field name cannot be empty!");
        }
        String country=field.getCountry();
        if (!StringUtils.hasLength(country)) {
            errors.rejectValue("country", "country", "Country cannot be empty!");
        }
        
        Field existingField=fieldRepository.findFieldByCountryAndName(field.getCountry(), field.getName());
        if (existingField==null){
        	return;
        }
        if (existingField.getId()!=field.getId()){
        	errors.reject("duplication", "Such field already exists!");
      
        }
    }

}
