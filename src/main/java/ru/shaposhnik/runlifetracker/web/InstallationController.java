/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import ru.shaposhnik.runlifetracker.exceptions.UnauthorizedException;
import ru.shaposhnik.runlifetracker.model.PSComponentCable;
import ru.shaposhnik.runlifetracker.model.PSComponentDHE;
import ru.shaposhnik.runlifetracker.model.PSComponentMotors;
import ru.shaposhnik.runlifetracker.model.PSComponentPumps;
import ru.shaposhnik.runlifetracker.model.SavedFile;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.Well;
import ru.shaposhnik.runlifetracker.model.Installation;
import ru.shaposhnik.runlifetracker.model.LcEvent;
import ru.shaposhnik.runlifetracker.model.CommonInfoService;
import ru.shaposhnik.runlifetracker.repository.InstallationRepository;
import ru.shaposhnik.runlifetracker.repository.LcEventRepository;
import ru.shaposhnik.runlifetracker.repository.WellRepository;
import ru.shaposhnik.runlifetracker.repository.FieldTechnicianRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class InstallationController {

	private final InstallationRepository installationRepository;
    private final WellRepository wellRepository;
    private final LcEventRepository lcEventRepository;
    private final FieldTechnicianRepository fieldTechnicianRepository;
    

    @Autowired
    private InstallationValidator validator;

    @Autowired
    public InstallationController(InstallationRepository ir, WellRepository wr, LcEventRepository lr, FieldTechnicianRepository tr) {
        this.installationRepository=ir;
        this.wellRepository=wr;
        this.lcEventRepository=lr;
        this.fieldTechnicianRepository=tr;
           
    }

    
    
    @ModelAttribute
    public Model prepareData(Model model){
    	
    	model.addAttribute("liftTypes",CommonInfoService.getLiftTypeList());
    	
    	model.addAttribute("xareas", PSComponentCable.xareas);
    	model.addAttribute("armours", PSComponentCable.armours);
    	model.addAttribute("reuses", PSComponentCable.reuses);
    	model.addAttribute("cablebrands", PSComponentCable.brands);
    	
    	model.addAttribute("motorbrands", PSComponentMotors.brands);
    	model.addAttribute("motorseries", PSComponentMotors.seriess);
    	model.addAttribute("motortypes", PSComponentMotors.motorTypes);
    	model.addAttribute("motormetallurgies", PSComponentDHE.metallurgies);
    	
    	model.addAttribute("pumpbrands", PSComponentPumps.brands);
    	model.addAttribute("pumpseries", PSComponentPumps.seriess);
    	model.addAttribute("flowtypes", PSComponentPumps.flowTypes);
    	model.addAttribute("pumpmetallurgies", PSComponentPumps.metallurgies);
    	model.addAttribute("pumpabrrestypes", PSComponentPumps.abrasionResTypes);
    	
      	return model;
    }
    
    
    
    @RequestMapping(value = "/installations/{installation_id}/edit", method = RequestMethod.GET)
    public String initUpdateForm(Principal principal, @PathVariable int installation_id, Model model) 
    	throws UnauthorizedException {
    		
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	Installation installation=this.installationRepository.findInstallationById(user, installation_id);
    	model.addAttribute("installation", installation);
    	
    	
    	model.addAttribute("fieldTechnicians", this.fieldTechnicianRepository.query4Country(installation.getWell().getField().getCountry()));

    	List<SavedFile> savedFiles=this.installationRepository.savedFiles(installation);
    	if (!savedFiles.isEmpty())
    		model.addAttribute("ireport",savedFiles.iterator().next());
 //   		System.out.println("lkj");
    	else
    		model.addAttribute("file",null);
        return "installation-edit";
        
    }
    
    
    @RequestMapping(value = "/wells/{well_id}/newinstallation", method = RequestMethod.GET)
    public String initNewForm(@PathVariable int well_id, Model model) {
    	
    	Installation installation=new Installation();
    	Well well=this.wellRepository.findWellById(well_id);
    	installation.setWell(well);
    	model.addAttribute("installation", installation);
    	
    	model.addAttribute("fieldTechnicians", this.fieldTechnicianRepository.query4Country(well.getField().getCountry()));
    	
        return "installation-edit";
        
    }
    
    
    
    
    @RequestMapping(value = "/installations", params = {"save"}, method = RequestMethod.POST)
    public String updateInstallation(Principal principal, @ModelAttribute("installation") @Valid Installation installation, final BindingResult bindingResult, Model model)
    		throws UnauthorizedException {
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	if (bindingResult.hasErrors()) {
    		
    		if (installation.getId() > 0){
    			installation.SetLcEvents(new HashSet<LcEvent>(lcEventRepository.findLcEvent4Installation(installation, user)));
    		}
    		model.addAttribute("installation", installation);

    		model.addAttribute("fieldTechnicians", this.fieldTechnicianRepository.query4Country(installation.getWell().getField().getCountry()));    		
    		
    		return "installation-edit";
        } else {
	    	installation = installationRepository.saveInstallation(installation, user);
        }
    	return "redirect:installations/"+installation.getId()+"/edit";
    }
    
    
    @RequestMapping(value = "/installations", params = {"delete"}, method = RequestMethod.POST)
    public String deleteInstallation(Principal principal, @ModelAttribute("installation") @Valid Installation installation, final BindingResult bindingResult, Model model)
    	throws UnauthorizedException {
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	installationRepository.deleteInstallation(installation, user);
     	    	
    	return "redirect:/installations/";
    }
    	
	
    @InitBinder("installation")
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.addValidators(validator);
    }
    
    
    
       
}