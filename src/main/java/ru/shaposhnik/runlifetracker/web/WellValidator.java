/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import ru.shaposhnik.runlifetracker.model.Well;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.repository.WellRepository;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;

// http://fruzenshtein.com/spring-mvc-validator-initbinder/

@Component
public class WellValidator implements Validator {

	@Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }
	
	private final WellRepository wellRepository;
	
	@Autowired
	WellValidator(WellRepository wr){
		this.wellRepository=wr;
	}
	
	@Override
    public void validate(Object obj, Errors errors) {
    	
    	Well well = (Well) obj; 
        
    	//check Field existense
  //  	if (well==null || fieldRepository.findFieldById(well.getField().getId()) == null){
  //  		errors.rejectValue("field", "field.notFound", "The field is not found");
  //  	}
    	
    	
    	Well existingWell =wellRepository.findWellById(well.getId());
    	if (existingWell== null){
    		return;
    	}
        if (existingWell.getId()!= well.getId()){
        	errors.rejectValue("email", "email.duplication", "User with this email already exists!");  
        }
  
    }


	
}
