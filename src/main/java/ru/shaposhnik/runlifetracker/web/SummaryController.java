package ru.shaposhnik.runlifetracker.web;

import java.security.Principal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import ru.shaposhnik.runlifetracker.model.LcEventTypesEnum;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.UserRolesEnum;
import ru.shaposhnik.runlifetracker.repository.InstallationRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
public class SummaryController {

    private final InstallationRepository installationRepository;

    @PersistenceContext
	private EntityManager em;
    
    
    @Autowired
    public SummaryController(InstallationRepository ir) {
        this.installationRepository = ir;

    }

    @RequestMapping(value = "/summary/init",method=RequestMethod.GET)
    public String summaryInit(Principal principal, Model model) {
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	List<String> allCountries = this.installationRepository.availableCountries(user);
    	
    	model.addAttribute("allCountries", allCountries);
    return "summary";
    }

    @RequestMapping(value = "/summary/csv",method=RequestMethod.GET)
    @ResponseBody
    public String summaryCsv(Principal principal,
							@RequestParam("eventType") LcEventTypesEnum eventType,
							@RequestParam("period") String period,
							@RequestParam("country") String country) {
    
	User user = (User) ((Authentication) principal).getPrincipal();
	
		
	String whereClause;
	if (user.getRole()!=UserRolesEnum.ADMIN)
		whereClause="w.field_id in (select uf.field_id from usersfields as uf where uf.user_id="+user.getId()+")";
	else
		whereClause="1=1";
		
	String period1="";
	switch (period){
		case "month":
			period1="1 month";
			break;
		case "quarter":
			period1="3 month";
			break;
		case "year":
			period1="1 year";
			break;
	
	}
	
	String queryCountryInstallationsStr="\n"+
			"select data.clientname, data.client_id, data.contractname, data.contract_id, data.contractdate, data.field_id, data.fieldname, to_char(periods.period,'YYYY-MM-DD'), count(data.period) \n" +
			"\n" + 
			"from\n" + 
			"\n" + 
			"	(\n" + 
			"	select  (date_trunc('"+period+"', current_date)-interval '"+period1+"' * a) AS period \n" + 
			"		FROM generate_series(0,11,1) AS a\n" + 
			"	) periods\n" + 
			"\n" + 
			"inner join\n" + 
			"\n" + 	
			"	(select i.id as inst_id, c.id as contract_id, cl.name as clientname, cl.id as client_id, c.name as contractname, c.startingdate as contractdate, f.id as field_id, f.name as fieldname, (date_trunc('"+period+"',i.instdate)) as period from\n" + 
			"	installations as i inner join\n" + 
			"	contracts as c on i.contract_id=c.id inner join\n" + 
			"	clients as cl on cl.id=c.client_id inner join\n" + 
			"	wells as w on w.id=i.well_id inner join\n" + 
			"	fields as f on f.id=w.field_id\n" + 
			"	where cl.country=:country and "+whereClause+") data\n" + 
			"\n" + 
			"on data.period=periods.period\n" + 
			"\n" + 
			"group by data.contract_id, data.clientname, data.client_id, data.contractname, data.contractdate, data.field_id, data.fieldname, periods.period\n" +
			"order by periods.period asc;";

	String queryCountryEventsStr="\n"+
			"select data.clientname, data.client_id, data.contractname, data.contract_id, data.contractdate, data.field_id, data.fieldname, to_char(periods.period,'YYYY-MM-DD'), count(data.period) \n"+
			"\n"+
			"from\n" + 
			"\n" + 
			"	(\n" + 
			"	select  (date_trunc('"+period+"', current_date)-interval '"+period1+"' * a) AS period \n" + 
			"		FROM generate_series(0,11,1) AS a\n" + 
			"	) periods\n" + 
			"\n" + 
			"inner join\n" + 
			" \n" + 
			"	(\n" + 
			"	select i.id as contract_id, i.clientname, i.client_id, i.contractname, i.startingdate as contractdate, l.period, l.id, i.field_id, i.fieldname from\n" + 
			"		(\n" + 
			"		\n" + 
			"		select i.id as inst_id, c.id, cl.name as clientname, cl.id as client_id, c.name as contractname, c.startingdate, f.id as field_id, f.name as fieldname from\n" + 
			"		installations as i inner join\n" + 
			"		contracts as c on i.contract_id=c.id inner join\n" + 
			"		clients as cl on cl.id=c.client_id inner join\n" + 
			"		wells as w on w.id=i.well_id inner join\n" +
			"		fields as f on f.id=w.field_id\n" +
			"		where cl.country=:country and "+whereClause+"\n" + 
			"		) i\n" + 
			"\n" + 
			"		inner join\n" + 
			"		\n" + 
			"		(\n" + 
			"		select (date_trunc('"+period+"',l.eventdate)) as period, l.installation_id, l.id from\n" + 
			"			lcevents as l where l.eventtype=:etype\n" + 
			"		) l on l.installation_id=i.inst_id\n" + 
			"		\n" + 
			"	) data\n" + 
			"\n" +
			"on data.period=periods.period\n" +
			"\n" + 
			"group by data.contract_id, data.clientname, data.client_id, data.contractname, data.contractdate, data.field_id, data.fieldname, periods.period\n" + 
			"order by periods.period asc;";
	
	
	Query queryCountry;
	
	if (eventType== null){
		//request about installations
		queryCountry=em.createNativeQuery(queryCountryInstallationsStr);
	} else {
		// request about events
		queryCountry=em.createNativeQuery(queryCountryEventsStr);
		queryCountry.setParameter("etype",eventType.ordinal());
	}
	queryCountry.setParameter("country",country);
	@SuppressWarnings("unchecked")
	List<String[]> results=queryCountry.getResultList();
	String out;
		
	out="ClientName,ClientID,ContractName,ContractID,ContractDate,FieldID,FieldName,Period,EventsQty\n";
	for (Object[] result : results){
		out=out+result[0]+","+result[1]+","+result[2]+","+result[3]+","+result[4]+","+result[5]+","+result[6]+","+result[7]+","+result[8]+"\n";			
	}

	return out;
	}    

}