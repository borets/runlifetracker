/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import ru.shaposhnik.runlifetracker.model.Contract;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.repository.ContractRepository;
import ru.shaposhnik.runlifetracker.repository.FieldRepository;
import ru.shaposhnik.runlifetracker.repository.UserRepository;
import ru.shaposhnik.runlifetracker.model.CommonInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class FieldController {

    private final FieldRepository fieldRepository;
    private final ContractRepository contractRepository;
    private final UserRepository userRepository;

    
    @PersistenceContext
	private EntityManager em;

    @Autowired
    public FieldController(FieldRepository fr, ContractRepository cr, UserRepository ur) {
        this.fieldRepository = fr;
        this.contractRepository=cr;
        this.userRepository=ur;
    }


    //@Autowired
    //private ConversionService conversionService;
    
    @Autowired
    private FieldValidator fieldValidator;
    
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
 //        binder.setConversionService(conversionService);
    	binder.setValidator(fieldValidator);
  //  	binder.registerCustomEditor(String.class,"name",  propertyEditor);
  //  String s = new String("dfdf");
  //  s.t
    }

       
    
    @ModelAttribute
    public Model prepareData(Model model){
    	
    	List<String> allCountries=CommonInfoService.getCountryList();
      	model.addAttribute("allCountries",allCountries);
    	
      	List<Field> allFields=this.fieldRepository.queryAll();
      	model.addAttribute("allFields",allFields);
    	
      	return model;
    }
    
       
    @RequestMapping(value = "/fields")
    public String listFields(Model model) {

    	Field field=new Field();
    	model.addAttribute("field",field);
    	
        return "fields";
    }
    
    
    @RequestMapping(value = "/fields/save", method = RequestMethod.POST)
    public String saveField(@ModelAttribute("field") @Valid final Field field, final BindingResult bindingResult, final ModelMap model) {
// from Petclinic: the validator is executed manually
 //   	new FieldValidator().validate(field, bindingResult);
    	if (bindingResult.hasErrors()) {
    	
    		model.addAttribute("field",field);
           return "fields";
        }
    	
    	if (field.getId()!= 0){
    		// Saving the link to contracts and fields in order not to overwrite
    		field.setUsers(fieldRepository.findFieldById(field.getId()).getUsers());
    		field.setContracts(fieldRepository.findFieldById(field.getId()).getContracts());
    	}
    	
    	
    	this.fieldRepository.saveField(field);
        return "redirect:/fields";
        
    }
    
    
    @RequestMapping(value = "/fields/{id}/contracts", method = RequestMethod.GET)
    public String showFields(@PathVariable int id, Model model) {
 
    	Field field=this.fieldRepository.findFieldById(id);
    	
    	
    	Map<Contract, Boolean> contracts=new LinkedHashMap<Contract,Boolean>();

    	Query query=em.createNativeQuery("SELECT c.id as cid, cfields.id as fid FROM contracts c LEFT OUTER JOIN (SELECT c.contract_id, c.field_id as id FROM contractsfields c WHERE c.field_id=:id) as cfields ON c.id=cfields.contract_id");
    	query.setParameter("id", field.getId());
    	
    	@SuppressWarnings("unchecked")
		List<Object[]> results=query.getResultList();
    	for(Object[] result : results){
    		
    		Contract contract=contractRepository.findContractById((Integer) result[0]);
    		if((result[1] != null) && ((Integer) result[1] == field.getId())){
     			contracts.put(contract, true);
    		} else {
     			contracts.put(contract, false);
    		}
    	}
    	
   
    	model.addAttribute("field",field);
    	model.addAttribute("contracts",contracts);
  
    
        return "field-contracts";      
    } 
    
    @RequestMapping(value = "/fields/{id}/savecontracts", method = RequestMethod.POST)
    public String SaveFields(@PathVariable int id, @RequestParam HashMap<String,String> contracts, ModelMap model) {
    
    	Field field=this.fieldRepository.findFieldById(id);
    	Set<Contract> newContracts=new HashSet<Contract>();
    	
    	for(String s : contracts.keySet()){
    		Contract c=contractRepository.findContractById(Integer.decode(s));
    		newContracts.add(c);
    	}
    	field.setContracts(newContracts);
    	fieldRepository.saveField(field);
    	
    	return "redirect:/fields/";

    }
    
    
    @RequestMapping(value = "/fields/{id}/users", method = RequestMethod.GET)
    public String showUsers(@PathVariable int id, Model model) {
 
    	Field field=this.fieldRepository.findFieldById(id);
    	
    	
    	Map<User, Boolean> users=new LinkedHashMap<User,Boolean>();

    	Query query=em.createNativeQuery("SELECT users.id, fusers.user_id FROM users LEFT OUTER JOIN (SELECT uf.user_id FROM usersfields uf WHERE uf.field_id=:id) as fusers ON users.id=fusers.user_id WHERE users.role!=0 ORDER by users.role ASC, users.email ASC");
    	query.setParameter("id", field.getId());
    	
    	@SuppressWarnings("unchecked")
		List<Object[]> results=query.getResultList();
    	for(Object[] result : results){
    		
    		User user=userRepository.findUserById((Integer) result[0]);
    		if(result[1] != null){
     			users.put(user, true);
    		} else {
     			users.put(user, false);
    		}
    	}
    	

/* 		This  code does not work, I don't know why!!!
    	List<User> allUsers=userRepository.queryAll();
    	for(User user : allUsers){
    		if (user.getRole() != UserRolesEnum.ADMIN){
	    		if (field.getUsers().contains(user)){
	    			users.put(user,true);
	    		} else {
	    			users.put(user,false);
	    		}
    		}
    	}
*/    	
    	model.addAttribute("field",field);
    	model.addAttribute("users",users);
  
    
        return "field-users";
        
        
    } 
    
    @RequestMapping(value = "/fields/{id}/saveusers", method = RequestMethod.POST)
    public String saveUsers(@PathVariable int id, @RequestParam HashMap<String,String> users, ModelMap model) {
    
    	Field field=this.fieldRepository.findFieldById(id);
    	Set<User> newUsers=new HashSet<User>();
    	
    	for(String s : users.keySet()){
    		User u=userRepository.findUserById(Integer.decode(s));
    		newUsers.add(u);
    	}
    	field.setUsers(newUsers);
    	fieldRepository.saveField(field);
    	
    	return "redirect:/fields/";

    }
    
    
    
}