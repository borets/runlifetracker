package ru.shaposhnik.runlifetracker.web;

import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import ru.shaposhnik.runlifetracker.model.Installation;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.repository.ContractRepository;
import ru.shaposhnik.runlifetracker.repository.FieldRepository;
import ru.shaposhnik.runlifetracker.repository.InstallationRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.distribution.FDistribution;



@Controller
@RequestMapping(value = "/runlifestat")
public class MTBFController {

    private final InstallationRepository installationRepository;
    private final FieldRepository fieldRepository;
    private final ContractRepository contractRepository;
    
    @PersistenceContext
	private EntityManager em;
    
    
    @Autowired
    public MTBFController(InstallationRepository ir, FieldRepository fr, ContractRepository cr) {
        this.installationRepository = ir;
        this.fieldRepository=fr;
        this.contractRepository=cr;
    }


    
    @RequestMapping(params = {"type=MTBF"})
    public String listContracts(Principal principal,
    							@RequestParam("type") String statType,
    							@RequestParam("window") int window,
    							@RequestParam("analyzeBy") String analyzeBy,
    							@RequestParam("country") String country, Model model) {
 
    	User user = (User) ((Authentication) principal).getPrincipal();
    	model.addAttribute("allCountries", this.installationRepository.availableCountries(user));
    	if (country == ""){
    		country= this.installationRepository.availableCountries(user).get(0);
    	} 
    	model.addAttribute("selcountry", country);
    	model.addAttribute("window", window);
    	model.addAttribute("analyzeby", analyzeBy);
    	model.addAttribute("statType", statType);
    	
    	String label;
    	Calendar startDate,stopDate;
    	startDate=Calendar.getInstance();
    	stopDate=Calendar.getInstance();
    	
    	Map<String,String>labels = new LinkedHashMap<String,String>();
    	Set<Date>dates = new TreeSet<Date>();
     	Map<Pair,Integer> failures = new HashMap<Pair, Integer>();
     	Map<Pair,Long> runLife = new HashMap<Pair, Long>();
    	
    	Calendar thisMonth =Calendar.getInstance();
    	thisMonth.setTime(new Date());
    	thisMonth.add(Calendar.MONTH, 1);  
    	thisMonth.set(Calendar.DAY_OF_MONTH, 1);  
    	thisMonth.add(Calendar.DATE, -1);
    	thisMonth.set(Calendar.HOUR_OF_DAY,0);
    	thisMonth.set(Calendar.MINUTE,0);
    	thisMonth.set(Calendar.SECOND,0);
    	thisMonth.set(Calendar.MILLISECOND,0);
    	
      	
    	Calendar winFirstDay = Calendar.getInstance();
    	Calendar winLastDay = Calendar.getInstance();
    	
    	List<Installation> installations= this.installationRepository.queryCountry(country,user);
    	
    	System.out.println(thisMonth.getTime());
    	for(Installation inst:installations){
    		
    		if (inst.getStartEvent()!= null){
	    		startDate.setTime(inst.getStartEvent().getEventDate());
	    		label=inst.getLabel(analyzeBy);
	    		labels.put(label,"2014-05-31");
	    		if(inst.getStopEvent() != null){
	    			stopDate.setTime(inst.getStopEvent().getEventDate());
	    		} else {
	    			stopDate.setTime(thisMonth.getTime());
	    			stopDate.add(Calendar.DATE, 1);
	    		}
	    	
	    		System.out.println(inst.getWell().getName());
	    		
	    		winLastDay.setTime(startDate.getTime());
	    		winLastDay.add(Calendar.MONTH, 1);  
	    		winLastDay.set(Calendar.DAY_OF_MONTH, 1);  
	    		winLastDay.add(Calendar.DATE, -1);
	    		winFirstDay.setTime(winLastDay.getTime());
	    		winFirstDay.add(Calendar.DATE, -1*window);

	    		while (!winLastDay.after(thisMonth)){
	    			Pair key=new Pair(label,winLastDay.getTime());
	    			dates.add(winLastDay.getTime());
	    			if (failures.get(key) == null){
	    				failures.put(key, 0);
	    			}
	    			if (runLife.get(key) == null){
	    				runLife.put(key, (long) 0);
	    			}
	    			
	    			runLife.put(key,runLife.get(key)+xPeriods(winFirstDay,winLastDay,startDate,stopDate));
	    			
	    			if (winLastDay.after(stopDate) && winFirstDay.before(stopDate) && inst.getIsFailure()){
	    				failures.put(key, failures.get(key)+1);
	    			}
	    			
	    			
	    			winLastDay.add(Calendar.MONTH, 2);  
	    			winLastDay.set(Calendar.DAY_OF_MONTH, 1);  
	    			winLastDay.add(Calendar.DATE, -1);
	    			winFirstDay.setTime(winLastDay.getTime());
		    		winFirstDay.add(Calendar.DATE, -1*window);
	    		}
    		}
    	}
    	
    	SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd");
    	String out="Date";
    	for(String l : labels.keySet()){
    		out=out+","+l;
    	}
    	
    	HashMap<String,Double> maxMtbf = new HashMap<String,Double>();
    	for (String l : labels.keySet()){
    		maxMtbf.put(l, (double) 0);
    	}
    	
    	for (Date d:dates){
    		out=out+"\n"+df.format(d);
    		for(String l : labels.keySet()){
    			Pair p = new Pair(l,d);
    			if (failures.get(p) != null && failures.get(p) != 0){
    				out=out+","+runLife.get(p)/failures.get(p);
    				
    				if (runLife.get(p)/failures.get(p) > maxMtbf.get(l)){
    					maxMtbf.put(l, (double) (runLife.get(p)/failures.get(p)));
    					labels.put(l, df.format(d));
    				}
    				
    			} else {
    				out=out+",";
    			}
    			
    		}
    	}	
    	
    	model.addAttribute("labels",labels);
    	model.addAttribute("data", out);
        return "mtbf";
        
    }

//-------------------------------------------------------------------------------------
// ----------------------Failure Index ------------------------------------------------
// ------------------------------------------------------------------------------------   
    @RequestMapping(params = {"type=IndexOfFailures"})
    public String failureIndex(Principal principal,
    							@RequestParam("type") String statType,
    							@RequestParam("window") int window,
    							@RequestParam("analyzeBy") String analyzeBy,
    							@RequestParam("country") String country, Model model) {
 
    	User user = (User) ((Authentication) principal).getPrincipal();
    	model.addAttribute("allCountries", this.installationRepository.availableCountries(user));
    	if (country == ""){
    		country= this.installationRepository.availableCountries(user).get(0);
    	} 
    	
    	window=365;
    	model.addAttribute("selcountry", country);
    	model.addAttribute("window", window);
    	model.addAttribute("analyzeby", analyzeBy);
    	model.addAttribute("statType", statType);
    	
    	String label;
    	Calendar startDate,stopDate;
    	startDate=Calendar.getInstance();
    	stopDate=Calendar.getInstance();
    	
    	Map<String,String>labels = new LinkedHashMap<String,String>();
    	Set<Date>dates = new TreeSet<Date>();
     	Map<Pair,Integer> failures = new HashMap<Pair, Integer>();
     	Map<Pair,Integer> population = new HashMap<Pair, Integer>();
    	
    	Calendar thisMonth =Calendar.getInstance();
    	thisMonth.setTime(new Date());
    	thisMonth.add(Calendar.MONTH, 1);  
    	thisMonth.set(Calendar.DAY_OF_MONTH, 1);  
    	thisMonth.add(Calendar.DATE, -1);
    	thisMonth.set(Calendar.HOUR_OF_DAY,0);
    	thisMonth.set(Calendar.MINUTE,0);
    	thisMonth.set(Calendar.SECOND,0);
    	thisMonth.set(Calendar.MILLISECOND,0);
    	
      	
    	Calendar winFirstDay = Calendar.getInstance();
    	Calendar winLastDay = Calendar.getInstance();
    	
    	List<Installation> installations= this.installationRepository.queryCountry(country,user);
    	
    	System.out.println(thisMonth.getTime());
    	for(Installation inst:installations){
    		
    		if (inst.getStartEvent()!= null){
	    		startDate.setTime(inst.getStartEvent().getEventDate());
	    		label=inst.getLabel(analyzeBy);
	    		labels.put(label,"2014-05-31");
	    		if(inst.getStopEvent() != null){
	    			stopDate.setTime(inst.getStopEvent().getEventDate());
	    		} else {
	    			stopDate.setTime(thisMonth.getTime());
	    			stopDate.add(Calendar.DATE, 1);
	    		}
	    	
	    		System.out.println(inst.getWell().getName());
	    		
	    		winLastDay.setTime(startDate.getTime());
	    		winLastDay.add(Calendar.MONTH, 1);  
	    		winLastDay.set(Calendar.DAY_OF_MONTH, 1);  
	    		winLastDay.add(Calendar.DATE, -1);
	    		winFirstDay.setTime(winLastDay.getTime());
	    		winFirstDay.add(Calendar.DATE, -1*window);

	    		while (!winLastDay.after(thisMonth)){
	    			Pair key=new Pair(label,winLastDay.getTime());
	    			dates.add(winLastDay.getTime());
	    			if (failures.get(key) == null)
	    				failures.put(key, 0);
	    		
	    			if (population.get(key) == null)
	    				population.put(key, 0);
	    			
	    			
	    			if (winLastDay.before(stopDate) && winLastDay.after(startDate))
	    				population.put(key,population.get(key)+1);
	    				    			
	    			if (winLastDay.after(stopDate) && winFirstDay.before(stopDate) && inst.getIsFailure())
	    				failures.put(key, failures.get(key)+1);
	    			
	    			
	    			
	    			winLastDay.add(Calendar.MONTH, 2);  
	    			winLastDay.set(Calendar.DAY_OF_MONTH, 1);  
	    			winLastDay.add(Calendar.DATE, -1);
	    			winFirstDay.setTime(winLastDay.getTime());
		    		winFirstDay.add(Calendar.DATE, -1*window);
	    		}
    		}
    	}
    	
    	SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd");
    	String out="Date";
    	for(String l : labels.keySet()){
    		out=out+","+l;
    	}
    	
    	HashMap<String,Float> maxFIndex = new HashMap<String,Float>();
    	for (String l : labels.keySet()){
    		maxFIndex.put(l, (float) 0);
    	}
    	Float fi;
    	for (Date d:dates){
    		out=out+"\n"+df.format(d);
    		for(String l : labels.keySet()){
    			Pair p = new Pair(l,d);
    			if (population.get(p) != null && population.get(p) != 0){
    				fi=(float) failures.get(p)/population.get(p);
    				out=out+","+fi;
    				
    				if (failures.get(p)/population.get(p) > maxFIndex.get(l)){
    					maxFIndex.put(l, (float) failures.get(p)/population.get(p));
    					labels.put(l, df.format(d));
    				}
    				
    			} else {
    				out=out+",";
    			}
    			
    		}
    	}	
    	
    	model.addAttribute("labels",labels);
    	model.addAttribute("data", out);
        return "mtbf";
        
    }
    
    
    
    
       
   private long xPeriods(Calendar w1, Calendar w2, Calendar r1, Calendar r2){
	    if (w2.before(r1) || r2.before(w1)){
	    	return 0;
	    }
	    
	   return (Math.min(w2.getTime().getTime(),r2.getTime().getTime())-Math.max(w1.getTime().getTime(),r1.getTime().getTime()))/(1000*60*60*24);
   }
    
    
  public class Pair {
	  public String label;
	  public Date day;
	  
	  Pair(String l, Date d){
		  label=l;
		  day=d;
	  }
	  
	  @Override   
	   public boolean equals(Object obj) {
	      if (!(obj instanceof Pair))
	        return false;
	      Pair ref = (Pair) obj;
	      return this.label.equals(ref.label) && 
	          this.day.equals(ref.day);
	   }

	    @Override
	    public int hashCode() {
	        return (label+day).toString().hashCode();
	    }
  }
   
  //===============================================================================
  
  @RequestMapping(value = "/init")
  public String mtbfInit(Principal principal,@RequestParam("type") String type, Model model) {

  	User user = (User) ((Authentication) principal).getPrincipal();
  	model.addAttribute("type",type);
  	model.addAttribute("allCountries", this.installationRepository.availableCountries(user));
  	return "mtbf";
  	
  }
  
//=================================================================================
  
  @RequestMapping(value = "/init2")
  public String filtermtbf(Principal principal, @RequestParam("type") String type, Model model) {

  	User user = (User) ((Authentication) principal).getPrincipal();
  	model.addAttribute("type",type);
  	model.addAttribute("allCountries", this.installationRepository.availableCountries(user));
  	model.addAttribute("allFields", this.fieldRepository.query4User(user));
  	model.addAttribute("allContracts", this.contractRepository.query4User(user));
  	return "mtbf2";
  	
  }
    
  //=================================================================================
  
  private static final String AGGLABEL="Average";
  
  @RequestMapping(value= "/runlife.txt")
  public void mtbfCSV(Principal principal,
		  					@RequestParam("graphtype") String graphType,
  							@RequestParam("window") int window,
  							@RequestParam("analyzeBy") String analyzeBy,
  							@RequestParam("country") String country,HttpServletResponse response)throws IOException {

  	User user = (User) ((Authentication) principal).getPrincipal();

  	
  	String label;
  	Calendar startDate,stopDate;
  	startDate=Calendar.getInstance();
  	stopDate=Calendar.getInstance();
  	
  	Map<String,String>labels = new LinkedHashMap<String,String>();
  	Set<Date>dates = new TreeSet<Date>();
   	Map<Pair,Integer> failures = new HashMap<Pair, Integer>();
   	Map<Pair,Long> runLife = new HashMap<Pair, Long>();
 	Map<Pair,Integer> population = new HashMap<Pair, Integer>();
  	
  	Calendar thisMonth =Calendar.getInstance();
  	thisMonth.setTime(new Date());
  	thisMonth.add(Calendar.MONTH, 1);  
  	thisMonth.set(Calendar.DAY_OF_MONTH, 1);  
  	thisMonth.add(Calendar.DATE, -1);
  	thisMonth.set(Calendar.HOUR_OF_DAY,0);
  	thisMonth.set(Calendar.MINUTE,0);
  	thisMonth.set(Calendar.SECOND,0);
  	thisMonth.set(Calendar.MILLISECOND,0);
  	
    	
  	Calendar winFirstDay = Calendar.getInstance();
  	Calendar winLastDay = Calendar.getInstance();
  	
  	List<Installation> installations;
  	if (!analyzeBy.equals("country")){
  		if (country == "") this.installationRepository.availableCountries(user).get(0);
  		installations = this.installationRepository.queryCountry(country,user);
  	} else {
  		installations= this.installationRepository.queryAll(user);
  	}
  	
  	for(Installation inst:installations){
  		
  		if (inst.getStartEvent()!= null){
	    		startDate.setTime(inst.getStartEvent().getEventDate());
	    		labels.put(AGGLABEL,"2014-05-31");
	    		label=inst.getLabel(analyzeBy);
	    		labels.put(label,"2014-05-31");
	    		if(inst.getStopEvent() != null){
	    			stopDate.setTime(inst.getStopEvent().getEventDate());
	    		} else {
	    			stopDate.setTime(thisMonth.getTime());
	    			stopDate.add(Calendar.DATE, 1);
	    		}
	    	
	    		
	    		winLastDay.setTime(startDate.getTime());
	    		winLastDay.add(Calendar.MONTH, 1);  
	    		winLastDay.set(Calendar.DAY_OF_MONTH, 1);  
	    		winLastDay.add(Calendar.DATE, -1);
	    		winFirstDay.setTime(winLastDay.getTime());
	    		winFirstDay.add(Calendar.DATE, -1*window);

	    		while (!winLastDay.after(thisMonth)){
	    			Pair key=new Pair(label,winLastDay.getTime());
	    			Pair average=new Pair(AGGLABEL,winLastDay.getTime());
	    			dates.add(winLastDay.getTime());

	    			if (failures.get(average) == null){
	    				failures.put(average, 0);
	    				runLife.put(average, (long) 0);
	    				population.put(average, 0);	    				
	    			}
	    			
	    			if (failures.get(key) == null){
	    				failures.put(key, 0);
	    				runLife.put(key,(long) 0);
	    				population.put(key, 0);

	    			}

	    			
	    			
	    			runLife.put(key,runLife.get(key)+xPeriods(winFirstDay,winLastDay,startDate,stopDate));
	    			runLife.put(average,runLife.get(average)+xPeriods(winFirstDay,winLastDay,startDate,stopDate));
	    			
	    			if (winLastDay.before(stopDate) && winLastDay.after(startDate)){
	    				population.put(key,population.get(key)+1);
	    				population.put(average,population.get(average)+1);
	    			}
	    			
	    			if (winLastDay.after(stopDate) && winFirstDay.before(stopDate) && inst.getIsFailure()){
	    				failures.put(key, failures.get(key)+1);
	    				failures.put(average, failures.get(average)+1);
	    			}
	    			
	    			
	    			    			
	    			winLastDay.add(Calendar.MONTH, 2);  
	    			winLastDay.set(Calendar.DAY_OF_MONTH, 1);  
	    			winLastDay.add(Calendar.DATE, -1);
	    			winFirstDay.setTime(winLastDay.getTime());
		    		winFirstDay.add(Calendar.DATE, -1*window);
	    		}
  		}
  	}

  	 // if there is only one label, delete aggregate label
  	  if(labels.keySet().size() == 2)
  		  labels.remove(AGGLABEL);
 	
  	
  	SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd");
  	String out="Date";
  	for(String l : labels.keySet())
  		out=out+","+l;
  	
  	
  	switch (graphType){
	  	
	  	case "mtbf":
	  			  		
		  	Double mtbf,mtbfLow,mtbfHigh;
		  	for (Date d:dates){
		  		out=out+"\n"+df.format(d);
		  		
		  		
		  		
		  		for(String l : labels.keySet()){
		  			Pair p = new Pair(l,d);
		  			if (failures.get(p) != null && failures.get(p) != 0){
		
		  				
			  				mtbf=(double) (runLife.get(p)/failures.get(p));
			  				mtbfLow=2*runLife.get(p)/chi2(failures.get(p)*2,"h");
			  				mtbfHigh=2*runLife.get(p)/chi2(failures.get(p)*2,"l");
			  				if (mtbfHigh > 2*mtbf)  mtbfHigh=2*mtbf; 
			  					out=out+","+String.format("%.0f", mtbfLow)+";"+String.format("%.0f", mtbf)+";"+String.format("%.0f", mtbfHigh);
			  	
		  				
		  			} else {
		  				out=out+",";
		  			}
		  			
		  		}
		  	}
		  	out=out+"\n";
		  	break;
  
  	  	case "fi":

	  	  	Float fi;
	    	for (Date d:dates){
	    		out=out+"\n"+df.format(d);
	  
	    		for(String l : labels.keySet()){
	    			Pair p = new Pair(l,d);
	    			if (population.get(p) != null && population.get(p) != 0){
	    				fi=(float) failures.get(p)/population.get(p);
	    				out=out+","+String.format("%.3f", fi);
	    				
	    			} else {
	    				out=out+",";
	    			}
	    			
	    		}
	    	}
		  	out=out+"\n";
  			break;  			  		
  			  			  		
  	  	default:
  	  		break;  	
  	}
  	
  	
  	response.setContentType("text/csv");
	response.getOutputStream().print(out);
	response.getOutputStream().flush();
  	
        
  }
  
  
  private double chi2(int n, String f){
	  
	  ChiSquaredDistribution chi2Distr = new ChiSquaredDistribution(n);
	  switch (f){
	  	case "l": return chi2Distr.inverseCumulativeProbability((Double) 0.025);
	  	case "h": return chi2Distr.inverseCumulativeProbability((Double) 0.975);
	  default: return 0;
	  }
	  

  }
  
}

// Not to show Average if there is only one label
// Sorting labels - Average the 1st