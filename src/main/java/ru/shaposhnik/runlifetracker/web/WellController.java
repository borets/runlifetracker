/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.exceptions.UnauthorizedException;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.repository.FieldRepository;
import ru.shaposhnik.runlifetracker.model.Well;
import ru.shaposhnik.runlifetracker.repository.WellRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
public class WellController {

    private final WellRepository wellRepository;
    private final FieldRepository fieldRepository;


    @Autowired
    public WellController(WellRepository wr, FieldRepository fr) {
        this.wellRepository = wr;
        this.fieldRepository=fr;
    }

    @Autowired
    private WellValidator wellValidator;
    

    @ModelAttribute
    public Model prepareData(Principal principal, Model model){
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	List<Well> allWells=wellRepository.query4User(user);
      	model.addAttribute("allWells",allWells);
    	
      	List<Field> allFields=this.fieldRepository.query4User(user);
      	model.addAttribute("allFields",allFields);
      	
      	Map<Integer, String> prefixes=new HashMap<Integer, String>();
      	for (Field f : allFields){
      		prefixes.put(f.getId(),f.getWellpattern());
      	}
      	model.addAttribute("prefixes",prefixes);   	
      	return model;
    }
    
    
    @RequestMapping(value = "/wells")
    public String listFields(Principal principal, Model model) {
    	Well well=new Well();
    	model.addAttribute("well",well);
    	
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	model.addAttribute("user", user);
 
        return "wells";
        
    }
    
    @RequestMapping(value = "/wells/save", method = RequestMethod.POST)
    public String saveField(Principal principal,@ModelAttribute("well") @Valid final Well well, final BindingResult bindingResult, final ModelMap model)
    		throws UnauthorizedException {
// from Petclinic: the validator is executed manually
 //   	new FieldValidator().validate(field, bindingResult);
    	wellValidator.validate(well, bindingResult);
    	if (bindingResult.hasErrors()) {    	
    		model.addAttribute("well",well);
    		return "wells";
        }
    	User user = (User) ((Authentication) principal).getPrincipal();
   
    	this.wellRepository.saveWell(well, user);
        return "redirect:/wells";
        
    }
    
    
        
    
}