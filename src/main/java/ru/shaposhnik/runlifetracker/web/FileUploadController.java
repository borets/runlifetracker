package ru.shaposhnik.runlifetracker.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import ru.shaposhnik.runlifetracker.model.Installation;
import ru.shaposhnik.runlifetracker.model.SavedFile;
import ru.shaposhnik.runlifetracker.repository.InstallationRepository;
import org.apache.commons.codec.binary.Base64;

@Controller
public class FileUploadController {

//	@PersistenceContext
//	private EntityManager em;
    private InstallationRepository installationRepository;
    
    @Autowired
    public FileUploadController(InstallationRepository ir) {
        this.installationRepository = ir;

    }

    @RequestMapping(value="/fileupload", method=RequestMethod.POST)
    @ResponseBody
    public String handleFileUpload(@RequestParam("filename") String name,
    		@RequestParam("instid") Installation installation,
            @RequestParam("file") MultipartFile file){
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                /* *** This code saves the file to filesystem - from Spring docs
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File(name)));
                stream.write(bytes);
                stream.close();
                *** */
                SavedFile f= new SavedFile();
                f.setContentType(file.getContentType());
                f.setInstallation(installation);
                f.setFile(new String(Base64.encodeBase64(bytes)));
                return String.valueOf(installationRepository.saveFile(f).getId());
//                return "You successfully uploaded " + name + "!";
                
            } catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + name + " because the file was empty.";
        }
    }
    
    @RequestMapping(value="/installations/{id}/deletefiles", method=RequestMethod.POST)
    @ResponseBody
    public void handleFileDeleteByInst(@PathVariable("id") Installation i){
    	installationRepository.deleteFileByInst(i);
    };

    
    @RequestMapping(value="/filedownload", method=RequestMethod.GET)
    public HttpEntity<byte[]> handleFileDownload(@RequestParam("id") int file_id){
    	
    	SavedFile file=installationRepository.findFileById(file_id);
    	byte[] decoded=Base64.decodeBase64(file.getFile());
    	
        HttpHeaders responseHeaders = new HttpHeaders();
        String[] contentType=file.getContentType().split("/");
        responseHeaders.setContentType(new MediaType(contentType[0],contentType[1]));
//        responseHeaders.setContentLength(file.);
        responseHeaders.setContentDispositionFormData("inline", "InstallationReport."+contentType[1]+";");
        return new HttpEntity<byte[]>(decoded,responseHeaders);
    	
    }
    

}