package ru.shaposhnik.runlifetracker.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import ru.shaposhnik.runlifetracker.exceptions.UnauthorizedException;

import org.springframework.http.HttpStatus;



@ControllerAdvice
class GlobalControllerExceptionHandler {

	
	@ResponseStatus(value=HttpStatus.UNAUTHORIZED, reason="User does not have permission for this operation")
    @ExceptionHandler(UnauthorizedException.class)
    public void UnauthorizedExceptionHandler() {
        // Nothing to do
    }
}


   