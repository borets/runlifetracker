/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import ru.shaposhnik.runlifetracker.model.Client;

import ru.shaposhnik.runlifetracker.repository.ClientRepository;
import ru.shaposhnik.runlifetracker.model.CommonInfoService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.ModelAttribute;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
public class ClientController {

    private final ClientRepository clientRepository;


    @Autowired
    public ClientController(ClientRepository cr) {
        this.clientRepository = cr;
    }


    
    @RequestMapping(value = "/clients",method = RequestMethod.GET)
    public String listClients(Model model) {
    	model.addAttribute("allClients", this.clientRepository.queryAll());
    	model.addAttribute("allCountries", CommonInfoService.getCountryList());
    	
    	Client client=new Client();
    	model.addAttribute("client", client);
        return "clients";
        
    }
    
    @RequestMapping(value = "/clients/save", method = RequestMethod.POST)
    public String saveClient(@ModelAttribute("client") @Valid final Client client, final BindingResult bindingResult, final ModelMap model) {
    	// from Petclinic: the validator is executed manually in addition to default validator
    	// another approach: http://stackoverflow.com/questions/25075683/spring-mvc-validator-annotation-custom-validation
    	
 //   	userValidator.validate(user, bindingResult);
    	if (bindingResult.hasErrors()) {
    	
    		model.addAttribute("client",client);
    		model.addAttribute("allClients", this.clientRepository.queryAll());
        	model.addAttribute("allCountries", CommonInfoService.getCountryList());
    		
    		return "clients";
        }
    	    	
    	this.clientRepository.saveClient(client);
        return "redirect:/clients";
        
    }    
    
}