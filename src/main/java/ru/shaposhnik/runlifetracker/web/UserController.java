/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import java.security.Principal;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.ui.ModelMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import ru.shaposhnik.runlifetracker.model.UserRolesEnum;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.repository.FieldRepository;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.repository.UserRepository;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;


@Controller
public class UserController {

		
	private final UserRepository userRepository;
    private final FieldRepository fieldRepository;

    @PersistenceContext
	private EntityManager em;
    
    
    @Autowired
    public UserController(UserRepository ur, FieldRepository fr) {
        this.userRepository = ur;
        this.fieldRepository= fr;

    }

    
    @Autowired
    private UserValidator userValidator;
      
    @RequestMapping(value = "/users")
    public String listUsers(Model model) {
    	model.addAttribute("allUsers", this.userRepository.queryAll());
    	UserRolesEnum[] allRoles=UserRolesEnum.values();
    	model.addAttribute("allRoles",allRoles);
    	
    	User user=new User();
    	model.addAttribute("user",user);
    	
        return "users";
    }
    
    
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String LoggedInUser(Principal principal, Model model) {
    	User user = (User) ((Authentication) principal).getPrincipal();
    	model.addAttribute("user", user);
    	return "user-info";
    }
    
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String ChangePassword(Principal principal, @RequestParam("password") String password, Model model) {
    	User user = (User) ((Authentication) principal).getPrincipal();
    	user.setPassword(password);
    	
    	this.userRepository.saveUser(user);
    	model.addAttribute("user", user);
    	return "user-info";
    }
    
    
    
    
    @RequestMapping(value = "/users/save", method = RequestMethod.POST)
    public String saveField(@ModelAttribute("user") @Valid final User user, final BindingResult bindingResult, final ModelMap model) {
    	// from Petclinic: the validator is executed manually in addition to default validator
    	// another approach: http://stackoverflow.com/questions/25075683/spring-mvc-validator-annotation-custom-validation
    	
    	userValidator.validate(user, bindingResult);
    	if (bindingResult.hasErrors()) {
    	
    		model.addAttribute("user",user);
    		model.addAttribute("allUsers", this.userRepository.queryAll());
    		UserRolesEnum[] allRoles=UserRolesEnum.values();
        	model.addAttribute("allRoles",allRoles);
    		return "users";
        }
    	
    	// Copying fields in order not to overwrite them
    	if (user.getId()>0){
    		Set<Field> fields=userRepository.findUserById(user.getId()).getFields();
    		user.setFields(fields);
    	}
    	
    	this.userRepository.saveUser(user);
        return "redirect:/users";
        
    }
    

    
    
    
    
    @RequestMapping(value = "/users/{user_id}/fields")
    public String listRights(@PathVariable int user_id, Model model) {
    	User user=this.userRepository.findUserById(user_id);
    	
    	Map<Field, Boolean> rights=new LinkedHashMap<Field,Boolean>();

    	Query query=em.createNativeQuery("SELECT f.id, ffields.uid FROM fields f LEFT OUTER JOIN (SELECT ur.field_id as fid, ur.user_id as uid FROM usersfields ur WHERE ur.user_id=:id) as ffields ON f.id=ffields.fid  ORDER BY f.country, f.name ASC");
    	query.setParameter("id", user.getId());
    	@SuppressWarnings("unchecked")
		List<Object[]> results=query.getResultList();
    	for(Object[] result : results){
    		
    		Field field=fieldRepository.findFieldById((Integer) result[0]);
    		if(result[1] != null) {
     			rights.put(field, true);
    		} else {
     			rights.put(field, false);
    		}
    	}


    	model.addAttribute("user",user);
    	model.addAttribute("rights",rights);
  
        return "user-fields";      
    }
    
    
    @RequestMapping(value = "/users/{user_id}/savefields", method = RequestMethod.POST)
    public String SaveRights(@PathVariable int user_id, @RequestParam HashMap<String,String> rights, ModelMap model) {
    
    	User user=this.userRepository.findUserById(user_id);
    	Set<Field> newRights=new HashSet<Field>();
    	
    	for(String s : rights.keySet()){
    		Field f=fieldRepository.findFieldById(Integer.decode(s));
    		newRights.add(f);
    	}
    	user.setFields(newRights);
    	userRepository.saveUser(user);
    	
    	return "redirect:/users/";
    }


}