/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import java.io.IOException;

import java.security.Principal;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import ru.shaposhnik.runlifetracker.repository.InstallationRepository;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.UserRolesEnum;
import ru.shaposhnik.runlifetracker.model.Reasons4PullEnum;
import ru.shaposhnik.runlifetracker.model.FailureCausesEnum;
import ru.shaposhnik.runlifetracker.model.FailuredComponentEnum;
import ru.shaposhnik.runlifetracker.model.FailureResponsibilityEnum;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class BarChartController {

    private final InstallationRepository installationRepository;

	
	
	@PersistenceContext
	private EntityManager em;


    @Autowired
    public BarChartController(InstallationRepository ir) {
    	
    	this.installationRepository=ir;
    }

    
    @RequestMapping(value = "/barchart",method = RequestMethod.GET)
    public String initpage(Principal principal, Model model){
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	List<String> allCountries = this.installationRepository.availableCountries(user);
    	model.addAttribute("allCountries", allCountries);
		
    	return "barchart-clientrecognition";
    }
    
    @RequestMapping(value = "/charts2",method = RequestMethod.GET)
    public String initCharts2(Principal principal, Model model){
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	List<String> allCountries = this.installationRepository.availableCountries(user);
    	model.addAttribute("allCountries", allCountries);
		
    	return "charts2";
    }
    
    @RequestMapping(value = "/charts3",method = RequestMethod.GET)
    public String initCharts3(Principal principal, Model model){
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	List<String> allCountries = this.installationRepository.availableCountries(user);
    	model.addAttribute("allCountries", allCountries);
		
    	return "charts3";
    }
    
    
    @RequestMapping(value = "/teardowns",method = RequestMethod.GET)
    public String initCharts4(Principal principal, Model model){
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	List<String> allCountries = this.installationRepository.availableCountries(user);
    	model.addAttribute("allCountries", allCountries);
		
    	return "teardownscharts";
    }
    
    @RequestMapping(value = "/recognitions4country",method = RequestMethod.GET)
    public void recognitions4country(Principal principal, @RequestParam("country") String country,HttpServletResponse response) throws IOException{
    
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	String whereClause;
		if (user.getRole()!=UserRolesEnum.ADMIN)
			whereClause="w.field_id in (select uf.field_id from usersfields uf where uf.user_id ="+user.getId()+")";
    	else
    		whereClause="1=1";
    	
    	
 /*   	String queryStr="select * from crosstab('\n" + 
    			"\n" + 
    			"select c.name as contractname, l.client_class as ftype, count(l.client_class) \n" + 
    			"from lcevents l\n" + 
    			"inner join\n" + 
    			"installations i\n" + 
    			"on l.installation_id=i.id\n" + 
    			"inner join wells w\n" + 
    			"on w.id=i.well_id\n" + 
    			"inner join fields f\n" + 
    			"on f.id=w.field_id\n" +
    			"inner join contracts c\n" +
    			"on c.id=i.contract_id\n" +
    			"where l.eventtype=4 and f.country=''"+country+"''"+"and "+whereClause+"\n" + 
    			"group by contractname,ftype\n" + 
    			"order by 1,2\n" + 
    			"','\n" + 
    			"select distinct client_class \n" + 
    			"from lcevents \n" + 
    			"where eventtype=4 \n" + 
    			"order by 1\n" + 
    			"')\n" + 
    			"\n" + 
    			"as (\n" + 
    			"\"contractname\" text,\n" + 
    			"\"Direct\" int,\n" + 
    			"\"Indirect\" int\n" + 
    			"\n" + 
    			");";
 */   	
		// added the 1st field cl.id
		String queryStr="select c1.id as id, c1.name as name, p.direct as direct, p.indirect as indirect from crosstab('\n" + 
    			"\n" + 
    			"select c.id as contract_id, l.client_class as ftype, count(l.client_class) \n" + 
    			"from lcevents l\n" + 
    			"inner join\n" + 
    			"installations i\n" + 
    			"on l.installation_id=i.id\n" + 
    			"inner join wells w\n" + 
    			"on w.id=i.well_id\n" + 
    			"inner join fields f\n" + 
    			"on f.id=w.field_id\n" +
    			"inner join contracts c\n" +
    			"on c.id=i.contract_id\n" +
    			"where l.eventtype=4 and f.country=''"+country+"''"+"and "+whereClause+"\n" + 
    			"group by c.id,ftype\n" + 
    			"order by 1,2\n" + 
    			"','\n" + 
    			"select distinct client_class \n" + 
    			"from lcevents \n" + 
    			"where eventtype=4 \n" + 
    			"order by 1\n" + 
    			"')\n" + 
    			"\n" + 
    			"as p(\n" + 
    			"\"contract_id\" int,\n" + 
    			"\"direct\" int,\n" + 
    			"\"indirect\" int\n" + 
    			"\n" + 
    			") inner join contracts c1 on c1.id=p.contract_id;";
		
		
		
    	Query query;
    	query=em.createNativeQuery(queryStr);
    	
//    	query.setParameter("country",country);
    	@SuppressWarnings("unchecked")
		List<String[]> results=query.getResultList();
		String out;
		
		
		// Also here is added the 1st field Id
		out="Id,Contract,Direct,Indirect\n";
		for (Object[] result : results){
    		

			out=out+result[0]+","+result[1]+",";
			if (result[2] != null)
				out=out+result[2]+",";
			else
				out=out+",";
				
			if (result[3] != null)
				out=out+result[3]+"\n";
			else
				out=out+"\n";
			
		}
		
		
		response.setContentType("text/csv");
    	response.getOutputStream().print(out);
    	response.getOutputStream().flush();
    	
    }
    
    
    @RequestMapping(value = "/recognitions4contract",method = RequestMethod.GET)
    public void recognitions4contract(Principal principal, @RequestParam("contract") int contract,HttpServletResponse response) throws IOException{
    
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	String whereClause;
		if (user.getRole()!=UserRolesEnum.ADMIN)
			whereClause="w.field_id in (select uf.field_id from usersfields uf where uf.user_id ="+user.getId()+")";
    	else
    		whereClause="1=1";
    	
    	
    	String queryStr="select * from crosstab('\n" + 
    			"\n" + 
    			"select extract(year from l.eventdate) as year, l.client_class as ftype, count(l.client_class) \n" + 
    			"from lcevents l\n" + 
    			"inner join\n" + 
    			"installations i\n" + 
    			"on l.installation_id=i.id\n" + 
    			"inner join wells w\n" + 
    			"on w.id=i.well_id\n" + 
    			"inner join fields f\n" + 
    			"on f.id=w.field_id\n" +
    			"inner join contracts c\n" +
    			"on c.id=i.contract_id\n" +
    			"where l.eventtype=4 and c.id="+contract+" and "+whereClause+"\n" + 
    			"group by year,ftype\n" + 
    			"order by 1,2\n" + 
    			"','\n" + 
    			"select distinct client_class \n" + 
    			"from lcevents \n" + 
    			"where eventtype=4 \n" + 
    			"order by 1\n" + 
    			"')\n" + 
    			"\n" + 
    			"as (\n" + 
    			"\"year\" text,\n" + 
    			"\"Direct\" int,\n" + 
    			"\"Indirect\" int\n" + 
    			"\n" + 
    			");";
    	
    	Query query;
    	query=em.createNativeQuery(queryStr);
    	
 //   	query.setParameter("contract",contract);
    	@SuppressWarnings("unchecked")
		List<String[]> results=query.getResultList();
		String out;
		
		out="Year,Direct,Indirect\n";
		for (Object[] result : results){
    		

			out=out+result[0]+",";
			if (result[1] != null)
				out=out+result[1]+",";
			else
				out=out+",";
				
			if (result[2] != null)
				out=out+result[2]+"\n";
			else
				out=out+"\n";
			
		}
		
		
		response.setContentType("text/csv");
    	response.getOutputStream().print(out);
    	response.getOutputStream().flush();
    	
    }
    
       
   
    @RequestMapping(value = "/csvstatistics",method = RequestMethod.GET)
    public void csvstatistics(Principal principal, HttpServletResponse response) throws IOException{
    
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	String whereClause;
		if (user.getRole()!=UserRolesEnum.ADMIN)
			whereClause="w.field_id in (select uf.field_id from usersfields uf where uf.user_id ="+user.getId()+")";
    	else
    		whereClause="1=1";
    	
    	
		String queryStr="\n"+
				"select c.name as contract_name, f.name as field_name, extract(year from l.eventdate) as year, l.client_class as ftype, count(l.client_class) \n" + 
    			"from lcevents l\n" + 
    			"inner join\n" + 
    			"installations i\n" + 
    			"on l.installation_id=i.id\n" + 
    			"inner join wells w\n" + 
    			"on w.id=i.well_id\n" + 
    			"inner join fields f\n" + 
    			"on f.id=w.field_id\n" +
    			"inner join contracts c\n" +
    			"on c.id=i.contract_id\n" +
    			"where l.eventtype=4 and "+whereClause+"\n" + 
    			"group by c.name, f.name, year, ftype\n";
		
		
		
    	Query query;
    	query=em.createNativeQuery(queryStr);
    	
    	@SuppressWarnings("unchecked")
		List<String[]> results=query.getResultList();
		String out;
		
		
		// Also here is added the 1st field Id
		out="Contract,Field,Year,Recognition,Qty\n";
		for (Object[] result : results){
    		out=out+result[0]+","+result[1]+","+result[2]+","+result[3]+","+result[4]+"\n";			
		}
		
		
		response.setContentType("text/csv");
    	response.getOutputStream().print(out);
    	response.getOutputStream().flush();
    	
    }
    
    
    @RequestMapping(value = "/pullreasons4country",method = RequestMethod.GET)
    public void pullReasons4Country(Principal principal, @RequestParam("country") String country,HttpServletResponse response) throws IOException{
    
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	String whereClause;
		if (user.getRole()!=UserRolesEnum.ADMIN)
			whereClause="w.field_id in (select uf.field_id from usersfields uf where uf.user_id ="+user.getId()+")";
    	else
    		whereClause="1=1";
 	
		// added the 1st field cl.id
		String queryStr="select c1.id as id, c1.name as name, p.nodata as nodata, p.lowflow as lowflow, p.noflow as noflow, p.pumpstuck as pumpstuck, p.electricfailure as electricfailure, p.clientdecision as clientdecision from crosstab('\n" + 
    			"\n" + 
    			"select c.id as contract_id, l.pullreason as pullreason, count(l.pullreason) \n" + 
    			"from lcevents l\n" + 
    			"inner join\n" + 
    			"installations i\n" + 
    			"on l.installation_id=i.id\n" + 
    			"inner join wells w\n" + 
    			"on w.id=i.well_id\n" + 
    			"inner join fields f\n" + 
    			"on f.id=w.field_id\n" +
    			"inner join contracts c\n" +
    			"on c.id=i.contract_id\n" +
    			"where l.eventtype=1 and f.country=''"+country+"''"+"and "+whereClause+"\n" + 
    			"group by c.id,l.pullreason\n" + 
    			"order by 1,2\n" + 
    			"','\n" + 
    			"select pr from generate_series(0,5) pr\n"+  
    			"')\n" + 
    			"\n" + 
    			"as p(\n" + 
    			"\"contract_id\" int,\n" + 
    			"\"nodata\" int,\n" + 
    			"\"lowflow\" int,\n" + 
    			"\"noflow\" int,\n" + 
    			"\"pumpstuck\" int,\n" + 
    			"\"electricfailure\" int,\n" + 
    			"\"clientdecision\" int\n" + 
    			"\n" + 
    			") inner join contracts c1 on c1.id=p.contract_id;";
		
		
		
    	Query query;
    	query=em.createNativeQuery(queryStr);
    	
//    	query.setParameter("country",country);
    	@SuppressWarnings("unchecked")
		List<String[]> results=query.getResultList();
		String out;
		
		
		// Also here is added the 1st field Id
		out="Id,Contract,\"No data\",\"Low flow\",\"No flow\",\"Pump stuck\",\"Electric failure\",\"Client decision\"\n";
		for (Object[] result : results){
    		

			out=out+result[0]+","+result[1]+",";
			if (result[2] != null)
				out=out+result[2]+",";
			else
				out=out+",";
				
			if (result[3] != null)
				out=out+result[3]+",";
			else
				out=out+",";
			
			if (result[4] != null)
				out=out+result[4]+",";
			else
				out=out+",";
			
			if (result[5] != null)
				out=out+result[5]+",";
			else
				out=out+",";
			
			if (result[6] != null)
				out=out+result[6]+",";
			else
				out=out+",";
			
			if (result[7] != null)
				out=out+result[7]+"\n";
			else
				out=out+"\n";
			
		}
		
		
		response.setContentType("text/csv");
    	response.getOutputStream().print(out);
    	response.getOutputStream().flush();
    	
    }
    
    @RequestMapping(value = "/pullreasons4countrypie",method = RequestMethod.GET)
    public void pullReasons4CountryPie(Principal principal, @RequestParam("country") String country,HttpServletResponse response) throws IOException{
    
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	String whereClause;
		if (user.getRole()!=UserRolesEnum.ADMIN)
			whereClause="w.field_id in (select uf.field_id from usersfields uf where uf.user_id ="+user.getId()+")";
    	else
    		whereClause="1=1";
 	
		// added the 1st field cl.id
		String queryStr="\n"+
    			"select l.pullreason, count(l.pullreason) \n" + 
    			"from lcevents l\n" + 
    			"inner join\n" + 
    			"installations i\n" + 
    			"on l.installation_id=i.id\n" + 
    			"inner join wells w\n" + 
    			"on w.id=i.well_id\n" + 
    			"inner join fields f\n" + 
    			"on f.id=w.field_id\n" +
    			"inner join contracts c\n" +
    			"on c.id=i.contract_id\n" +
    			"where l.eventtype=1 and f.country='"+country+"'"+"and "+whereClause+"\n" + 
    			"group by l.pullreason\n" + 
    			"order by 1\n";
    			
		
		
		
    	Query query;
    	query=em.createNativeQuery(queryStr);
    	
//    	query.setParameter("country",country);
    	@SuppressWarnings("unchecked")
		List<String[]> results=query.getResultList();
		String out;
		
		
		// Also here is added the 1st field Id
		out="pullreason,qty\n";
		for (Object[] result : results)
			out=out+"\""+Reasons4PullEnum.values()[(int) result[0]].getLabel()+"\","+result[1]+"\n";
		
		
		response.setContentType("text/csv");
    	response.getOutputStream().print(out);
    	response.getOutputStream().flush();
    	
    }
    
    @RequestMapping(value = "/teardownsstat",method = RequestMethod.GET)
    public void teardownStat(Principal principal,@RequestParam("country") String country, HttpServletResponse response) throws IOException{
    
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	String whereClause;
		if (user.getRole()!=UserRolesEnum.ADMIN)
			whereClause="w.field_id in (select uf.field_id from usersfields uf where uf.user_id ="+user.getId()+")";
    	else
    		whereClause="1=1";
    	
    	
		String queryStr="\n"+
				"select c.name as contract_name, f.name as field_name, extract(year from ls.eventdate) as year, lt.rca_rootcause as rootcause, lt.rca_responsibility as responsible, lt.rca_fcomponent as fcomp, count(i.id) as qty\n" + 
    			"from lcevents ls\n" + 
    			"inner join\n" + 
    			"installations i\n" + 
    			"on ls.installation_id=i.id and ls.eventtype=1 and ls.pullreason<>5\n" + 
    			"inner join wells w\n" + 
    			"on w.id=i.well_id\n" + 
    			"inner join fields f\n" + 
    			"on f.id=w.field_id\n" +
    			"inner join contracts c\n" +
    			"on c.id=i.contract_id\n" +
    			"left outer join lcevents lt on lt.installation_id=i.id and lt.eventtype=3\n"+
    			"where 1=1 and f.country=:country and "+whereClause+"\n" + 
    			"group by c.name, f.name, year, rootcause, responsible, fcomp\n"+
    			"order by year desc";
		
		
		
    	Query query;
    	query=em.createNativeQuery(queryStr);
    	query.setParameter("country",country);
    	
    	@SuppressWarnings("unchecked")
		List<String[]> results=query.getResultList();
		String out;
		
		
		// Also here is added the 1st field Id
		out="Contract,Field,Year,Rootcause,Responsibility,Fcomponent,Qty\n";
		for (Object[] result : results){
    		out=out+result[0]+","+result[1]+","+result[2]+","+ 
    				(result[3] != null ? FailureCausesEnum.values()[(int) result[3]].getLabel() : "No teardown report")+","+
    				(result[4] != null ? FailureResponsibilityEnum.values()[(short) result[4]].getLabel() : "No teardown report")+","+
    				(result[5] != null ? FailuredComponentEnum.values()[(int) result[5]].getLabel() : "No teardown report")+","+
    				result[6]+"\n";			
		}
		
		
		response.setContentType("text/csv");
    	response.getOutputStream().print(out);
    	response.getOutputStream().flush();
    	
    }
    
   
}