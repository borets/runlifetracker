/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import ru.shaposhnik.runlifetracker.model.LcEventStart;
import ru.shaposhnik.runlifetracker.model.LcEventStop;
import ru.shaposhnik.runlifetracker.model.LcEventPull;
import ru.shaposhnik.runlifetracker.model.LcEventTeardown;
import ru.shaposhnik.runlifetracker.model.LcEventRca;
import ru.shaposhnik.runlifetracker.model.LcEventSensor;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.FailureCausesEnum;
import ru.shaposhnik.runlifetracker.model.FailuredComponentEnum;
import ru.shaposhnik.runlifetracker.model.Installation;
import ru.shaposhnik.runlifetracker.model.LcEvent;
import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;
import ru.shaposhnik.runlifetracker.exceptions.UnauthorizedException;
import ru.shaposhnik.runlifetracker.repository.InstallationRepository;
import ru.shaposhnik.runlifetracker.repository.LcEventRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class LcEventController {

    private final InstallationRepository installationRepository;
    private final LcEventRepository lcEventRepository;


    @Autowired
    public LcEventController(InstallationRepository ir, LcEventRepository lr) {
        this.installationRepository=ir;
        this.lcEventRepository=lr;
    }

    
// ================ START EVENT METHODS ============================================================
    
    @RequestMapping(value = "/installations/{installation_id}/startevent", method = RequestMethod.GET)
    public String initStartEventForm(@PathVariable int installation_id, Model model) {
    	Installation installation=this.installationRepository.findInstallationById(installation_id);
    	
    	
    	LcEvent event = installation.getStartEvent();
    	if (event == null) {
    		event=new LcEventStart();
    		event.setInstallation(installation);
    	}
    		
  		model.addAttribute("event", event);
    	return "event-start";
    }
    
    @RequestMapping(value = "/installations/startevent", params = {"save"}, method = RequestMethod.POST)
    public String startEventSave(Principal principal,@ModelAttribute("event") @Valid LcEvent event, final BindingResult bindingResult, Model model) 
    			throws UnauthorizedException {
    	
    	if (bindingResult.hasErrors()) {
    		model.addAttribute("event",event);
    		return "event-start";
        } else {
        	User user = (User) ((Authentication) principal).getPrincipal();
        	    	
	    	try {
	    		lcEventRepository.saveLcEvent(event,user);
	    	} catch (LogicValidationException exception){
	    		bindingResult.rejectValue("eventDate", "eventDate","The date is incorrect");
	    		model.addAttribute("event",event);
	    		return "event-start";
	    	}
	    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
        }  
    }
    
    @RequestMapping(value = "/installations/startevent", params = {"delete"}, method = RequestMethod.POST)
    public String startEventDelete(Principal principal,@ModelAttribute("event") @Valid LcEvent event, final BindingResult bindingResult, Model model)
    throws UnauthorizedException {
    	User user = (User) ((Authentication) principal).getPrincipal();  	
    	try {
    		lcEventRepository.deleteLcEvent(event, user);	
    	} catch (LogicValidationException exception){
    		bindingResult.rejectValue("eventDate", "eventDate","The date is incorrect");
    		model.addAttribute("event",event);

    		return "event-start";
    	}

    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
    }

// ================ SENSOR EVENT METHODS ============================================================
    
    @RequestMapping(value = "/installations/{installation_id}/sensorevent", method = RequestMethod.GET)
    public String initSensorEventForm(@PathVariable int installation_id, Model model) {
    	Installation installation=this.installationRepository.findInstallationById(installation_id);
    	
    	
    	LcEvent event = installation.getSensorEvent();
    	if (event == null) {
    		event=new LcEventSensor();
    		event.setInstallation(installation);
    	}
    		
  		model.addAttribute("event", event);
    	return "event-sensorfailure";
    }
    
    @RequestMapping(value = "/installations/sensorevent", params = {"save"}, method = RequestMethod.POST)
    public String sensorEventSave(Principal principal,@ModelAttribute("event") @Valid LcEvent event, final BindingResult bindingResult, Model model) 
    			throws UnauthorizedException {
    	
    	if (bindingResult.hasErrors()) {
    		model.addAttribute("event",event);
    		return "event-sensorfailure";
        } else {
        	User user = (User) ((Authentication) principal).getPrincipal();
        	    	
	    	try {
	    		lcEventRepository.saveLcEvent(event,user);
	    	} catch (LogicValidationException exception){
	    		bindingResult.rejectValue("eventDate", "eventDate","The date is incorrect");
	    		model.addAttribute("event",event);
	    		return "event-sensorfailure";
	    	}
	    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
        }  
    }
    
    @RequestMapping(value = "/installations/sensorevent", params = {"delete"}, method = RequestMethod.POST)
    public String sensorEventDelete(Principal principal,@ModelAttribute("event") @Valid LcEvent event, final BindingResult bindingResult, Model model)
    throws UnauthorizedException {
    	User user = (User) ((Authentication) principal).getPrincipal();  	
    	try {
    		lcEventRepository.deleteLcEvent(event, user);	
    	} catch (LogicValidationException exception){
    		bindingResult.rejectValue("eventDate", "eventDate","The date is incorrect");
    		model.addAttribute("event",event);

    		return "event-sensorfailure";
    	}

    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
    }

 
    
    
// ================ STOP EVENT METHODS ============================================================
    
    @RequestMapping(value = "/installations/{installation_id}/stopevent", method = RequestMethod.GET)
    public String initStopEventForm(@PathVariable int installation_id, Model model) {
    	Installation installation=this.installationRepository.findInstallationById(installation_id);
    	LcEvent event = installation.getStopEvent();
    	if (event == null) {
    		event=new LcEventStop();
     		event.setInstallation(installation);
    	}
    		
  		model.addAttribute("event", event);
    	return "event-stop";
    }
      
    
    @RequestMapping(value = "/installations/stopevent", params = {"save"}, method = RequestMethod.POST)
    public String stopEventSave(Principal principal,@ModelAttribute("event") @Valid LcEventStop event, final BindingResult bindingResult, Model model) 
    			throws UnauthorizedException {
    	
    	if (bindingResult.hasErrors()) {
    		model.addAttribute("event",event);
    		return "event-stop";
        } else {
        	User user = (User) ((Authentication) principal).getPrincipal();
        	    	
	    	try {
	    		lcEventRepository.saveLcEvent(event,user);
	    	} catch (LogicValidationException exception){
	    		bindingResult.rejectValue("eventDate", "eventDate",exception.getMessage());
	    		model.addAttribute("event",event);
	    		return "event-stop";
	    	}
	    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
        }  
    }
    
    @RequestMapping(value = "/installations/stopevent", params = {"delete"}, method = RequestMethod.POST)
    public String stopEventDelete(Principal principal,@ModelAttribute("event") @Valid LcEventStop event, final BindingResult bindingResult, Model model)
    throws UnauthorizedException {
    	User user = (User) ((Authentication) principal).getPrincipal();  	
    	try {
    		lcEventRepository.deleteLcEvent(event, user);	
    	} catch (LogicValidationException exception){
    		bindingResult.rejectValue("eventDate", "eventDate","The date is incorrect");
    		model.addAttribute("event",event);

    		return "event-stop";
    	}

    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
    }

    
   
    
// ================ PULL EVENT METHODS ============================================================
    
    @RequestMapping(value = "/installations/{installation_id}/pullevent", method = RequestMethod.GET)
    public String initPullEventForm(@PathVariable int installation_id, Model model) {
    	Installation installation=this.installationRepository.findInstallationById(installation_id);
    	LcEventPull event = (LcEventPull) installation.getPullEvent();
    	if (event == null) {
    		event=new LcEventPull();
    		event.setInstallation(installation);
    	}
    	
  		model.addAttribute("event", event);
    	return "event-pull";
    }
      
    
    @RequestMapping(value = "/installations/pullevent", params = {"save"}, method = RequestMethod.POST)
    public String pullEventSave(Principal principal,@ModelAttribute("event") @Valid LcEventPull event, final BindingResult bindingResult, Model model) 
    			throws UnauthorizedException {
    	
    	if (bindingResult.hasErrors()) {
    		model.addAttribute("event",event);
    		return "event-pull";
        } else {
        	User user = (User) ((Authentication) principal).getPrincipal();
        	    	
	    	try {
	    		lcEventRepository.saveLcEvent(event,user);
	    	} catch (LogicValidationException exception){
	    		bindingResult.rejectValue("eventDate", "eventDate","The date is incorrect");
	    		model.addAttribute("event",event);
	    		return "event-pull";
	    	}
	    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
        }  
    }
    
    @RequestMapping(value = "/installations/pullevent", params = {"delete"}, method = RequestMethod.POST)
    public String pullEventDelete(Principal principal,@ModelAttribute("event") @Valid LcEventPull event, final BindingResult bindingResult, Model model)
    throws UnauthorizedException {
    	User user = (User) ((Authentication) principal).getPrincipal();  	
    	try {
    		lcEventRepository.deleteLcEvent(event, user);	
    	} catch (LogicValidationException exception){
    		bindingResult.rejectValue("eventDate", "eventDate","The date is incorrect");
    		model.addAttribute("event",event);

    		return "event-pull";
    	}

    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
    }
    
    
// ================ Teardown EVENT METHODS ============================================================
    
    @RequestMapping(value = "/installations/{installation_id}/teardownevent", method = RequestMethod.GET)
    public String initRCAEventForm(@PathVariable int installation_id, Model model) {
    	Installation installation=this.installationRepository.findInstallationById(installation_id);
    	LcEventTeardown event = installation.getTeardownEvent();
    	if (event == null) {
    		event=new LcEventTeardown();
    		event.setInstallation(installation);
    	}

    	
      	Map<String, String> subRootCauses=new HashMap<String, String>();
      	for (FailureCausesEnum fc : FailureCausesEnum.values()){
      		subRootCauses.put(fc.name(),fc.getSubRootCauses());
      	}
      	model.addAttribute("subRootCauses",subRootCauses);

    	
    	Map<String, String> subcomponents=new HashMap<String, String>();
      	for (FailuredComponentEnum fc : FailuredComponentEnum.values()){
      		subcomponents.put(fc.name(),fc.getSubComponents());
      	}    	
      	model.addAttribute("subcomponents",subcomponents);
    	
      	
  		model.addAttribute("event", event);
    	return "event-teardown";
    }
      
    
    @RequestMapping(value = "/installations/teardownevent", params = {"save"}, method = RequestMethod.POST)
    public String RCAEventSave(Principal principal,@ModelAttribute("event") @Valid LcEventTeardown event, final BindingResult bindingResult, Model model) 
    			throws UnauthorizedException {
    	
    	if (bindingResult.hasErrors()) {
    		model.addAttribute("event",event);
    		
    		Map<String, String> subRootCauses=new HashMap<String, String>();
          	for (FailureCausesEnum fc : FailureCausesEnum.values()){
          		subRootCauses.put(fc.name(),fc.getSubRootCauses());
          	}
          	model.addAttribute("subRootCauses",subRootCauses);
    		
    		Map<String, String> subcomponents=new HashMap<String, String>();
          	for (FailuredComponentEnum fc : FailuredComponentEnum.values()){
          		subcomponents.put(fc.name(),fc.getSubComponents());
          	}
          	model.addAttribute("subcomponents",subcomponents);
    		
    		
    		return "event-teardown";
        } else {
        	User user = (User) ((Authentication) principal).getPrincipal();
        	    	
	    	try {
	    		lcEventRepository.saveLcEvent(event,user);
	    	} catch (LogicValidationException exception){
	    		bindingResult.rejectValue("eventDate", "eventDate",exception.getMessage());
	    		model.addAttribute("event",event);

	    		Map<String, String> subRootCauses=new HashMap<String, String>();
	          	for (FailureCausesEnum fc : FailureCausesEnum.values()){
	          		subRootCauses.put(fc.name(),fc.getSubRootCauses());
	          	}
	          	model.addAttribute("subRootCauses",subRootCauses);
	    		
	    		Map<String, String> subcomponents=new HashMap<String, String>();
	          	for (FailuredComponentEnum fc : FailuredComponentEnum.values()){
	          		subcomponents.put(fc.name(),fc.getSubComponents());
	          	}
	          	model.addAttribute("subcomponents",subcomponents);
	    		
	    			
	    		return "event-teardown";
	    	}
	    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
        }  
    }
    
    @RequestMapping(value = "/installations/rcaevent", params = {"delete"}, method = RequestMethod.POST)
    public String RCAEventDelete(Principal principal,@ModelAttribute("event") @Valid LcEventTeardown event, final BindingResult bindingResult, Model model)
    throws UnauthorizedException {
    	User user = (User) ((Authentication) principal).getPrincipal();  	
    	try {
    		lcEventRepository.deleteLcEvent(event, user);	
    	} catch (LogicValidationException exception){
    		bindingResult.rejectValue("eventDate", "eventDate","The date is incorrect");
    		model.addAttribute("event",event);

    		return "event-teardonw";
    	}

    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
    }
    
// ================ RCA EVENT METHODS ============================================================
    
    @RequestMapping(value = "/installations/{installation_id}/rcaevent", method = RequestMethod.GET)
    public String initClientEventForm(@PathVariable int installation_id, Model model) {
    	Installation installation=this.installationRepository.findInstallationById(installation_id);
    	LcEventRca event = installation.getRcaEvent();
    	
    	String[] ftypes=installation.getContract().getFailureTypes().split("/",10);
    	model.addAttribute("ftypes",ftypes);
    	
    	if (event == null) {
    		event=new LcEventRca();
    		event.setInstallation(installation);
    	}
    		
  		model.addAttribute("event", event);
    	return "event-client";
    }
      
    
    @RequestMapping(value = "/installations/rcaevent", params = {"save"}, method = RequestMethod.POST)
    public String clientEventSave(Principal principal,@ModelAttribute("event") @Valid LcEventRca event, final BindingResult bindingResult, Model model) 
    			throws UnauthorizedException {
    	
    	if (bindingResult.hasErrors()) {
    		model.addAttribute("event",event);
    		return "event-rca";
        } else {
        	User user = (User) ((Authentication) principal).getPrincipal();
        	    	
	    	try {
	    		lcEventRepository.saveLcEvent(event,user);
	    	} catch (LogicValidationException exception){
	    		bindingResult.rejectValue("eventDate", "eventDate","The date is incorrect");
	    		model.addAttribute("event",event);
	    		return "event-rca";
	    	}
	    	return "redirect:/installations/"+event.getInstallation().getId()+"/edit";
        }  
    }
    
    @RequestMapping(value = "/installations/rcaevent", params = {"delete"}, method = RequestMethod.POST)
    public String clientEventDelete(Principal principal,@ModelAttribute("event") @Valid LcEventRca event, final BindingResult bindingResult, Model model)
    throws UnauthorizedException {
    	User user = (User) ((Authentication) principal).getPrincipal();  	
    	try {
    		lcEventRepository.deleteLcEvent(event, user);	
    	} catch (LogicValidationException exception){
    		bindingResult.rejectValue("eventDate", "eventDate","The date is incorrect");
    		model.addAttribute("event",event);

    		return "event-rca";
    	}

    	return "redirect:/installations/"+event.getInstallation().getId()+"/clientevent";
    }
        
    
}