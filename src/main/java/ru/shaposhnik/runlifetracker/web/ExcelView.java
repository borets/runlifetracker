package ru.shaposhnik.runlifetracker.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import ru.shaposhnik.runlifetracker.model.Installation;



public class ExcelView extends AbstractExcelView {
 @Override
 protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook,
   HttpServletRequest request, HttpServletResponse response)
   throws Exception {


  HSSFSheet sheet = workbook.createSheet("Installations");

  HSSFRow header = sheet.createRow(0);
  header.createCell(0).setCellValue("Country");
  header.createCell(1).setCellValue("Field");
  header.createCell(2).setCellValue("Field Type");
  header.createCell(3).setCellValue("Well");
  header.createCell(4).setCellValue("Contract");
  header.createCell(5).setCellValue("Contract start date");
  header.createCell(6).setCellValue("Pump type");   
  header.createCell(7).setCellValue("Installation date");
  header.createCell(8).setCellValue("Start date");
  header.createCell(9).setCellValue("Start note");
  
  header.createCell(10).setCellValue("Sensor failure date");
  header.createCell(11).setCellValue("Sensor failure note");
  
  header.createCell(12).setCellValue("Stop date");
  header.createCell(13).setCellValue("Stop/pull reason");
  header.createCell(14).setCellValue("Stop note");

  
  header.createCell(15).setCellValue("Pull date");
  header.createCell(16).setCellValue("Pull note");
  
  header.createCell(17).setCellValue("Teardown date");
  header.createCell(18).setCellValue("Failure root cause");
  header.createCell(19).setCellValue("Failure responsible");
  header.createCell(20).setCellValue("Failured component");
  header.createCell(21).setCellValue("Teardown note");
  
  header.createCell(22).setCellValue("Client recognition date"); 
  header.createCell(23).setCellValue("Client recognition");
  header.createCell(24).setCellValue("Client recognition note");

  header.createCell(25).setCellValue("Installation note");
  
  
  header.createCell(30).setCellValue("Casing OD");
  header.createCell(31).setCellValue("Casing weight");
  
  header.createCell(32).setCellValue("Well treatment");
  
  header.createCell(33).setCellValue("Reservoirs");
  header.createCell(34).setCellValue("Reservoir Temp,C");
  header.createCell(35).setCellValue("GOR,scf/stb");
  header.createCell(36).setCellValue("Watercut,%");
  header.createCell(37).setCellValue("Viscosity,cp");
  header.createCell(38).setCellValue("Solids content,mg/l");
  header.createCell(39).setCellValue("Oil gravity,API");
  header.createCell(40).setCellValue("Pump head setting depth,ft");

  
  header.createCell(41).setCellValue("Pumps Brand");
  header.createCell(42).setCellValue("Pumps Series");
  header.createCell(43).setCellValue("Pumps flow, bpd");
  header.createCell(44).setCellValue("Pumps qty");
  header.createCell(45).setCellValue("Stages qty");
  header.createCell(46).setCellValue("Stage flow type");
  header.createCell(47).setCellValue("Assembling type");
  header.createCell(48).setCellValue("Pump metallurgy");
  header.createCell(49).setCellValue("Abrasion resistance");
  header.createCell(50).setCellValue("Pumps serials");
  header.createCell(51).setCellValue("Vapro serial");
  header.createCell(52).setCellValue("Vapro description");
  header.createCell(53).setCellValue("Pumps note");

  header.createCell(54).setCellValue("EmptySlot1");
  header.createCell(55).setCellValue("EmptySlot2");

  
  
  header.createCell(56).setCellValue("UGS serial");
  header.createCell(57).setCellValue("UGS description");
  header.createCell(58).setCellValue("LGS serial");
  header.createCell(59).setCellValue("LGS description");
  header.createCell(60).setCellValue("Intake serial");
  header.createCell(61).setCellValue("Intake description");
  
  header.createCell(62).setCellValue("EmptySlot3");
  
  header.createCell(63).setCellValue("Seal series");
  header.createCell(64).setCellValue("Useal brand");
  header.createCell(65).setCellValue("Useal serial");
  header.createCell(66).setCellValue("Useal description");
  header.createCell(67).setCellValue("Lseal brand");
  header.createCell(68).setCellValue("Lseal serial");
  header.createCell(69).setCellValue("Lseal description");

  header.createCell(70).setCellValue("EmptySlot4");
  
  header.createCell(71).setCellValue("Motors brand");
  header.createCell(72).setCellValue("Motors type");
  header.createCell(73).setCellValue("Motors series");
  header.createCell(74).setCellValue("Motors qty");
  header.createCell(75).setCellValue("Motors total HP");
  header.createCell(76).setCellValue("Motors Amps");
  header.createCell(77).setCellValue("Motors Volts");
  header.createCell(78).setCellValue("Motors metallurgy");
  header.createCell(79).setCellValue("Upper motor serial");
  header.createCell(80).setCellValue("Lower motor serial");
  header.createCell(81).setCellValue("Motors note");
    
  header.createCell(82).setCellValue("EmptySlot5");
  
  header.createCell(83).setCellValue("Sensor brand");
  header.createCell(84).setCellValue("Senosr reusage");
  header.createCell(85).setCellValue("Sensor variables");
  header.createCell(86).setCellValue("Sensor serial");
  header.createCell(87).setCellValue("Sensor note");

  header.createCell(88).setCellValue("EmptySlot6");

  header.createCell(89).setCellValue("Cable brand");
  header.createCell(90).setCellValue("Cable reusage");
  header.createCell(91).setCellValue("Cable length,ft");
  header.createCell(92).setCellValue("Cable number");
  header.createCell(93).setCellValue("Cable armour");
  header.createCell(94).setCellValue("Cable captube");
  header.createCell(95).setCellValue("Cable note");

  
  
  

  int counter=1;
  @SuppressWarnings("unchecked")
  List<Installation> installations = (List<Installation>) model.get("allInstallations");
  
  HSSFCellStyle cellStyle=workbook.createCellStyle();
  HSSFDataFormat dataFormat = workbook.createDataFormat();
  cellStyle.setDataFormat(dataFormat.getFormat("yyyy-mm-dd"));
  
  HSSFCell cell;
  
  for (Installation i : installations){
	  HSSFRow row = sheet.createRow(counter++);
	
	  row.createCell(0).setCellValue(i.getWell().getField().getCountry());
	  row.createCell(1).setCellValue(i.getWell().getField().getName());
	  row.createCell(2).setCellValue(i.getWell().getField().getFieldType());
	  row.createCell(3).setCellValue(i.getWell().getName());

	  row.createCell(4).setCellValue(i.getContract().getName());
	  
	  cell=row.createCell(5);
	  cell.setCellValue(i.getContract().getStartingDate());
	  cell.setCellStyle(cellStyle);
	  
	  row.createCell(6).setCellValue(i.getLifttype());
	  
	  cell=row.createCell(7);
	  cell.setCellValue(i.getEventDate());
	  cell.setCellStyle(cellStyle);
	  
	  
	  if (i.getStartEvent() != null){
		  cell=row.createCell(8);
		  cell.setCellValue(i.getStartEvent().getEventDate());
		  cell.setCellStyle(cellStyle);
		  
		  row.createCell(9).setCellValue(i.getStartEvent().getNote());
	  }
	  if (i.getSensorEvent() != null){
		  cell=row.createCell(10);
		  cell.setCellValue(i.getSensorEvent().getEventDate());
		  cell.setCellStyle(cellStyle);
		  
		  row.createCell(11).setCellValue(i.getSensorEvent().getNote());
	  }
	  
	  if (i.getStopEvent() != null){
		  cell=row.createCell(12);
		  cell.setCellValue(i.getStopEvent().getEventDate());
		  cell.setCellStyle(cellStyle);
		  
		  row.createCell(13).setCellValue(i.getStopEvent().getPullReason().getLabel());			  
		  row.createCell(14).setCellValue(i.getStopEvent().getNote());
	  }
	  
	  if (i.getPullEvent() != null){
		  cell=row.createCell(15);
		  cell.setCellValue(i.getPullEvent().getEventDate());
		  cell.setCellStyle(cellStyle);

		  row.createCell(16).setCellValue(i.getPullEvent().getNote());

	  }
	  if (i.getTeardownEvent() != null){
		  cell=row.createCell(17);
		  cell.setCellValue(i.getTeardownEvent().getEventDate());
		  cell.setCellStyle(cellStyle);
		  
		  if ((i.getTeardownEvent()).getRootCause() != null)
			  row.createCell(18).setCellValue((i.getTeardownEvent()).getRootCause().getLabel());
		  if ((i.getTeardownEvent()).getResponsibility() != null)
			  row.createCell(19).setCellValue((i.getTeardownEvent()).getResponsibility().getLabel());
		  if ((i.getTeardownEvent()).getFailuredComponent() != null)
			  row.createCell(20).setCellValue((i.getTeardownEvent()).getFailuredComponent().getLabel());
		  row.createCell(21).setCellValue(i.getRcaEvent().getNote());
	  }
	  if (i.getRcaEvent() != null){
		  cell=row.createCell(22);
		  cell.setCellValue(i.getRcaEvent().getEventDate());
		  cell.setCellStyle(cellStyle);
		  
		  row.createCell(23).setCellValue((i.getRcaEvent()).getRcaResult());
		  row.createCell(24).setCellValue(i.getRcaEvent().getNote());
	  }
	  row.createCell(25).setCellValue(i.getNote());
	  
	  
	  row.createCell(30).setCellValue(i.getWell().getCasingOD());
	  if(i.getWell().getCasingWeight() != null)
		  row.createCell(31).setCellValue(i.getWell().getCasingWeight());
	  
	  
	  // Installation general data
	  if (i.getWellTreatment() != null)
		  row.createCell(32).setCellValue(i.getWellTreatment());

	  row.createCell(33).setCellValue(i.getReservoirs());
	  if(i.getReservoirTemp() != null)
		  row.createCell(34).setCellValue(i.getReservoirTemp());
	  if (i.getGor() != null)
		  row.createCell(35).setCellValue(i.getGor());
	  if (i.getWaterCut() != null)
		  row.createCell(36).setCellValue(i.getWaterCut());
	  if (i.getViscosity() != null)
		  row.createCell(37).setCellValue(i.getViscosity());
	  if (i.getSolidsContent() != null)
		  row.createCell(38).setCellValue(i.getSolidsContent());
	  if (i.getOilGravity() != null)
		  row.createCell(39).setCellValue(i.getOilGravity());
	  if (i.getPHeadSetDepth() != null)
		  row.createCell(40).setCellValue(i.getPHeadSetDepth());

//	  if (i.getGor() != null)
//		  row.createCell(26).setCellValue(i.getGor());


	  
	  // Pumps data
	  if (i.getPumps() != null){
		  row.createCell(41).setCellValue(i.getPumps().getBrand());
		  row.createCell(42).setCellValue(i.getPumps().getSeries());
		  if (i.getPumps().getPumpsFlow() != null)
			  row.createCell(43).setCellValue(i.getPumps().getPumpsFlow());
		  if (i.getPumps().getQty() != null)
			  row.createCell(44).setCellValue(i.getPumps().getQty());
		  if (i.getPumps().getStagesQ() != null)
			  row.createCell(45).setCellValue(i.getPumps().getStagesQ());
		  row.createCell(46).setCellValue(i.getPumps().getFlowType());
		  row.createCell(47).setCellValue(i.getPumps().getAssemblingType());
		  row.createCell(48).setCellValue(i.getPumps().getMetallurgy());
		  row.createCell(49).setCellValue(i.getPumps().getAbrasionRes());
		  row.createCell(50).setCellValue(i.getPumps().getPumpSerials());
		  row.createCell(51).setCellValue(i.getPumps().getVaproSerial());
		  row.createCell(52).setCellValue(i.getPumps().getVaproDescription());
		  row.createCell(53).setCellValue(i.getPumps().getNote());
	  }
	  // GS/Intake data
	  if (i.getGsi() != null){
		  row.createCell(56).setCellValue(i.getGsi().getUGSSerial());
		  row.createCell(57).setCellValue(i.getGsi().getUGSDescription());
		  row.createCell(58).setCellValue(i.getGsi().getLGSSerial());
		  row.createCell(59).setCellValue(i.getGsi().getLGSDescription());
		  row.createCell(60).setCellValue(i.getGsi().getIntSerial());
		  row.createCell(61).setCellValue(i.getGsi().getIntDescription());
	  }
	  
	  // Seals data
	  if (i.getSeals() != null){
		  row.createCell(63).setCellValue(i.getSeals().getSeries());
		  row.createCell(64).setCellValue(i.getSeals().getUSealBrand());
		  row.createCell(65).setCellValue(i.getSeals().getUSealSerial());
		  row.createCell(66).setCellValue(i.getSeals().getUSealDescription());
		  row.createCell(67).setCellValue(i.getSeals().getLSealBrand());
		  row.createCell(68).setCellValue(i.getSeals().getLSealSerial());
		  row.createCell(69).setCellValue(i.getSeals().getLSealDescription());
	  }
	  
	  
	  // Motors data
	  if (i.getMotors() != null){
		  row.createCell(71).setCellValue(i.getMotors().getBrand());
		  row.createCell(72).setCellValue(i.getMotors().getType());
		  row.createCell(73).setCellValue(i.getMotors().getSeries());
		  if (i.getMotors().getQty() != null)
			  row.createCell(74).setCellValue(i.getMotors().getQty());
		  if (i.getMotors().getHp() != null)
			  row.createCell(75).setCellValue(i.getMotors().getHp());
		  if (i.getMotors().getAmps() != null)
			  row.createCell(76).setCellValue(i.getMotors().getAmps());
		  if (i.getMotors().getVolts() != null)
			  row.createCell(77).setCellValue(i.getMotors().getVolts());
		  row.createCell(78).setCellValue(i.getMotors().getMetallurgy());
		  row.createCell(79).setCellValue(i.getMotors().getUMotorSerial());
		  row.createCell(80).setCellValue(i.getMotors().getLMotorSerial());
		  row.createCell(81).setCellValue(i.getMotors().getNote());
	  }
	  
	  // Sensor  data
	  if (i.getSensor() != null){
		  row.createCell(83).setCellValue(i.getSensor().getBrand());
		  row.createCell(84).setCellValue(i.getSensor().getReuse());
		  row.createCell(85).setCellValue(i.getSensor().getVariables());
		  row.createCell(86).setCellValue(i.getSensor().getSn());
		  row.createCell(87).setCellValue(i.getSensor().getNote());
	  }
	  
	  // Cable data
	  if (i.getCable() != null){
		  row.createCell(89).setCellValue(i.getCable().getBrand());
		  row.createCell(90).setCellValue(i.getCable().getReuse());
		  if (i.getCable().getLength() != null)
			  row.createCell(91).setCellValue(i.getCable().getLength());
		  row.createCell(92).setCellValue(i.getCable().getXarea());
		  row.createCell(93).setCellValue(i.getCable().getArmour());
		  row.createCell(94).setCellValue(i.getCable().getIsConcaptube());
		  row.createCell(95).setCellValue(i.getCable().getNote());
	  }
	  
	  
  }
  
 
 }


}