/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import ru.shaposhnik.runlifetracker.model.Installation;
import ru.shaposhnik.runlifetracker.repository.FieldTechnicianRepository;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

// http://fruzenshtein.com/spring-mvc-validator-initbinder/

@Component
public class InstallationValidator implements Validator {

	@Override
    public boolean supports(Class<?> clazz) {
        return Installation.class.isAssignableFrom(clazz);
    }
	
	private final FieldTechnicianRepository fieldTechnicianRepository;
	
	@Autowired
	InstallationValidator(FieldTechnicianRepository tr){
		this.fieldTechnicianRepository=tr;
	}
	
	@Override
    public void validate(Object obj, Errors errors) {
    	
    	Installation installation = (Installation) obj; 
    	
    	
    	if (fieldTechnicianRepository.query4Country(installation.getWell().getField().getCountry()).isEmpty())
    		return;
    	
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    	Date cutDate = null;
		try {
			cutDate = formatter.parse("2015-10-31");
		} catch (ParseException e) {
			System.out.println("ERROR parse data");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	System.out.println("Cut date: "+cutDate);
    	System.out.println(installation.getEventDate().after(cutDate));
    	if ((installation.getId() == 0 || installation.getEventDate().after(cutDate)) && installation.getFieldTechnician() == null)
    		errors.rejectValue("fieldTechnician", "fieldTechnician.isEmpty", "Please fill in the field technician field");  
    		
    	  
    }


	
}
