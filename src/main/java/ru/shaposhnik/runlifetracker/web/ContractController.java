package ru.shaposhnik.runlifetracker.web;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import ru.shaposhnik.runlifetracker.model.Contract;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.repository.ClientRepository;
import ru.shaposhnik.runlifetracker.repository.ContractRepository;
import ru.shaposhnik.runlifetracker.repository.FieldRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class ContractController {

    private final ContractRepository contractRepository;
    private final FieldRepository fieldRepository;
    private final ClientRepository clientRepository;

    @PersistenceContext
	private EntityManager em;
    
    
    @Autowired
    public ContractController(ContractRepository cr, FieldRepository fr, ClientRepository clr) {
        this.contractRepository = cr;
        this.fieldRepository=fr;
        this.clientRepository=clr;
    }


    
    @RequestMapping(value = "/contracts",method = RequestMethod.GET)
    public String listContracts(Model model) {
    	model.addAttribute("allContracts", this.contractRepository.queryAll());
    	model.addAttribute("allClients", this.clientRepository.queryAll());
    	
    	Contract contract = new Contract();
    	model.addAttribute("contract",contract);
    	
        return "contracts";
        
    }
    
    @RequestMapping(value = "/contracts/save", method = RequestMethod.POST)
    public String saveContract(@ModelAttribute("contract") @Valid final Contract contract, final BindingResult bindingResult, final ModelMap model) {
    	// from Petclinic: the validator is executed manually in addition to default validator
    	// another approach: http://stackoverflow.com/questions/25075683/spring-mvc-validator-annotation-custom-validation
    	
 //   	userValidator.validate(user, bindingResult);
    	if (bindingResult.hasErrors()) {
    	
    		model.addAttribute("contract",contract);
    		model.addAttribute("allContracts", this.contractRepository.queryAll());
    		model.addAttribute("allClients", this.clientRepository.queryAll());
        	    		
    		return "contracts";
        }
    	 
    	if (contract.getId()!= 0){
    		// Saving the link to fields
    		contract.setFields(contractRepository.findContractById(contract.getId()).getFields());
    	}
    	this.contractRepository.saveContract(contract);
        return "redirect:/contracts";
        
    }    
    
    
    @RequestMapping(value = "/contracts/{id}/fields")
    public String showFields(@PathVariable int id, Model model) {
    	model.addAttribute("allContracts", this.contractRepository.queryAll());
 
    	Contract contract=this.contractRepository.findContractById(id);
    	
    	
    	Map<Field, Boolean> fields=new LinkedHashMap<Field,Boolean>();

    	Query query=em.createNativeQuery("SELECT f.id as fid, fcontracts.id as cid FROM fields f LEFT OUTER JOIN (SELECT c.field_id, c.contract_id as id FROM contractsfields c WHERE c.contract_id=:id) as fcontracts ON f.id=fcontracts.field_id  ORDER BY f.country, f.name ASC");
    	query.setParameter("id", contract.getId());
    	@SuppressWarnings("unchecked")
		List<Object[]> results=query.getResultList();
    	for(Object[] result : results){
    		
    		Field field=fieldRepository.findFieldById((Integer) result[0]);
    		if((result[1] != null) && ((Integer) result[1] == contract.getId())){
     			fields.put(field, true);
    		} else {
     			fields.put(field, false);
    		}
    	}
    	
   
 //   	query.setParameter("id", contract.getId());
 //   	query.addEntity(Field.class);
   
    	model.addAttribute("contract",contract);
    	model.addAttribute("fields",fields);
  
    
        return "contract-fields";
        
        
    } 
    
    @RequestMapping(value = "/contracts/{id}/savefields", method = RequestMethod.POST)
    public String SaveFields(@PathVariable int id, @RequestParam HashMap<String,String> fields, ModelMap model) {
    
    	Contract contract=this.contractRepository.findContractById(id);
    	Set<Field> newFields=new HashSet<Field>();
    	
    	for(String s : fields.keySet()){
    		Field f=fieldRepository.findFieldById(Integer.decode(s));
    		newFields.add(f);
    	}
    	contract.setFields(newFields);
    	contractRepository.saveContract(contract);
    	
    	return "redirect:/contracts/";

    }
    
    
    
}