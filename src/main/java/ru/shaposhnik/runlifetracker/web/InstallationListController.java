/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.web;

import java.security.Principal;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import ru.shaposhnik.runlifetracker.model.Contract;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.model.LcEvent;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.Well;
import ru.shaposhnik.runlifetracker.repository.ContractRepository;
import ru.shaposhnik.runlifetracker.repository.FieldRepository;
import ru.shaposhnik.runlifetracker.repository.InstallationRepository;
import ru.shaposhnik.runlifetracker.repository.WellRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class InstallationListController {

    private final InstallationRepository installationRepository;
    private final WellRepository wellRepository;
    private final ContractRepository contractRepository;
    private final FieldRepository fieldRepository;
    
    @Autowired
    public InstallationListController(InstallationRepository ir, WellRepository wr, ContractRepository cr, FieldRepository fr) {
        this.installationRepository=ir;
        this.wellRepository=wr;
        this.contractRepository=cr;
        this.fieldRepository=fr;
    }
         
    @RequestMapping(value = "/installations",method = RequestMethod.GET)
    public String listInstallations(Principal principal,Model model) {
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	model.addAttribute("allInstallations", installationRepository.queryAll(user));
        return "installations";        
    }
     
    @RequestMapping(value = "/installations",params={"well"}, method=RequestMethod.GET)
    public String listInstallations4Well(Principal principal, Model model) {
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	model.addAttribute("allInstallations", installationRepository.queryAll(user));
        return "installations";        
    }
    
    @RequestMapping(value = "/wells/{well_id}/installations",method = RequestMethod.GET)
    public String listInstallations4Well2(Principal principal,@PathVariable int well_id, Model model) {
    	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	Well well=this.wellRepository.findWellById(well_id);
    	

    	model.addAttribute("allInstallations", installationRepository.queryAll(well,user));
        return "installations";        
    }
    
    
    @RequestMapping(value = "/installations/filter",method = RequestMethod.GET)
    public String listSelInstallations(Principal principal,
    					@RequestParam("eventType") Class<? extends LcEvent> eventType,
    					@RequestParam("period") String period,
    					@RequestParam("contractId") int contract_id,
    					@RequestParam("fieldId") int field_id,
    					@RequestParam("dateFrom") Date dateFrom,
    															Model model) {
    	
    	User user = (User) ((Authentication) principal).getPrincipal();

    	Contract contract = contractRepository.findContractById(contract_id);
    	Field field = fieldRepository.findFieldById(field_id);
    		
    		
    	Calendar cal =Calendar.getInstance();
    	cal.setTime(dateFrom);
    	switch (period){
    		case "month":
    			cal.add(Calendar.MONTH, 1);
    			break;
    		case "quarter":
    			cal.add(Calendar.MONTH, 3);
    			break;
    		case "year":
    			cal.add(Calendar.YEAR, 1);
    			break;
    		default:
    			break;
    	}
    	cal.add(Calendar.DATE, -1);
		
    	
    	
    	Date dateTo=cal.getTime();

    	
    	model.addAttribute("allInstallations",
    						installationRepository.querySelection(contract, field, eventType, dateFrom, dateTo, user));
        return "installations";        
    }
    
    
    @RequestMapping(value = "/installations/waiting",method = RequestMethod.GET)
    public String listWaitingInstallations(Principal principal,
    					@RequestParam("eventType") Class<? extends LcEvent> eventType,
    					@RequestParam("contractId") int contract_id,
    					@RequestParam("laFecha") Date laFecha,
 								Model model) {
    	
    	User user = (User) ((Authentication) principal).getPrincipal();

    	Contract contract = contractRepository.findContractById(contract_id);
    	
    	
    	
    	
    	model.addAttribute("allInstallations", installationRepository.queryWaiting4(contract, eventType, laFecha, user));
        return "installations";        
    }
    
    @RequestMapping("/installations.xls")
    public String getExcel(Principal principal, HttpServletRequest request, HttpServletResponse response, Model model){

    	User user = (User) ((Authentication) principal).getPrincipal();
    	model.addAttribute("allInstallations", installationRepository.queryAll(user));
    	
    	return "excelView";
    	
    	
    }

    
    
    
    
}