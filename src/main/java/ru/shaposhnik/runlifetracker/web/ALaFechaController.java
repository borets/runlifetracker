package ru.shaposhnik.runlifetracker.web;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.UserRolesEnum;
import ru.shaposhnik.runlifetracker.repository.InstallationRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class ALaFechaController {

    private final InstallationRepository installationRepository;

    @PersistenceContext
	private EntityManager em;
    
    
    @Autowired
    public ALaFechaController(InstallationRepository ir) {
        this.installationRepository = ir;

    }

    @RequestMapping(value = "/alafecha/init",method=RequestMethod.GET)
    public String init(Principal principal,Model model) {
 	
    	User user = (User) ((Authentication) principal).getPrincipal();
    	
    	List<String> allCountries = this.installationRepository.availableCountries(user);
    	model.addAttribute("allCountries", allCountries);
    	
    	return "alafecha";
    	
    }


    @RequestMapping(value = "/alafecha/csv",method=RequestMethod.GET)
    public void alafechaCsv(Principal principal,
    							@RequestParam("lafecha") Date laFecha,
    							@RequestParam("country") String country,
    							HttpServletResponse response) throws IOException {
    	
    	User user = (User) ((Authentication) principal).getPrincipal();

    	String whereClause="";
    	if (user.getRole()!=UserRolesEnum.ADMIN)
			whereClause="w.field_id in (select uf.field_id from usersfields uf where uf.user_id ="+user.getId()+")";
    	else
    		whereClause="1=1";
/*    	
    	String queryALaFechaStrOLD="select contracts.id, clients.name as clientname, contracts.name as contractname,\n" + 
    			"sum(case when i.instdate <= :lafecha and (starts.eventdate > :lafecha or starts.eventdate is null) then 1 end) as notstarted,\n" + 
    			"sum(case when starts.eventdate <= :lafecha and (stops.eventdate > :lafecha or stops.eventdate is null) then 1 end) as running,\n" + 
    			"sum(case when stops.eventdate <= :lafecha and (pulls.eventdate > :lafecha or pulls.eventdate is null) then 1 end) as waiting4pull,\n" + 
    			"sum(case when stops.pullreason < 5 and pulls.eventdate <= :lafecha and (rcas.eventdate > :lafecha or rcas.eventdate is null) then 1 end) as waiting4rca,\n" + 
    			"sum(case when stops.pullreason < 5 and rcas.eventdate <= :lafecha and (recognitions.eventdate > :lafecha or recognitions.eventdate is null) then 1 end) as notrecognized\n" + 
    			"\n" + 
    			"\n" + 
    			"from installations i\n" + 
    			"left outer join lcevents starts on (starts.installation_id = i.id and starts.eventtype = 0)\n" + 
    			"left outer join lcevents stops on (stops.installation_id = i.id and stops.eventtype = 1)\n" + 
    			"left outer join lcevents pulls on (pulls.installation_id = i.id and pulls.eventtype = 2)\n" + 
    			"left outer join lcevents rcas on (rcas.installation_id = i.id and rcas.eventtype = 3)\n" + 
    			"left outer join lcevents recognitions on (recognitions.installation_id = i.id and recognitions.eventtype=4)\n" + 
    			"inner join wells on wells.id = i.well_id\n" + 
    			"inner join contracts on contracts.id = i.contract_id\n" + 
    			"inner join clients on clients.id = contracts.client_id\n" + 
    			"where clients.country = :country and " + whereClause + "\n" + 
    			"group by contracts.id, clientname, contractname\n" + 
    			"order by clientname asc;\n";
*/    	
    	
    	String queryALaFechaStr=""
    			+ "select u.client_id, cl.name as clientName, u.contractId, u.contractName, u.fieldId, u.fieldName, u.status, u.Qty"
    			+ " FROM (" 
    			+ " select c.client_id, c.id as contractId, c.name as contractName, f.id as fieldId, f.name as fieldName, trim('') as status, count(i.id) as Qty"
    			+ " from installations i"
    			+ " left outer join lcevents l"
    			+ " on (i.id=l.installation_id and l.eventtype=0)"
    			+ " inner join contracts c"
    			+ " on c.id=i.contract_id"
    			+ " inner join wells w"
    			+ " on w.id=i.well_id"
    			+ " inner join fields f"
    			+ " on f.id=w.field_id"
    			+ " where i.instdate<=:lafecha and (l.eventdate>:lafecha or l.eventdate is null) and f.country=:country and "+whereClause
    			+ " group by c.client_id, c.id, c.name, f.id, f.name"
    			+ "\n"
    			+ "UNION ALL"
    			+ "\n"
    			+ " select c.client_id, c.id, c.name, f.id, f.name, trim('START'), count(i.id)"
    			+ " from installations i"
    			+ " inner join lcevents l1"
    			+ " on (i.id=l1.installation_id and l1.eventtype=0)"
    			+ " left outer join lcevents l2"
    			+ " on (i.id=l2.installation_id and l2.eventtype=1)"
    			+ " inner join contracts c"
    			+ " on c.id=i.contract_id"
    			+ " inner join wells w"
    			+ " on w.id=i.well_id"
    			+ " inner join fields f"
    			+ " on f.id=w.field_id"
    			+ " where l1.eventdate<=:lafecha and (l2.eventdate>:lafecha or l2.eventdate is null) and f.country=:country and "+whereClause
    			+ " group by c.client_id, c.id, c.name, f.id, f.name"
    			+ "\n"
				+ "UNION ALL"
				+ "\n"
				+ " select c.client_id, c.id, c.name, f.id, f.name, trim('STOP'), count(i.id)"
				+ " from installations i"
				+ " inner join lcevents l1"
				+ " on (i.id=l1.installation_id and l1.eventtype=1)"
				+ " left outer join lcevents l2"
				+ " on (i.id=l2.installation_id and l2.eventtype=2)"
				+ " inner join contracts c"
				+ " on c.id=i.contract_id"
				+ " inner join wells w"
				+ " on w.id=i.well_id"
				+ " inner join fields f"
				+ " on f.id=w.field_id"
				+ " where l1.eventdate<=:lafecha and (l2.eventdate>:lafecha or l2.eventdate is null) and f.country=:country and "+whereClause
				+ " group by c.client_id, c.id, c.name, f.id, f.name"
		    	+ "\n"
				+ "UNION ALL"
				+ "\n"
				+ " select c.client_id, c.id, c.name, f.id, f.name, trim('PULL'), count(i.id)"
				+ " from installations i"
				+ " inner join lcevents l1"
				+ " on (i.id=l1.installation_id and l1.eventtype=2)"
				+ " left outer join lcevents l2"
				+ " on (i.id=l2.installation_id and l2.eventtype=3)"
				+ " inner join lcevents failure"
				+ " on (i.id=failure.installation_id and failure.eventtype=1 and failure.pullreason<5)"
				+ " inner join contracts c"
				+ " on c.id=i.contract_id"
				+ " inner join wells w"
				+ " on w.id=i.well_id"
				+ " inner join fields f"
				+ " on f.id=w.field_id"
				+ " where l1.eventdate<=:lafecha and (l2.eventdate>:lafecha or l2.eventdate is null) and f.country=:country and "+whereClause
				+ " group by c.client_id, c.id, c.name, f.id, f.name"
				+ "\n"
				+ "UNION ALL"
				+ "\n"
				+ " select c.client_id, c.id, c.name, f.id, f.name, trim('RCA'), count(i.id)"
				+ " from installations i"
				+ " inner join lcevents l1"
				+ " on (i.id=l1.installation_id and l1.eventtype=3)"
				+ " left outer join lcevents l2"
				+ " on (i.id=l2.installation_id and l2.eventtype=4)"
				+ " inner join lcevents failure"
				+ " on (i.id=failure.installation_id and failure.eventtype=1 and failure.pullreason<5)"
				+ " inner join contracts c"
				+ " on c.id=i.contract_id"
				+ " inner join wells w"
				+ " on w.id=i.well_id"
				+ " inner join fields f"
				+ " on f.id=w.field_id"
				+ " where l1.eventdate<=:lafecha and (l2.eventdate>:lafecha or l2.eventdate is null) and f.country=:country and "+whereClause
				+ " group by c.client_id, c.id, c.name, f.id, f.name"
				+ ") as u"
				+ "\n"
				+ " inner join clients cl"
				+ " on cl.id=u.client_id";

		    	
    	Query queryALaFecha;
    	queryALaFecha=em.createNativeQuery(queryALaFechaStr);
    	
    	queryALaFecha.setParameter("country",country);
    	queryALaFecha.setParameter("lafecha", laFecha);
    	@SuppressWarnings("unchecked")
		List<String[]> results=queryALaFecha.getResultList();
		String out;
		
		out="ClientID,ClientName,ContractID,ContractName,FieldID,FieldName,Status,Qty\n";
		for (Object[] result : results){
			out=out+result[0]+","+result[1]+","+result[2]+","+result[3]+","+result[4]+","+result[5]+","+result[6]+","+result[7]+"\n";			
		}
			
		response.setContentType("text/csv");
		response.getOutputStream().print(out);
		response.getOutputStream().flush();
    	
    }
    
}

    