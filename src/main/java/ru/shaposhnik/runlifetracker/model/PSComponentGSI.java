package ru.shaposhnik.runlifetracker.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;


@Embeddable
public class PSComponentGSI  {

			
	@Column(name="ugsserial")
	String uGSSerial;
	@Size(max=15)
	public String getUGSSerial(){
		return this.uGSSerial;
	}
	public void setUGSSerial(String s){
		this.uGSSerial=s;
	}
	
	@Column(name="lgsserial")
	@Size(max=15)
	String lGSSerial;
	public String getLGSSerial(){
		return this.lGSSerial;
	}
	public void setLGSSerial(String s){
		this.lGSSerial=s;
	}
	
	@Column(name="intserial")
	@Size(max=15)
	String intSerial;
	public String getIntSerial(){
		return this.intSerial;
	}
	public void setIntSerial(String s){
		this.intSerial=s;
	}
	
	
	@Column(name="ugsdescription")
	@Size(max=30)
	String uGSDescription;
	public String getUGSDescription(){
		return this.uGSDescription;
	}
	public void setUGSDescription(String s){
		this.uGSDescription=s;
	}
	
	@Column(name="lGSdescription")
	@Size(max=30)
	String lGSDescription;
	public String getLGSDescription(){
		return this.lGSDescription;
	}
	public void setLGSDescription(String s){
		this.lGSDescription=s;
	}
	
	@Column(name="intdescription")
	@Size(max=30)
	String intDescription;
	public String getIntDescription(){
		return this.intDescription;
	}
	public void setIntDescription(String s){
		this.intDescription=s;
	}
		
	
	public String getCType(){
		return "GSI";
	}
	
	public PSComponentGSI(){
	}
}
