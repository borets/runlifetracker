package ru.shaposhnik.runlifetracker.model;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;


@Embeddable
public class PSComponentSeals  {

	
	@Column(name="sealsseries")
	@Size(max=10)
	String series;
	public void setSeries(String s){
		this.series=s;
	}
	public String getSeries(){
		return this.series;
	}
	
	
	@Column(name="usealbrand")
	@NotEmpty
	String uSealBrand;
	public void setUSealBrand(String s){
		this.uSealBrand=s;
	}
	public String getUSealBrand(){
		return this.uSealBrand;
	}
		
	@Column(name="lsealbrand")
	@Size(max=8)
	String lSealBrand;
	public void setLSealBrand(String s){
		this.lSealBrand=s;
	}
	public String getLSealBrand(){
		return this.lSealBrand;
	}
	
	
	@Column(name="usealserial")
	String uSealSerial;
	@Size(max=15)
	public String getUSealSerial(){
		return this.uSealSerial;
	}
	public void setUSealSerial(String s){
		this.uSealSerial=s;
	}
	
	@Column(name="lsealserial")
	@Size(max=15)
	String lSealSerial;
	public String getLSealSerial(){
		return this.lSealSerial;
	}
	public void setLSealSerial(String s){
		this.lSealSerial=s;
	}
	
	@Column(name="usealdescription")
	@Size(max=30)
	String uSealDescription;
	public String getUSealDescription(){
		return this.uSealDescription;
	}
	public void setUSealDescription(String s){
		this.uSealDescription=s;
	}
	
	@Column(name="lsealdescription")
	@Size(max=30)
	String lSealDescription;
	public String getLSealDescription(){
		return this.lSealDescription;
	}
	public void setLSealDescription(String s){
		this.lSealDescription=s;
	}
	
	
	public static Set<String> seriess=new LinkedHashSet<String>(Arrays.asList("375", "400", "513", "538", "136mm"));
	public static Set<String> brands = new LinkedHashSet<String>(Arrays.asList("BRT-NL", "BRT-OL", "SLB","BH", "GE", "ALK", "NLS"));
	
	
	public String getCType(){
		return "Seal";
	}
	
	public PSComponentSeals(){
	}
}
