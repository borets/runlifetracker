package ru.shaposhnik.runlifetracker.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CommonInfoService {

	static List<String> liftTypeList = new ArrayList<String>();
	static List<String> countryList = new ArrayList<String>();
	static List<String> espComponentTypeList = new ArrayList<String>();
	static List<String> wellStatusList =new ArrayList<String>();
	static Set<String> motorTypes = new HashSet<String>();
	static Set<String> corrosionResistance = new HashSet<String>();
	static Set<String> espBrand = new HashSet<String>();
	
	
	static{
		countryList.add("Colombia");
		countryList.add("Brazil");
		countryList.add("Mexico");
		countryList.add("Congo");
		countryList.add("Oman");
		countryList = Collections.unmodifiableList(countryList);
		
		
		liftTypeList.add("ESP");
		liftTypeList.add("PCP");
		liftTypeList = Collections.unmodifiableList(liftTypeList);
		
		espComponentTypeList.add("Pump");
		espComponentTypeList.add("Vapro");
		espComponentTypeList.add("Gas separator");
		espComponentTypeList.add("Intake");
		espComponentTypeList.add("Seal");
		espComponentTypeList.add("Motor");
		espComponentTypeList.add("Sensor");
		
		wellStatusList.add("Empty");
		wellStatusList.add("Installed");
		wellStatusList.add("Started");
		wellStatusList.add("Stopped");
		wellStatusList.add("Waiting for the pulling");
		
		motorTypes.add("Induction");
		motorTypes.add("PMM");
		
		corrosionResistance.add("CS");
		corrosionResistance.add("SS");
		corrosionResistance.add("Coated");
		
		espBrand.add("BRT");
		espBrand.add("SLB");
		espBrand.add("BH");
		espBrand.add("GE");
		espBrand.add("NVM");
		espBrand.add("ALK");
		
				
	}
	
	public static List<String> getCountryList() {
		return countryList;
	}
	
	public static List<String> getLiftTypeList() {
		return liftTypeList;
	}

	
	public static List<String> getEspComponentTypeList() {
		return espComponentTypeList;
	}
	
	
}
