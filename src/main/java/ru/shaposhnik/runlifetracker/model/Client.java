package ru.shaposhnik.runlifetracker.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;


@Entity
@Table(name="clients")
public class Client implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	
	
	@Column(name="name", nullable=false,length=32)
	@Size(min=3,max=20)
	String name;
	public String getName() {
		return name;
	}
	public void setName(String fieldname) {
		this.name = fieldname;
	}
	
	
	@Column(name="country", nullable=false,length=32)
	@Size(min=3,max=20)
	String country;	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name="taxid", nullable=false,length=32)
	@Size(min=3,max=20)
	String taxId;	
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String s) {
		this.taxId = s;
	}
	

	@OneToMany(mappedBy="client", fetch = FetchType.LAZY)
	Set<Contract> contracts;
	
	
	public Client(){}
	
	public Client(String name, String country) {
		this.name = name;
		this.country = country;
	}
	
}
