/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package ru.shaposhnik.runlifetracker.model;

import java.util.EnumSet;
import java.util.Set;


public enum LcEventTypesEnum {
	START(1, "Start"),
	STOP(2, "Stop"), 
	PULL(3, "Pull"), 
	RCA(4, "Teardown"), 
	CLIENT(5, "RCA"),
	SENSOR(6,"Sensor failure");

	static {
		START.roles=EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
		STOP.roles=EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
		PULL.roles=EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
		RCA.roles=EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.DIFAING);
		CLIENT.roles=EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
		SENSOR.roles=EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
		
	}
	
	
	
	private int lcEventTypesEnum;
	private String label;

	private EnumSet<UserRolesEnum> roles;
	
	private LcEventTypesEnum(int e, String l) {
		this.lcEventTypesEnum = e;
		this.label = l;
	}

	public int getLcEventTypesEnum() {
		return lcEventTypesEnum;
	}

	public String getLabel() {
		return label;
	}
	
	public Set<UserRolesEnum> getRoles(){
		return this.roles;
	}
	
	public LcEventTypesEnum getNextEvent(){
		return LcEventTypesEnum.values()[this.ordinal()+1];
		
	}
}