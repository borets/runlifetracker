package ru.shaposhnik.runlifetracker.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name="contracts")
public class Contract implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	
	@Column(name="name",nullable=false,length=8)
	@NotEmpty
	String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="startingdate",nullable=false,length=8)
	@Temporal(TemporalType.DATE)
	@NotNull
	Date startingDate;
	public Date getStartingDate() {
		return startingDate;
	}
	public void setStartingDate(Date d) {
		this.startingDate = d;
	}
	
		
	@ManyToOne
	@JoinColumn(name="client_id", nullable=false)
	Client client;
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	
	@Column(name="failuretypes",nullable=false)
	String failureTypes;
	public String getFailureTypes() {
		return this.failureTypes;
	}
	public void setFailureTypes(String s) {
		this.failureTypes = s;
	}
	
	
	@Column(name="isclosed",nullable=false)
	Boolean isClosed;
	public Boolean getIsClosed() {
		return this.isClosed;
	}
	public void setIsClosed(Boolean b) {
		this.isClosed = b;
	}
	
	
	
	@OneToMany(mappedBy="contract",fetch = FetchType.LAZY)
	Set<Installation> installation;
	
	public Contract(){}
	
	public Contract(String name, Client client) {
		this.name = name;
		this.client = client;
		this.isClosed=false;
	}
	

	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="contractsfields",
			    joinColumns=@JoinColumn(name="contract_id"),
			   	inverseJoinColumns=@JoinColumn(name="field_id"))
	Set<Field> fields;
	
	public Set<Field> getFields() {
		return fields;
	}
	public void setFields(Set<Field> fs) {
		this.fields = fs;
	}	

	public String getFieldsString(){
		
		String str=new String();
		for(Field f : fields){
			str=str+", "+f.getName();
    	}
		
		if (str.length() >= 1){
			str=str.substring(1);
		}
		return str;

	}
	
	public String getFullName(){
		return client.name+"-"+name;
	}
	
}
