package ru.shaposhnik.runlifetracker.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;


/**
 * User entity
 */
@Entity
@Table(name="appinstalledequipment")
public class InstalledEquipment implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Long id;
	
	
	@Column(nullable=false,length=20)
	String espComponentType;
	
	@Column(nullable=false,length=200)
	String equipmentDescr;
	
	
	@ManyToOne
	@JoinColumn(nullable=false)
	Installation installation;
	
	public InstalledEquipment(){}
	
	public InstalledEquipment(Installation installation, String equipmentType, String equipmentDescr) {
		this.installation=installation;
		this.espComponentType = equipmentType;		
		this.equipmentDescr = equipmentDescr;
	}
	
	public Long getId() {
		return id;
	}

	
	public void setEquipmentDescr(String equipmentDescr) {
		this.equipmentDescr=equipmentDescr;
	}
	public String getEquipmentDescr() {
		return equipmentDescr;
	}
		
	
	public void setEspComponentType(String espComponentType) {
		this.espComponentType=espComponentType;
	}
	public String getEspComponentType() {
		return espComponentType;
	}
	
	
	
	public void setInstallation(Installation installation) {
		this.installation = installation;
	}
	public Installation getInstallation() {
		return installation;
	}
}
