package ru.shaposhnik.runlifetracker.model;

import java.io.Serializable;
import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.joda.time.Days;

import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;

import org.joda.time.DateTime;


@Entity
@Table(name="installations")
public class Installation extends LcEvent {
	
	
	@ManyToOne
	@JoinColumn(name="well_id", nullable=false)
	Well well;
	public Well getWell() {
		return well;
	}
	public void setWell(Well well) {
		this.well = well;
	}
	
	
	@ManyToOne
	@JoinColumn(name="contract_id", nullable=false)
	@NotNull
	Contract contract;
	public Contract getContract() {
		return contract;
	}
	public void setContract(Contract c) {
		this.contract = c;
	}
	
	
	@Column (name="lifttype", nullable=false)
	@NotNull
	String lifttype;
	public String getLifttype() {
		return lifttype;
	}
	public void setLifttype(String systemtype) {
		this.lifttype = systemtype;
	}
	
	
	@Column (name="activitytype", nullable=false)
	@Min(1)
	Integer wellTreatment;
	public Integer getWellTreatment() {
		return wellTreatment;
	}
	public void setWellTreatment(Integer i) {
		this.wellTreatment = i;
	}
	
	@ManyToOne
	@JoinColumn(name="fieldtechnician_id")
	FieldTechnician fieldTechnician;
	public FieldTechnician getFieldTechnician() {
		return this.fieldTechnician;
	}
	public void setFieldTechnician(FieldTechnician t) {
		this.fieldTechnician = t;
	}
	
	
	@Column(name="reservoirs")
	String reservoirs;
	public String getReservoirs() {
		return reservoirs;
	}
	public void setReservoirs(String r) {
		this.reservoirs = r;
	}
	
		
	@Column(name="reservoirtemp")
	Double reservoirTemp;
	public Double getReservoirTemp() {
		return reservoirTemp;
	}
	public void setReservoirTemp(Double t) {
		this.reservoirTemp = t;
	}
	
	@Column(name="gor")
	Double gor;
	public Double getGor() {
		return gor;
	}
	public void setGor(Double r) {
		this.gor = r;
	}
	
	@Column(name="watercut")
	@NotNull
	@Min(0)
	@Max(100)
	Double waterCut;
	public Double getWaterCut() {
		return waterCut;
	}
	public void setWaterCut(Double w) {
		this.waterCut = w;
	}
	
	@Column(name="viscosity")
	Double viscosity;
	public Double getViscosity() {
		return viscosity;
	}
	public void setViscosity(Double v) {
		this.viscosity = v;
	}
	
	@Column(name="solidscontent")
	Double solidsContent;
	public Double getSolidsContent() {
		return solidsContent;
	}
	public void setSolidsContent(Double s) {
		this.solidsContent = s;
	}
	
	@Column(name="oilgravity")
	Double oilGravity;
	public Double getOilGravity() {
		return oilGravity;
	}
	public void setOilGravity(Double g) {
		this.oilGravity = g;
	}
	
	@Column(name="pheadsetdepth")
	Double pHeadSetDepth;
	public Double getPHeadSetDepth() {
		return this.pHeadSetDepth;
	}
	public void setPHeadSetDepth(Double d) {
		this.pHeadSetDepth = d;
	}
	
			
	@Embedded
//	@Valid
	PSComponentPumps pumps;
	public PSComponentPumps getPumps() {
		return pumps;
	}
	public void setPumps(PSComponentPumps p) {
		this.pumps=p;
	}

	@Embedded
//	@Valid
	PSComponentGSI gsi;
	public PSComponentGSI getGsi() {
		return gsi;
	}
	public void setGsi(PSComponentGSI g) {
		this.gsi=g;
	}

	
	@Embedded
//	@Valid
	PSComponentSeals seals;
	public PSComponentSeals getSeals() {
		return seals;
	}
	public void setSeals(PSComponentSeals s) {
		this.seals=s;
	}
	
	
	@Embedded
	@Valid
	PSComponentMotors motors;
	public PSComponentMotors getMotors() {
		return motors;
	}
	public void setMotors(PSComponentMotors m) {
		this.motors=m;
	}
	
	@Embedded
	@Valid
	PSComponentSensor sensor;
	public PSComponentSensor getSensor() {
		return sensor;
	}
	public void setSensor(PSComponentSensor s) {
		this.sensor=s;
	}
	
	
	@Embedded
//	@Valid
	PSComponentCable cable;
	public PSComponentCable getCable() {
		return cable;
	}
	public void setCable(PSComponentCable c) {
		this.cable=c;
	}
	
	@Embedded
//	@Valid
	PSComponentSurface sfe;
	public PSComponentSurface getSfe() {
		return this.sfe;
	}
	public void setSfe(PSComponentSurface sfe) {
		this.sfe=sfe;
	}
	
		
	@OneToMany(mappedBy="installation", fetch = FetchType.EAGER)
	Set<LcEvent> lcEvents;
	public Set<LcEvent> getLcEvents(){
		return this.lcEvents;
	}
	public void SetLcEvents(Set<LcEvent> events){
		this.lcEvents=(Set<LcEvent>) events;
	}
	
	
	public Installation(){}
	
	public LcEvent getLcEvent(Class<?> clazz){
		for (LcEvent event : lcEvents){
			if (event.getClass() == clazz){
				return event;
			}
		}

		return null;
	}
	
	public LcEventStart getStartEvent(){
		for (LcEvent event : lcEvents){
			if (event.getClass() == LcEventStart.class){
				return (LcEventStart) event;
			}
		}
		return null;
	}
	
	public LcEventStop getStopEvent(){
		for (LcEvent event : lcEvents){
			if (event.getClass() == LcEventStop.class){
				return (LcEventStop) event;
			}
		}
		return null;
	}
	
	public LcEventPull getPullEvent(){
		for (LcEvent event : lcEvents){
			if (event.getClass() == LcEventPull.class){
				return (LcEventPull) event;
			}
		}
		return null;
	}
	
	public LcEventTeardown getTeardownEvent(){
		for (LcEvent event : lcEvents){
			if (event.getClass() == LcEventTeardown.class){
				return (LcEventTeardown) event;
			}
		}
		return null;
	}
	
	public LcEventRca getRcaEvent(){
		for (LcEvent event : lcEvents){
			if (event.getClass() == LcEventRca.class){
				return (LcEventRca) event;
			}
		}
		return null;
	}
	
	public LcEvent getSensorEvent(){
		for (LcEvent event : lcEvents){
			if (event.getClass() == LcEventSensor.class){
				return event;
			}
		}
		return null;
	}
	
	public boolean getIsFailure(){
		if (getStopEvent() != null && getStopEvent().getPullReason().getIsFailure()) 
			return true;

		return false;
	}
	
	
	// Used for MTBF calculation
	public String getLabel(String labelType){
		switch (labelType){
			case "country":
				return this.well.getField().getCountry();
			case "contract":
				return this.contract.getName();
			case "field":
				return this.well.getField().getName();
			case "wellTreatment":
				return ""+this.getWellTreatment();
			case "pumpsBrand":
				return this.pumps.getBrand();
			case "pumpsSeries":
				return this.pumps.getSeries();
			case "pumpsAssemblingType":
				return this.pumps.getAssemblingType();
			default:
				return "";
		}
	}
	
	public int getRundays(){
		DateTime lastDay;
		if (this.getStartEvent() == null) {
			return 0;
		}
		if (this.getStopEvent() != null) {
			lastDay=new DateTime(this.getStopEvent().getEventDate()).withTimeAtStartOfDay();
		} else {
			lastDay=new DateTime(new Date()).withTimeAtStartOfDay();
		}
		return Days.daysBetween(new DateTime(this.getStartEvent().getEventDate()).withTimeAtStartOfDay(), lastDay).getDays();
		
	}
	
	
	// Used in installations list, for presentation purposes
		public String getRCAColumn(){
			if (getStopEvent() == null)
				return "";
			if (!getIsFailure())
				return "Not a failure";
			if(getRcaEvent() != null)
				return getRcaEvent().getRcaResult();
			return "TBD";
		}
	
	
	// Used in installations list, for presentation purposes
	public String getTechnology(){
				
		String s;
		s=this.lifttype;
		if(this.motors != null && this.motors.getType() != null && this.motors.getType().equals("PMM"))
				s= this.lifttype + " PMM";
		return s;
	}
	
	
	@PrePersist
    protected void onCreate() {
		updatedDate = createdDate = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
    	updatedDate = new Date();
    }
	
    @Override
	public Set<UserRolesEnum> getRoles() {
		return EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
	}

	public static Class<? extends LcEvent> getNextEvent() {
		return LcEventStart.class;
	}

	public void checkEvent() throws LogicValidationException{
		
		// To add the check that previous installation is pulled
						
		if (this.getInstallation().getStartEvent() != null || this.getInstallation().getStartEvent().getEventDate().before(this.getEventDate()))
			throw new LogicValidationException("Installation date cannot be after start date!");
	}
	
	public void checkDelete() throws LogicValidationException{
		if (this.getInstallation().getStartEvent() != null)
			throw new LogicValidationException("Start event shall be deleted first");
	};
	
	public boolean isWaitAt(Date atDate){
		if (!this.getEventDate().after(atDate)
				&& (this.getInstallation().getStartEvent() == null || this.getInstallation().getStartEvent().getEventDate().after(atDate)))
			return true;
		else
			return false;
		
	}	
	
    
}
