package ru.shaposhnik.runlifetracker.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;


@Embeddable
public class PSComponentSurface  {

			
	@Column(name="sutransfdescr")
	String suTransfDescr;
	@Size(max=40)
	public String getSuTransfDescr(){
		return this.suTransfDescr;
	}
	public void setSuTransfDescr(String s){
		this.suTransfDescr=s;
	}
	
	@Column(name="sdtransfdescr")
	String sdTransfDescr;
	@Size(max=40)
	public String getSdTransfDescr(){
		return this.sdTransfDescr;
	}
	public void setSdTransfDescr(String s){
		this.sdTransfDescr=s;
	}
	
	@Column(name="vsddescr")
	String VSDDescr;
	@Size(max=40)
	public String getVSDDescr(){
		return this.VSDDescr;
	}
	public void setVSDDescr(String s){
		this.VSDDescr=s;
	}
	
	@Column(name="sfnote")
	String sfNote;
	@Size(max=300)
	public String getSfNote(){
		return this.sfNote;
	}
	public void setSfNote(String s){
		this.sfNote=s;
	}
	
		
	public String getCType(){
		return "SFE";
	}
	
	public PSComponentSurface(){
	}
}
