package ru.shaposhnik.runlifetracker.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name="fields")
public class Field implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	
	@Column(name="name", nullable=false,length=32)
	String name;
	public String getName() {
		return name;
	}
	public void setName(String fieldname) {
		this.name = fieldname;
	}
	
	@Column(name="wellpattern", nullable=false,length=32)
	String wellpattern;
	public String getWellpattern() {
		return wellpattern;
	}
	public void setWellpattern(String wp) {
		this.wellpattern = wp;
	}
	
	
	@Column(name="country", nullable=false,length=32)
	String country;
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name="reservoirs")
	String reservoirs;
	public String getReservoirs() {
		return reservoirs;
	}
	public void setReservoirs(String r) {
		this.reservoirs = r;
	}
	
	@Column(name="field_type")
	String fieldType;
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String t) {
		this.fieldType = t;
	}
	
	
	@OneToMany(mappedBy="field", fetch = FetchType.LAZY)
	Set<Well> wells;
	
//	@ManyToMany(mappedBy="fields",fetch = FetchType.EAGER)

	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="usersfields",
			   joinColumns=@JoinColumn(name="field_id"),
			   inverseJoinColumns=@JoinColumn(name="user_id"))
	Set<User> users;
	public Set<User> getUsers() {
/*		Set<User> users=new HashSet<User>();
		for( User user :this.users){
			if (user.getRole() == UserRolesEnum.APPING){
				users.add(user);
			}
		}
*/		return this.users;
	}
	public void setUsers(Set<User> users){
		this.users=users;
	}
	public String getUsersString(){
		String s=new String("ddd");
		for (User user : this.users){
		
			s=s+user.getRole().getAuthority()+":"+user.getNames()+" "+user.getSurnames()+"<br/>";
		} 
		return s;
	}
	
	
//	@ManyToMany(mappedBy="fields", fetch=FetchType.EAGER)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="contractsfields",
    		joinColumns=@JoinColumn(name="field_id"),
    		inverseJoinColumns=@JoinColumn(name="contract_id"))	
	Set<Contract> contracts;
	public Set<Contract> getContracts() {
		return contracts;
	}
	public void setContracts(Set<Contract> cs) {
		this.contracts = cs;
	}	
	public Set<Contract> getActiveContracts(){
		
		Set<Contract> activeContracts = new HashSet<Contract>();
		for (Contract c : this.contracts){
			if (!c.isClosed){
				activeContracts.add(c);
			}
		}
		return activeContracts;
	}
		

	
	public Field(){}
	
	public Field(String name, String country) {
		this.name = name.toUpperCase();
		this.country = country;
	}
	
		
	@Override
	public boolean equals(Object other) {
        if (this == other) return true;
                
        if ( !(other instanceof Field) ) return false;

        final Field otherField = (Field) other;
        
        if ( this.country.equals(otherField.getCountry()) && this.name.equals(otherField.getName()))
        	return true;
        else 
        	return false;
    }

	
	@Override
    public int hashCode() {
        return (name+country).hashCode();
    }

	
	public String getContractsString(){
		
		String str=new String();
		for(Contract c : contracts){
			str=str+", "+c.getName();
    	}
		if (str.length() >= 1){
			str=str.substring(1);
		}
		return str;

	}
	
	public String getDescription(){
		return name+","+country;
	}
}
