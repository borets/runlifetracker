/* 
	2Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package ru.shaposhnik.runlifetracker.model;


public enum FailuredComponentEnum {
	PUMP(1,"Pump",true), 
	GSI(2,"GS/Intake",true),
	SEAL(3,"Seal",true),
	MOTOR(4,"Motor",true), 
	CABLE(5,"Cable",true),
	OTHER(6,"Other",true),
	NODATA(9,"No data",false),
	NOCOMPFAILURE(8,"No component failed",true);


	static {
		PUMP.subcomponents=new String(""+
			"Adapter#"+
			"AR Bushing#"+
			"AR flanged sleeve#"+
			"AR sleeve#"+
			"Base#"+
			"Bottom diffuser#"+
			"Compression bolt#"+
			"Compression tube#"+
			"Diffuser#"+
			"Exterior bolt/fastener#"+
			"Head#"+
			"Housing#"+
			"Impeller#"+
			"Interior bolt/fastener#"+
			"Key#"+
			"Monel coating#"+
			"Ni-resist sleve#"+
			"O-ring#"+
			"Other#"+
			"Shaft#"+
			"Shaft support bearing#"+
			"Shim#"+
			"Snap (Retainer) ring#"+
			"Spider bearing#"+
			"Split (Lock) ring#"+
			"Thrust washer");
		GSI.subcomponents=new String(""+
			"AR bushing#"+
			"Base#"+
			"Compression bolt#"+
			"Compression tube#"+
			"Connector#"+
			"Diverter#"+
			"Exterior bolt/fastener#"+
			"Guide vanes#"+
			"Head#"+
			"Housing#"+
			"Inducer#"+
			"Intake base#"+
			"Interior bolt/fastener#"+
			"Key#"+
			"Monel coating#"+
			"Ni-resist sleve#"+
			"O-ring#"+
			"Other#"+
			"Rotor gassep#"+
			"Shaft#"+
			"Shim#"+
			"Snap (Retainer) ring#"+
			"Spider bearing#"+
			"Split (Lock) ring#"+
			"Thrust washer");
				

		SEAL.subcomponents=new String(""+
			"Adapter#"+
			"Base#"+
			"Bladder (Expansion) bag#"+
			"Check valve#"+
			"Extension plug#"+
			"Exterior bolt/fastener#"+
			"Fill/Drain plug#"+
			"Drain/fill valve#"+
			"Guide#"+
			"Head#"+
			"Heat exchanger#"+
			"Housing#"+
			"Interior bolt/fastener#"+
			"Key#"+
			"Lead washer#"+
			"Mechanical seal#"+
			"Monel coating#"+
			"Oil#"+
			"O-ring#"+
			"Other#"+
			"Relief valve#"+
			"Rubber boot#"+
			"Screen#"+
			"Shaft#"+
			"Shim#"+
			"Snap (Retainer) ring#"+
			"Split (Lock) ring#"+
			"Thrust bearing#"+
			"Thrust runner#"+
			"Upthrust ring");
				
				
		MOTOR.subcomponents=new String(""+
			"Adapter#"+
			"Base#"+
			"Bearing support#"+
			"Check valve#"+
			"Epoxy#"+
			"Exterior bolt/fastener#"+
			"Fill/Drain plug#"+
			"Head#"+
			"Housing#"+
			"Insulating block#"+
			"Insulation#"+
			"Interconnect insulator#"+
			"Interior bolt/fastener#"+
			"Key#"+
			"Lamination#"+
			"Lead washer#"+
			"Leads#"+
			"Leads protection tube#"+
			"Magnet wire#"+
			"Monel coating#"+
			"Oil#"+
			"O-ring#"+
			"Other#"+
			"Rotor#"+
			"Rotor bearing#"+
			"Rotor Bearing Bronze Sleeve#"+
			"Rubber boot#"+
			"Sensor wire#"+
			"Shaft#"+
			"Shim#"+
			"Snap (Retainer) ring#"+
			"Solder#"+
			"Split (Lock) ring#"+
			"Terminal pin#"+
			"Thrust runner#"+
			"Thrust washer#"+
			"Wye connection");
			
		
		CABLE.subcomponents=new String(""+
			"Main power cable#"+
			"MLE#"+
			"Penetrator#"+
			"Pothead connector#"+
			"Splice");
		
		
		OTHER.subcomponents=new String(""+
			"Bands#"+
			"Cable/MLE protector#"+
			"Centralizer#"+
			"Check valve#"+
			"Coupling#"+
			"Discharge head#"+
			"Downhole sensor#"+
			"Drain valve#"+
			"Shroud#"+
			"X-over#"+
			"Tubing#"+
			"Packer#"+
			"Y-tool#"+
			"Other");
		
		NOCOMPFAILURE.subcomponents=new String("No component");
		
	}
	
	private int sortingOder;
	private String label;
	private boolean isEnabled;
	private String subcomponents;


	private FailuredComponentEnum(int so,String l, boolean b) {
		this.sortingOder=so;
		this.label = l;
		this.isEnabled=b;
	}

	public String getLabel(){
		return label;
	}
	
	public boolean getIsEnabled(){
		return this.isEnabled;
	}
	
	public String getSubComponents(){
		return this.subcomponents;
	}
	
}