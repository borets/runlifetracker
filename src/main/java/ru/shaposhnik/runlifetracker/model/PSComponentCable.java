package ru.shaposhnik.runlifetracker.model;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;


@Embeddable
public class PSComponentCable {
	
	
	@Column(name="cablebrand")
	@NotEmpty
	String brand;
	public void setBrand(String s){
		this.brand=s;
	}
	public String getBrand(){
		return this.brand;
	}
	
	@Column(name="cablenote")
	@Size(max=200)
	String note;
	public void setNote(String n){
		note=n;
	}
	public String getNote(){
		return this.note;		
	}
	
	@Column(name="cablelength")
	@Min(100)
	@Max(40000)
	Integer length;
	public void setLength(Integer l){
		this.length=l;
	}
	public Integer getLength(){
		return this.length;
	}
		
	@Column(name="cablereuse")
	@NotEmpty
	String reuse;
	public void setReuse(String s){
		this.reuse=s;
	}
	public String getReuse(){
		return this.reuse;
	}
	
	@Column(name="cablexarea")
	@NotEmpty
	private String xarea;
	public void setXarea(String x){
		this.xarea=x;
	}
	public String getXarea(){
		return this.xarea;
	}
	
	@Column(name="cablearmour")
	@NotEmpty
	private String armour;
	public void setArmour(String a){
		this.armour=a;
	}
	public String getArmour(){
		return this.armour;
	}
	
	@Column(name="cableisconcap")
	private boolean isConcaptube;
	public void setIsConcaptube(boolean b){
		this.isConcaptube=b;
	}
	public boolean getIsConcaptube(){
		return this.isConcaptube;
	}
	
	@Column(name="mlesn")
	@NotEmpty
	private String mleSn;
	public void setMleSn(String s){
		this.mleSn=s;
	}
	public String getMleSn(){
		return this.mleSn;
	}
	
	
	@Column(name="mledescr")
	@NotEmpty
	private String mleDescr;
	public void setMleDescr(String d){
		this.mleDescr=d;
	}
	public String getMleDescr(){
		return this.mleDescr;
	}
	
	
	

	public String getCType(){
		return "Cable";
	}
	

	
	public static Set<String> brands = new LinkedHashSet<String>(Arrays.asList("ZTS", "Kurgan", "3rd party"));
	public static Set<String> reuses = new LinkedHashSet<String>(Arrays.asList("New", "Repaired", "Re-run"));
	public static Set<String> xareas = new LinkedHashSet<String>(Arrays.asList("1", "2", "4", "6"));
	public static Set<String> armours = new LinkedHashSet<String>(Arrays.asList("G", "SS", "M", "2G","2SS"));
	
		
	public PSComponentCable(){}
}
