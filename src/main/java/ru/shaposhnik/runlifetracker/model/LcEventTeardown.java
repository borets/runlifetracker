package ru.shaposhnik.runlifetracker.model;


import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;


@Entity
@DiscriminatorValue("LcEventRCA")
public class LcEventTeardown extends LcEvent {


	@Column(name="rca_responsibility")
	@NotNull
	FailureResponsibilityEnum responsibility;
	public FailureResponsibilityEnum getResponsibility(){
		return this.responsibility;
	}
	public void setResponsibility(FailureResponsibilityEnum r){
		this.responsibility=r;
	}
	
	
	@Column(name="rca_rootcause")
	@NotNull
	FailureCausesEnum rootCause;
	public FailureCausesEnum getRootCause(){
		return this.rootCause;
	}
	public void setRootCause(FailureCausesEnum rc){
		this.rootCause=rc;
	}
	
	@Column(name="rca_subrootcause")
	@NotNull
	@Size(min=1)
	String subRootCause;
	public String getSubRootCause(){
		return this.subRootCause;
	}
	public void setSubRootCause(String s){
		this.subRootCause=s;
	}
	
	@Column(name="rca_fcomponent")
	@NotNull
	FailuredComponentEnum failuredComponent;
	public FailuredComponentEnum getFailuredComponent(){
		return this.failuredComponent;
	}
	public void setFailuredComponent(FailuredComponentEnum fc){
		this.failuredComponent=fc;
	}
	
	@Column(name="rca_subcomponent")
	@NotNull
	@Size(min=1)
	String failuredSubComponent;
	public String getFailuredSubComponent(){
		return this.failuredSubComponent;
	}
	public void setFailuredSubComponent(String fsc){
		this.failuredSubComponent=fsc;
	}
	
	@Override
	public Set<UserRolesEnum> getRoles() {
		return EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.DIFAING);
	}

	public static Class<? extends LcEvent> getNextEvent() {
		return LcEventRca.class;
	}
	
	public void checkEvent() throws LogicValidationException{
		
		if (this.getInstallation().getPullEvent() == null)
			throw new LogicValidationException("The well has not been pulled!");
		
		if (this.getInstallation().getPullEvent().getEventDate().after(this.getEventDate()))
			throw new LogicValidationException("Teardown date cannot be earlier then pull date!");
		
		if (this.getInstallation().getRcaEvent() != null || this.getInstallation().getRcaEvent().getEventDate().before(this.getEventDate()))
			throw new LogicValidationException("Teardown date cannot be after RCA date!");
	}
	
	public void checkDelete() throws LogicValidationException{
		if (this.getInstallation().getRcaEvent() != null)
			throw new LogicValidationException("RCA event shall be deleted first");
	};
	
	public boolean isWaitAt(Date atDate){
		if (!this.getEventDate().after(atDate)
				&& (this.getInstallation().getRcaEvent() == null || this.getInstallation().getRcaEvent().getEventDate().after(atDate)))
			return true;
		else
			return false;
		
	}	
	
}
