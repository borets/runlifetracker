package ru.shaposhnik.runlifetracker.model;

import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;


@Entity
@DiscriminatorValue("LcEventSensor")
public class LcEventSensor extends LcEvent {

	@Override
	public Set<UserRolesEnum> getRoles() {
		return EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
	}


	public static Class<? extends LcEvent> getNextEvent() {
		return LcEventStop.class;
	}
	
	
	public void checkEvent() throws LogicValidationException{
		
		if (this.getInstallation().getStartEvent() == null)
			throw new LogicValidationException("The well has not been started!");
		
		if (this.getInstallation().getStartEvent().getEventDate().after(this.getEventDate()))
			throw new LogicValidationException("Sensor cannot fail before the start!");
		
		if (this.getInstallation().getStopEvent() != null || this.getInstallation().getStopEvent().getEventDate().before(this.getEventDate()))
			throw new LogicValidationException("Sensor cannot fail after the stop date!");
	}
	
	public void checkDelete() throws LogicValidationException{
	};
	
	public boolean isWaitAt(Date atDate){
			return false;
		
	}	

}
