/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package ru.shaposhnik.runlifetracker.model;


public enum Reasons4PullEnum {

	
	NODATA("No data",false),
	MFLOWFLOW("Mechanic failure-low flow", true),
	MFNOFLOW("Mechanic failure-no flow",true),
	MFPUMPSTUCK("Mechanic failure-pump stuck",true),
	EF("Electric failure",true),
	CLIENTDECISION("Client decision",true);


	
	
	private String label;
	private boolean enabled;

	private Reasons4PullEnum(String l, boolean b) {
		this.label = l;
		this.enabled=b;
	}

	
	public String getLabel(){
		return label;
	}
	
	public boolean getEnabled(){
		return enabled;
	}
	
	public boolean getIsFailure(){
		return this == CLIENTDECISION ? false : true;
	}
}