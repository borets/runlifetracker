package ru.shaposhnik.runlifetracker.model;

import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;

@Entity
@DiscriminatorValue("LcEventClient")
public class LcEventRca extends LcEvent {

	@Column(name="client_class")
	@NotNull
	String rcaResult;
	public String getRcaResult(){
		return this.rcaResult;
	}
	public void setRcaResult(String cc){
		this.rcaResult=cc;
	}
	
	@Override
	public Set<UserRolesEnum> getRoles() {
		return EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
	}
	
	public static Class<? extends LcEvent> getNextEvent() {
		return null;
	}

	public void checkEvent() throws LogicValidationException{
		
		if (this.getInstallation().getTeardownEvent() == null)
			throw new LogicValidationException("Teardown has not been made!");
		
		if (this.getInstallation().getTeardownEvent().getEventDate().after(this.getEventDate()))
			throw new LogicValidationException("RCA date cannot be earlier then teardown date!");
	}
	
	public void checkDelete() throws LogicValidationException{
		// Always possible to delete
	};
	
	public boolean isWaitAt(Date atDate){
		// it is the last event
		return false;
	}	
}
