/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package ru.shaposhnik.runlifetracker.model;

import org.springframework.security.core.GrantedAuthority;

public enum UserRolesEnum implements GrantedAuthority {
	ADMIN("ROLE_ADMIN"), ANALYST("ROLE_ANALYST"), APPING("ROLE_AP_ENGINEER"), DIFAING("ROLE_DIFA_ENGINEER");

	private String label;

	private UserRolesEnum(String l) {
		this.label = l;
	}

	public String getAuthority(){
		return label;
	}
}