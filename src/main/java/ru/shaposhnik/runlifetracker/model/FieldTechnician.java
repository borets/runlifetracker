package ru.shaposhnik.runlifetracker.model;

import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.userdetails.UserDetails;


/**
 * User entity
 */
@Entity
@Table(name="fieldtechnicians")
public class FieldTechnician {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	

	@Column(name="names", nullable=false,length=32)
	@Size(min=1,max=20)
	String names;
	public String getNames() {
		return names;
	}
	public void setNames(String n) {
		this.names = n;
	}
	
	
	@Column(name="surnames", nullable=false,length=32)
	@Size(min=1,max=20)
	String surnames;
	public String getSurnames() {
		return surnames;
	}
	public void setSurnames(String n) {
		this.surnames = n;
	}
	
	
	@Column(name="country", nullable=false,length=32)
	@Size(min=4,max=45)
	String country;
	public String getCountry() {
		return country;
	}
	public void setCountry(String e) {
		this.country = e;
	}
	
	@Column(name="national_id", nullable=false,length=32)
	@NotNull
	@Size(min=3,max=10)
	String nationalId;
	public String getNationalId() {
		return nationalId;
	}
	public void setNationalId(String e) {
		this.nationalId = e;
	}
	
	@Column(name="isactive", nullable=false)
	@NotNull
	Boolean isActive;
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean b) {
		this.isActive = b;
	}
			
/*	
	@OneToMany(mappedBy="fieldTechnician",fetch = FetchType.LAZY)
	Set<Installation> installation;
*/	
	
	public FieldTechnician(){
	}
	

	@Override
	public boolean equals(Object other) {
        if (this == other) return true;
        if ( !(other instanceof FieldTechnician) ) return false;

        final FieldTechnician otherFT = (FieldTechnician) other;

        if ( !(this.country+this.names+this.surnames).equalsIgnoreCase(otherFT.getCountry()+otherFT.getNames()+otherFT.getSurnames()))
        	return false;
     
        return true;
    }
	@Override
    public int hashCode() {
        return (this.country+this.names+this.surnames).hashCode();
    }
		
	public String getDescription(){
		return surnames+", "+names;
	}
}