package ru.shaposhnik.runlifetracker.model;

import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;
import ru.shaposhnik.runlifetracker.model.Reasons4PullEnum;

@Entity
@DiscriminatorValue("LcEventStop")
public class LcEventStop extends LcEvent {

	@Column(name="pullreason")
	@NotNull
	Reasons4PullEnum pullReason;
		
	public Reasons4PullEnum getPullReason(){
		return this.pullReason;
	}
	public void setPullReason(Reasons4PullEnum rp){
		this.pullReason=rp;
	}
	
	@Override
	public Set<UserRolesEnum> getRoles() {
		return EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
	}
	

	public static Class<? extends LcEvent> getNextEvent() {
		return LcEventPull.class;
	}
	
	public void checkEvent() throws LogicValidationException{
		
		if (this.getInstallation().getStartEvent() == null)
			throw new LogicValidationException("The well has not started!");
		
		if (this.getInstallation().getStartEvent().getEventDate().before(this.getEventDate()))
			throw new LogicValidationException("Stop date cannot be earlier then start date!");
		
		if (this.getInstallation().getPullEvent() != null || this.getInstallation().getPullEvent().getEventDate().after(this.getEventDate()))
			throw new LogicValidationException("Stop date cannot be after pull date!");
	}
	
	public void checkDelete() throws LogicValidationException{
		if (this.getInstallation().getPullEvent() != null)
			throw new LogicValidationException("Pull event shall be deleted first");
	};
	
	public boolean isWaitAt(Date atDate){
		if (!this.getEventDate().after(atDate)
				&& (this.getInstallation().getPullEvent() == null || this.getInstallation().getPullEvent().getEventDate().after(atDate)))
			return true;
		else
			return false;
		
	}	
}
