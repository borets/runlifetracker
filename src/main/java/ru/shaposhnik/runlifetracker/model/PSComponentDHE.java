package ru.shaposhnik.runlifetracker.model;


import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.persistence.Embeddable;

@Embeddable
public abstract class PSComponentDHE {
	
	
	
	
	private String series;
	public void setSeries(String s){
		this.series=s;
	}
	public String getSeries(){
		return this.series;
	}
	
	
	private String metallurgy;
	public void setMetallurgy(String s){
		this.metallurgy=s;
	}
	public String getMetallurgy(){
		return this.metallurgy;
	}
	
	private String brand;
	public void setBrand(String s){
		this.brand=s;
	}
	public String getBrand(){
		return this.brand;
	}
	
	
	public static Set<String> metallurgies=new LinkedHashSet<String>(Arrays.asList("CS", "SS","Coated"));
	
	
	public PSComponentDHE(){
	}
}
