/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package ru.shaposhnik.runlifetracker.model;


public enum FailureCausesEnum {
	
	NODATA("No data",false),
	MANUFACTURE("Manufacture",true), 
	WORKSHOP("Local workshop",true),
	APPLICATION("Wrong application design",true),
	FIELDSERVICE("Installation procedure",true),
	OPERATION("Well running errors",true),
	RESERVOIR("Reservoir conditions",true),
	WELLPREPARATION("Well preparation",true),	
	OTHER("Other reason",true);

	static {
		MANUFACTURE.subRootCauses=new String("R&D#Factory");
		WORKSHOP.subRootCauses=new String("Local workshop");
		APPLICATION.subRootCauses=new String("Sizing-wrong input data#Material selection#Incompatible components selection#Wrong sizing#Wrong system design");
		FIELDSERVICE.subRootCauses=new String("Transportation&Logistics#Installation#Shimming#Splicing#Oil filling#Assembling#Other");
		OPERATION.subRootCauses=new String("Electric supply#Wrong VSD setting#Wrong SF equipment setting#Too much restarts#Pulled out by error#Other");
		RESERVOIR.subRootCauses=new String("Scale#Sand abrasives#Sand plugging#Paraffins#Asphaltenes#Gas#Viscosity#Temperature#Corrosion#Rocks#No inflow#Other");
		WELLPREPARATION.subRootCauses=new String("External vendor component#Cement#Proppant#Foreign debris#Well geometry#No tangent section#Drilling mud#Other");
		OTHER.subRootCauses=new String("Normal wear and tear#Intentionally taken risk#Seismoactivity#Sabotage#Other");
	}
	
	
	
	
	
/*	MANUFACTURE("Manufacture",true), 
	WORKSHOP("Local workshop",true),
	RD("Wrong equipment design (R/D)",false),
	APPLICATION("Wrong application design",true),
	TRANSPORTATION("Transportation and logistics",false), 
	FIELDSERVICE("Installation procedure",true),
	OPERATION("Equipment operation errors",true),
	RESERVOIR("Reservoir conditions",true),
	MANAGEMENT("Intentionally taken risk",false),
	NORMALWEAR("Normal wear and tear",false),
	OTHER("Other reason",true),
	NOFAILURE("No failure detected",false),
	NODATA("No data",false),
	WELLPREPARATION("Well preparation",true),
	THIRDPARTY("3rd party component",false);

		
	static {
		MANUFACTURE.subRootCauses=new String("R&D#Factory");
		WORKSHOP.subRootCauses=new String("");
		RD.subRootCauses=new String("");
		APPLICATION.subRootCauses=new String("Sizing-wrong input data#Material selection#Incompatible components selection#Wrong sizing#Wrong system design");
		TRANSPORTATION.subRootCauses=new String("");
		FIELDSERVICE.subRootCauses=new String("Transportation&Logistics#Installation#Shimming#Splicing#Oil filling#Assembling#Other");
		OPERATION.subRootCauses=new String("Electric supply#Wrong VSD setting#Wrong SF equipment setting#Too much restarts#Pulled out by error#Other");
		RESERVOIR.subRootCauses=new String("Scale#Sand abrasives#Sand plugging#Paraffins#Asphaltenes#Gas#Viscosity#Temperature#Corrosion#Rocks#No inflow#Other");
		MANAGEMENT.subRootCauses=new String("Re-used equipment w/o repair#Suboptimal equipment selection#Other");
		NORMALWEAR.subRootCauses=new String("");
		OTHER.subRootCauses=new String("Normal wear and tear#Intentionally taken risk#Seismoactivity#Sabotage#Other");
		NOFAILURE.subRootCauses=new String("");
		NODATA.subRootCauses=new String("");
		WELLPREPARATION.subRootCauses=new String("External vendor component#Cement#Proppant#Foreign debris#Well geometry#No tangent section#Drilling mud#Other");
		THIRDPARTY.subRootCauses=new String("");
	}
	
*/	
	private String label;
	private boolean isEnabled;
	private String subRootCauses;

	private FailureCausesEnum(String l,boolean  b) {
		this.label = l;
		this.isEnabled=b;
	}

	public String getLabel(){
		return label;
	}
	
	public boolean getIsEnabled(){
		return this.isEnabled;
	}
	
	public String getSubRootCauses(){
		return this.subRootCauses;
	}
	
}