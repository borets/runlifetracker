package ru.shaposhnik.runlifetracker.model;

import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.userdetails.UserDetails;


/**
 * User entity
 */
@Entity
@Table(name="users")
public class User implements Serializable,Cloneable,UserDetails {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	

	@Column(name="names", nullable=false,length=32)
	@Size(min=1,max=20)
	String names;
	public String getNames() {
		return names;
	}
	public void setNames(String n) {
		this.names = n;
	}
	
	
	@Column(name="surnames", nullable=false,length=32)
	@Size(min=1,max=20)
	String surnames;
	public String getSurnames() {
		return surnames;
	}
	public void setSurnames(String n) {
		this.surnames = n;
	}
	
	
	@Column(name="email", nullable=false,length=32)
	@Size(min=4,max=45)
	String email;
	public String getEmail() {
		return email;
	}
	public void setEmail(String e) {
		this.email = e.toLowerCase();
	}
	
	@Column(name="role", nullable=false)
	@NotNull
	UserRolesEnum role;
	public UserRolesEnum getRole() {
		return role;
	}
	public void setRole(UserRolesEnum r) {
		this.role = r;
	}
	
	@Column(name="password", nullable=false,length=32)
	@NotNull
	@Size(min=3,max=10)
	String password;
	public String getPassword() {
		return password;
	}
	public void setPassword(String e) {
		this.password = e;
	}
	
	
	@Column(name="isblocked", nullable=false)
	@NotNull
	Boolean isblocked;
	public Boolean getIsblocked() {
		return isblocked;
	}
	public void setIsblocked(Boolean b) {
		this.isblocked = b;
	}
			
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="usersfields",
			   joinColumns=@JoinColumn(name="user_id"),
			   inverseJoinColumns=@JoinColumn(name="field_id"))
	Set<Field> fields;
	
	public Set<Field> getFields() {
		return this.fields;
	}
	public void setFields(Set<Field> fs) {
		this.fields = fs;
	}
	
	
	public User(){
		this.email="";
	}
	
	public User(String names, String surnames, String email, UserRolesEnum role) {
		this.names = names;
		this.surnames = surnames;
		this.email = email.toLowerCase();
		this.role = role;
		this.isblocked=false;
	}
	
	@Override
	public boolean equals(Object other) {
        if (this == other) return true;
        if ( !(other instanceof User) ) return false;

        final User otherUser = (User) other;

        if ( this.email != otherUser.getEmail()) return false;
     
        return true;
    }
	@Override
    public int hashCode() {
        return email.hashCode();
    }	
	
	// interface UserDetails implementation
	public 	HashSet<UserRolesEnum> getAuthorities(){
		HashSet<UserRolesEnum> auth=new HashSet<UserRolesEnum>();
		auth.add(this.role);
		return auth;
	}	
	public String getUsername(){
		return this.email;
	}
	public boolean isAccountNonExpired(){
		return true;
	}
	public boolean isAccountNonLocked(){
		return true;
	}
	public boolean isCredentialsNonExpired(){
		return true;
	}
	public boolean isEnabled(){
		return !this.isblocked;
	}
	
	//method for presenting list in the views
	public String getFieldsString(){
		
		String str=new String();
		for(Field f : fields){
			str=str+", "+f.getName();
    	}
		
		if (str.length() >= 1){
			str=str.substring(1);
		}
		return str;

	}
	
	public boolean hasRightOnfield(Field field){
		if (this.role==UserRolesEnum.ADMIN){
			return true;
		} else 		
			return fields.contains(field);
		
	}
	
	public String getDescription(){
		return names+" "+surnames;
	}
}