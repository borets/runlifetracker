package ru.shaposhnik.runlifetracker.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;


@Entity
@Table(name="files4installation")
public class SavedFile implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	
	
	@Column(name="contenttype", nullable=false,length=32)
	String contentType;
	public String getContentType() {
		return this.contentType;
	}
	public void setContentType(String t) {
		this.contentType = t;
	}
	
	
	@Column(name="file", nullable=false)
	String file;	
	public String getFile() {
		return this.file;
	}
	public void setFile(String f) {
		this.file = f;
	}

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="installation_id", nullable=true)
	Installation installation;
	public Installation getInstallation() {
		return this.installation;
	}
	public void setInstallation(Installation i) {
		this.installation = i;
	}
	
	
	
	public SavedFile(){}
	
}
