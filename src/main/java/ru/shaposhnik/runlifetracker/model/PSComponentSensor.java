package ru.shaposhnik.runlifetracker.model;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;


@Embeddable
public class PSComponentSensor {
	
	
	@Column(name="sensorbrand")
	String brand;
	public void setBrand(String s){
		this.brand=s;
	}
	public String getBrand(){
		return this.brand;
	}
	
	@Column(name="sensornote")
	@Size(max=200)
	String note;
	public void setNote(String n){
		note=n;
	}
	public String getNote(){
		return this.note;		
	}
	
		
	@Column(name="sensorreuse")
	String reuse;
	public void setReuse(String s){
		this.reuse=s;
	}
	public String getReuse(){
		return this.reuse;
	}
	
	@Column(name="sensorvariables")
	String variables;
	public void setVariables(String s){
		this.variables=s;
	}
	public String getVariables(){
		return this.variables;
	}

	@Column(name="sensorsn")
	String sn;
	public void setSn(String s){
		this.sn=s;
	}
	public String getSn(){
		return this.sn;
	}
	
	
	public String getCType(){
		return "Sensor";
	}
	
	
	public static Set<String> brands = new LinkedHashSet<String>(Arrays.asList("Viewpoint", "SPT", "Zenith", "GRC", "Other"));
	public static Set<String> reuses = new LinkedHashSet<String>(Arrays.asList("New", "Repaired", "Re-run"));
	public static Set<String> variableslist = new LinkedHashSet<String>(Arrays.asList("5", "6"));
	
		
	public PSComponentSensor(){}
}
