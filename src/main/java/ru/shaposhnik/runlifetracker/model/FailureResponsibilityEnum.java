/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package ru.shaposhnik.runlifetracker.model;


public enum FailureResponsibilityEnum {
	
	ESPVENDOR("ESP vendor"),
	CLIENT("Client"), 
	NOBODY("No responsibles");
	

	private String label;

	private FailureResponsibilityEnum(String l) {
		this.label = l;
	}

	public String getLabel(){
		return label;
	}
	

}