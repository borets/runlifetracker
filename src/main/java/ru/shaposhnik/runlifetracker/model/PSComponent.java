package ru.shaposhnik.runlifetracker.model;

import ru.shaposhnik.runlifetracker.model.Installation;

public abstract class PSComponent {
	
	

	String note;
	public void setNote(String n){
		note=n;
	}
	public String getNote(){
		return this.note;		
	}
	
	Installation installation;
	public void setInstallation(Installation i){
		this.installation=i;
	}
	public Installation getInstallation(){
		return this.installation;
	}
	
		
		
	public abstract String getCType();
	
	
	public PSComponent(){
		
	}
}
