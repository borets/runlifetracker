package ru.shaposhnik.runlifetracker.model;

import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;


@Entity
@DiscriminatorValue("LcEventPull")
public class LcEventPull extends LcEvent {

	@Override
	public Set<UserRolesEnum> getRoles() {
		return EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
	}

	public static Class<? extends LcEvent> getNextEvent() {
		return LcEventTeardown.class;
	}

	public void checkEvent() throws LogicValidationException{
		
		if (this.getInstallation().getStopEvent() == null)
			throw new LogicValidationException("The well has not been stopped!");
		
		if (this.getInstallation().getStopEvent().getEventDate().before(this.getEventDate()))
			throw new LogicValidationException("Pull date cannot be earlier then stop date!");
		
		if (this.getInstallation().getTeardownEvent() != null || this.getInstallation().getPullEvent().getEventDate().after(this.getEventDate()))
			throw new LogicValidationException("Pull date cannot be after teardown date!");
	}
	
	public void checkDelete() throws LogicValidationException{
		if (this.getInstallation().getTeardownEvent() != null)
			throw new LogicValidationException("Teardown event shall be deleted first");
	};
	
	public boolean isWaitAt(Date atDate){
		if (!this.getEventDate().after(atDate)
				&& (this.getInstallation().getTeardownEvent() == null || this.getInstallation().getTeardownEvent().getEventDate().after(atDate)))
			return true;
		else
			return false;
		
	}	
	
}
