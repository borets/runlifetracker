package ru.shaposhnik.runlifetracker.model;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;


@Embeddable
public class PSComponentPumps {
	
	@Column(name="pumpsseries")
	@NotEmpty
	private String series;
	public void setSeries(String s){
		this.series=s;
	}
	public String getSeries(){
		return this.series;
	}
	
	@Column(name="pumpsmetallurgy")
	@NotEmpty
	private String metallurgy;
	public void setMetallurgy(String s){
		this.metallurgy=s;
	}
	public String getMetallurgy(){
		return this.metallurgy;
	}
	
	@Column(name="pumpsbrand")
	@NotEmpty
	private String brand;
	public void setBrand(String s){
		this.brand=s;
	}
	public String getBrand(){
		return this.brand;
	}
	
	
	@Column(name="pumpsstagesq")
	@Min(10)
	@Max(1000)
	private Integer stagesQ;
	public Integer getStagesQ(){
		return this.stagesQ;
	}
	public void setStagesQ(Integer q){
		this.stagesQ=q;
	}
	
	@Column(name="pumpsabrasionres")
	@NotEmpty
	private String abrasionRes;
	public String getAbrasionRes(){
		return this.abrasionRes;
	}
	public void setAbrasionRes(String s){
		this.abrasionRes=s;
	}
	
	@Column(name="pumpsflowtype")
	@NotEmpty
	private String flowType;
	public String getFlowType(){
		return this.flowType;
	}
	public void setFlowType(String f){
		this.flowType=f;
	}
	
	@Column(name="pumpsassemblingtype")
	@NotEmpty
	private String assemblingType;
	public String getAssemblingType(){
		return this.assemblingType;
	}
	public void setAssemblingType(String f){
		this.assemblingType=f;
	}

	@Column(name="pumpsflow")
	@NotEmpty
	private Integer pumpsFlow;
	public Integer getPumpsFlow(){
		return this.pumpsFlow;
	}
	public void setPumpsFlow(Integer i){
		this.pumpsFlow=i;
	}
	
	@Column(name="vaproserial")
	@Size(max=15)
	private String vaproSerial;
	public String getVaproSerial(){
		return this.vaproSerial;
	}
	public void setVaproSerial(String f){
		this.vaproSerial=f;
	}
	
	@Column(name="vaprodescription")
	@NotEmpty
	private String vaproDescription;
	public String getVaproDescription(){
		return this.vaproDescription;
	}
	public void setVaproDescription(String f){
		this.vaproDescription=f;
	}
	
	@Column(name="pumpserials")
	@Size(max=60)
	private String pumpSerials;
	public String getPumpSerials(){
		return this.pumpSerials;
	}
	public void setPumpSerials(String f){
		this.pumpSerials=f;
	}
	

	
	@Column(name="pumpsqty")
	@Min(1)
	@Max(3)
	Integer qty;
	public Integer getQty(){
		return this.qty;
	}
	public void setQty(Integer n){
		this.qty=n;
	}
	
	
	@Column(name="pumpsnote")
	@Size(max=200)
	String note;
	public void setNote(String n){
		note=n;
	}
	public String getNote(){
		return this.note;		
	}
	
	
		
	public static Set<String> brands = new LinkedHashSet<String>(Arrays.asList("BRT-NL","BRT-OL", "SLB","BH", "GE", "ALK", "NLS"));
	public static Set<String> metallurgies=new LinkedHashSet<String>(Arrays.asList("CS", "SS","Coated"));
	public static Set<String> seriess=new LinkedHashSet<String>(Arrays.asList("338","400", "513", "538"));
	public static Set<String> flowTypes=new LinkedHashSet<String>(Arrays.asList("Radial", "Mixed"));
	public static Set<String> abrasionResTypes=new LinkedHashSet<String>(Arrays.asList("STD", "TA2", "TA", "XA"));
	
	
	public String getCType(){
		return "Pump";
	}
	
	public PSComponentPumps(){
	}
}
