package ru.shaposhnik.runlifetracker.model;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;


@Embeddable
public class PSComponentMotors  {

	
	@Column(name="motorsseries")
//	@NotEmpty
	String series;
	public void setSeries(String s){
		this.series=s;
	}
	public String getSeries(){
		return this.series;
	}
	
	@Column(name="motorsmetallurgy")
//	@NotEmpty
	String metallurgy;
	public void setMetallurgy(String s){
		this.metallurgy=s;
	}
	public String getMetallurgy(){
		return this.metallurgy;
	}

	@Column(name="motorsbrand")
//	@NotEmpty
	String brand;
	public void setBrand(String s){
		this.brand=s;
	}
	public String getBrand(){
		return this.brand;
	}
	
	
	@Column(name="motorstype")
	@NotEmpty
	String type;
	public String getType(){
		return this.type;
	}
	public void setType(String t){
		this.type=t;
	}
	
	@Column(name="motorshp")
//	@Min(10)
	@Max(3000)
	Integer hp;
	public Integer getHp(){
		return this.hp;
	}
	public void setHp(Integer hp){
		this.hp=hp;
	}
	
	@Column(name="motorsvolts")
//	@Min(10)
	@Max(5000)
	Integer volts;
	public Integer getVolts(){
		return this.volts;
	}
	public void setVolts(Integer v){
		this.volts=v;
	}
	
	@Column(name="motorsamps")
//	@Min(10)
	@Max(200)
	Double amps;
	public Double getAmps(){
		return this.amps;
	}
	public void setAmps(Double a){
		this.amps=a;
	}
	
	@Column(name="motorsqty")
//	@Min(1)
	@Max(3)
	Integer qty;
	public Integer getQty(){
		return this.qty;
	}
	public void setQty(Integer n){
		this.qty=n;
	}
	
	@Column(name="umotorserial")
	@Size(max=15)
	String uMotorSerial;
	public void setUMotorSerial(String s){
		this.uMotorSerial=s;
	}
	public String getUMotorSerial(){
		return this.uMotorSerial;		
	}
	
	@Column(name="lmotorserial")
	@Size(max=15)
	String lMotorSerial;
	public void setLMotorSerial(String s){
		this.lMotorSerial=s;
	}
	public String getLMotorSerial(){
		return this.lMotorSerial;		
	}
	
	
	
	@Column(name="motorsnote")
	@Size(max=200)
	String note;
	public void setNote(String n){
		note=n;
	}
	public String getNote(){
		return this.note;		
	}
	
	
	
	public static Set<String> seriess=new LinkedHashSet<String>(Arrays.asList("375", "456", "562", "117mm"));
	public static Set<String> motorTypes = new LinkedHashSet<String>(Arrays.asList("Induction", "PMM"));
	public static Set<String> brands = new LinkedHashSet<String>(Arrays.asList("BRT", "SLB","BH", "GE", "ALK", "NLS"));
	public static Set<String> metallurgies=new LinkedHashSet<String>(Arrays.asList("CS", "SS","Coated"));
	
	
	public String getCType(){
		return "Motor";
	}
	
	public PSComponentMotors(){
	}
}
