package ru.shaposhnik.runlifetracker.model;

import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;


@Entity
@DiscriminatorValue("LcEventStart")
public class LcEventStart extends LcEvent {

	@Override
	public Set<UserRolesEnum> getRoles() {
		return EnumSet.of(UserRolesEnum.ADMIN,UserRolesEnum.APPING);
	}

	public static Class<? extends LcEvent> getNextEvent() {
		return LcEventStop.class;
	}
	
	public void checkEvent() throws LogicValidationException{
		
		if (this.getInstallation() == null)
			throw new LogicValidationException("Installation date is not defined!");
		
		if (getInstallation().getEventDate().after(this.getEventDate()))
			throw new LogicValidationException("Start date cannot be before installation date!");
		
		if (this.getInstallation().getStopEvent() != null || this.getInstallation().getStopEvent().getEventDate().before(this.getEventDate()))
			throw new LogicValidationException("Start date cannot be after stop date!");
	}

	public void checkDelete() throws LogicValidationException{
		if (this.getInstallation().getStopEvent() != null)
			throw new LogicValidationException("Stop event shall be deleted first");
	};
	
	public boolean isWaitAt(Date atDate){
		if (!this.getEventDate().after(atDate)
				&& (this.getInstallation().getStopEvent() == null || this.getInstallation().getStopEvent().getEventDate().after(atDate)))
			return true;
		else
			return false;
		
	}
	
}
