package ru.shaposhnik.runlifetracker.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * User entity
 */
@Entity
@Table(name="wells")
public class Well implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}

	public String getName() {
		return prefix+"-"+number+((suffix == null) ? "": suffix);
	}

	@Column(name="prefix",nullable=false,length=20)
	@Size(min=2,max=20)
	String prefix;
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String p) {
		this.prefix = p;
	}
	
	@Column(name="number",nullable=false)
	Integer number;
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer n) {
		this.number = n;
	}
	
	@Column(name="suffix",nullable=true,length=3)
	@Size(max=3)
	String suffix;
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String s) {
		this.suffix = s.toUpperCase();
	}
	
	@Column(name="casingod",nullable=true,length=8)
	@Size(min=1)
	String casingOD;
	public String getCasingOD() {
		return casingOD;
	}
	public void setCasingOD(String cod) {
		this.casingOD = cod;
	}
	
	
	@Column(name="casingweight",nullable=true,length=8)
	@NotNull
	@Min(value=0)
	Double casingWeight;
	public Double getCasingWeight() {
		return casingWeight;
	}
	public void setCasingWeight(Double w) {
		this.casingWeight = w;
	}
	
	
	@ManyToOne
	@JoinColumn(name="field_id", nullable=false)
	@NotNull
	Field field;
	public Field getField() {
		return field;
	}
	public void setField(Field field) {
		this.field = field;
	}
	
	
	public Well(){}
	
	@Override
	public boolean equals(Object other) {
        if (this == other) return true;
        if ( !(other instanceof Well) ) return false;

        final Well otherWell = (Well) other;

        if ( this.field != otherWell.getField()) return false;
        if ( this.getName() != otherWell.getName()) return false;

        return true;
    }

	
	@Override
    public int hashCode() {
		if ((getName()==null) || (field==null)){
			return 0;
		}
        return (getName()+field.getCountry().hashCode()).toUpperCase().hashCode();
    }
	
}
