package ru.shaposhnik.runlifetracker.model;


import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;


@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@Table(name="lces")
@DiscriminatorColumn(name="discriminator")
public abstract class LcEvent{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	
	@ManyToOne
	@JoinColumn(name="installation_id", nullable=false)
	Installation installation;
	public Installation getInstallation() {
		return installation;
	}
	public void setInstallation(Installation i) {
		this.installation = i;
	}
	
	@Temporal(TemporalType.DATE)
	@NotNull
	@Past
	@Column(name="eventdate")
	Date eventDate;
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date d) {
		this.eventDate = d;
	}
	
	
	@Column(name="eventnote", length=100)
	String note;
	public String getNote() {
		return note;
	}
	public void setNote(String s) {
		this.note = s;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="createddate",length=50)
	Date createdDate;
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date d) {
		this.createdDate=d;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updateddate",length=50)
	Date updatedDate;
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date d) {
		this.updatedDate=d;
	}

	@Column(name="createdby",length=50)
	String createdBy;
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String user) {
		this.createdBy=user;
	}
	
	@Column(name="updatedby",length=50)
	String updatedBy;
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String user) {
		this.updatedBy=user;
	}
	
	@PrePersist
    protected void onCreate() {
		updatedDate = createdDate = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
    	updatedDate = new Date();
    }
	
    public abstract Set<UserRolesEnum> getRoles();
    public static Class<? extends LcEvent> getNextEvent(){
    	return null;
    };
	
    public abstract void checkEvent() throws LogicValidationException;
    public abstract void checkDelete() throws LogicValidationException;
    public abstract boolean isWaitAt(Date d);
    
}
