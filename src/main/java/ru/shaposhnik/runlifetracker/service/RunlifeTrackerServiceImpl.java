/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.service;

import java.util.List;
import java.lang.Override;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.repository.FieldRepository;
import ru.shaposhnik.runlifetracker.model.Well;
import ru.shaposhnik.runlifetracker.repository.WellRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class RunlifeTrackerServiceImpl implements RunlifeTrackerService {

    private FieldRepository fieldRepository;
    private WellRepository wellRepository;

    @Autowired
    public RunlifeTrackerServiceImpl(FieldRepository fr, WellRepository wr) {
        this.fieldRepository = fr;
        this.wellRepository=wr;
    }

    @Override
    @Transactional
    public void saveField(Field field) throws DataAccessException {
        fieldRepository.saveField(field);
    }
    
    @Override
    @Transactional
    public List<Field> listFields(){
    	return fieldRepository.queryAll();
    }
    
    @Override
    @Transactional
    public List<Well> listWells(){
    	return wellRepository.queryAll();
    }
    
    
}
