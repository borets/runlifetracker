package ru.shaposhnik.runlifetracker.repository;

import java.util.List;
import org.springframework.dao.DataAccessException;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.model.User;

public interface FieldRepository {

	List<Field> queryAll();
	
	List<Field> query4User(User user);
	
    Field findFieldById(int id) throws DataAccessException;

    void saveField(Field field);
    
    Field findFieldByCountryAndName(String country, String name);
}
