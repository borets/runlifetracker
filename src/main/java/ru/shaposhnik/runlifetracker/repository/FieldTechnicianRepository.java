package ru.shaposhnik.runlifetracker.repository;

import java.util.List;
import org.springframework.dao.DataAccessException;
import ru.shaposhnik.runlifetracker.model.FieldTechnician;;


public interface FieldTechnicianRepository {

	FieldTechnician findFieldTechnicianById(int id) throws DataAccessException;

    void saveFieldTechnician(FieldTechnician technician);
    
	List<FieldTechnician> queryAll();
    
    List<FieldTechnician> query4Country(String country);

}
