
package ru.shaposhnik.runlifetracker.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ru.shaposhnik.runlifetracker.model.Well;
import ru.shaposhnik.runlifetracker.repository.WellRepository;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.UserRolesEnum;
import ru.shaposhnik.runlifetracker.exceptions.UnauthorizedException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class JpaWellRepositoryImpl implements WellRepository {

    @PersistenceContext
    private EntityManager em;


    @Override
    public List<Well> queryAll() {
        Query query = em.createQuery("SELECT t FROM Well t ORDER BY t.field.country, t.field.name, t.prefix, t.number asc");
        @SuppressWarnings("unchecked")
		List<Well> result = query.getResultList();
        return result;
    }
    
    @Override
    public List<Well> query4User(User user) {
    	if (user.getRole() == UserRolesEnum.ADMIN){
    		List<Well> result=queryAll();
    		return result;
    	} else {
    		Query query = em.createQuery("SELECT t FROM Well t WHERE t.field IN (:fields) ORDER BY t.field.country, t.field.name, t.prefix, t.number asc");
    		query.setParameter("fields", user.getFields());
    		@SuppressWarnings("unchecked")
			List<Well> result = query.getResultList();
    		return result;
    	}
    }
    
    
    @Override
    public Well findWellById(int id) {
        return this.em.find(Well.class, id);
    }

    @Override
    @Transactional
    public void saveWell(Well well, User user) throws UnauthorizedException {

    	
    	switch (user.getRole()){
    		case ADMIN :
    			this.em.merge(well);
    			break;
    		case APPING :
    			if (!user.getFields().contains(well.getField())) {
    				System.out.println("User does not have access to the field:"+well.getField().getName());
    				throw new UnauthorizedException("");
    			}
    			if (well.getId()!=0 && !user.getFields().contains(em.find(Well.class, well.getId()).getField())){
    				System.out.println("User tries to overwrite the well from the field:"+em.find(Well.class, well.getId()).getField().getName());
    				throw new UnauthorizedException("");
    			} 
    			this.em.merge(well);
    			break;
    		default:
    			throw new UnauthorizedException("");
    	}
    	
    	
    } 

}
