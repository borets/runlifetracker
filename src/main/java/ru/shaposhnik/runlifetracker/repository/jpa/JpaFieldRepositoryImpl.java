/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.repository.jpa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.repository.FieldRepository;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.UserRolesEnum;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class JpaFieldRepositoryImpl implements FieldRepository {

    @PersistenceContext
    private EntityManager em;


    @Override
    public List<Field> queryAll() {
        Query query = em.createQuery("SELECT t FROM Field t");
        @SuppressWarnings("unchecked")
		List<Field> result = query.getResultList();
        return result;
    }
    
    @Override
    public List<Field> query4User(User user) {
    	if (user.getRole() == UserRolesEnum.ADMIN){
    		List<Field> result=queryAll();
    		return result;
    	} else {
    		Query query = em.createQuery("SELECT u.fields FROM User u WHERE u=:user");
    		query.setParameter("user", user);
    		@SuppressWarnings("unchecked")
			List<Field> result = query.getResultList();
    		return result;
    	} 
    }
    
    
    @Override
    public Field findFieldById(int id) {
        return this.em.find(Field.class, id);
    }

    @Override
    @Transactional
    public void saveField(Field field) {
       this.em.merge(field);
    } 

    public Field findFieldByCountryAndName(String country, String name){

    	Query query=em.createQuery("SELECT f from Field f WHERE f.name=:name AND f.country=:country");
    	query=query.setParameter("name", name);
    	query.setParameter("country", country);
    	
    	try {
    		Field result=(Field) query.getSingleResult();
    		return result;
    	} catch (NoResultException e) {
    		return null;
    	}
    	
   	
    }
    
}
