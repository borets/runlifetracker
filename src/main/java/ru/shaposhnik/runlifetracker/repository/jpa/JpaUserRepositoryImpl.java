/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.repository.UserRepository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.userdetails.UserDetailsService;

@Repository
public class JpaUserRepositoryImpl implements UserRepository,UserDetailsService {

    @PersistenceContext
    private EntityManager em;


    @Override
    public List<User> queryAll() {
        Query query = em.createQuery("SELECT t FROM User t");
        @SuppressWarnings("unchecked")
		List<User> result = query.getResultList();
        return result;
    }
    
    
    @Override
    public User findUserById(int id) {
        return this.em.find(User.class, id);
    }

    @Override
    @Transactional
    public void saveUser(User user) {
       this.em.merge(user);
    } 

    public List<Field> findFields4User(User user){
    	Query query = em.createQuery("SELECT f FROM Field f WHERE");
    	@SuppressWarnings("unchecked")
		List<Field> result = query.getResultList();
        return result;
    	
    }
    
    // interface UserDetailsService implementation 
    @Override
    public User loadUserByUsername(String username){
    	Query query = em.createQuery("SELECT u FROM User u WHERE u.email=:email");
    	query.setParameter("email", username);
    	@SuppressWarnings("unchecked")
		List<User> result = query.getResultList();
    	System.out.println("LoadUserByUsername request: "+username);
        return result.iterator().next();  	
    }
    
    public User findUserByEmail(String email){
    	Query query=em.createQuery("SELECT u from User u WHERE u.email=:email");
    	query.setParameter("email", email);
    	try {
    		User result= (User) query.getSingleResult();
    		return result;
    	} catch (NoResultException e) {
    		return null;
    	}
    	 	
    }
}
