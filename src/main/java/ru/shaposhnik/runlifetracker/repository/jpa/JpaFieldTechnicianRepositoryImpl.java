/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import ru.shaposhnik.runlifetracker.model.FieldTechnician;
import ru.shaposhnik.runlifetracker.repository.FieldTechnicianRepository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class JpaFieldTechnicianRepositoryImpl implements FieldTechnicianRepository{

    @PersistenceContext
    private EntityManager em;


    @Override
    public List<FieldTechnician> queryAll() {
        Query query = em.createQuery("SELECT t FROM FieldTechnician t ORDER BY t.country ASC, t.surnames ASC");
        @SuppressWarnings("unchecked")
		List<FieldTechnician> result = query.getResultList();
        return result;
    }
    
    
    @Override
    public FieldTechnician findFieldTechnicianById(int id) {
        return this.em.find(FieldTechnician.class, id);
    }

    @Override
    @Transactional
    public void saveFieldTechnician(FieldTechnician technician) {
       this.em.merge(technician);
    } 

    public List<FieldTechnician> query4Country(String country){
    	Query query = em.createQuery("SELECT t FROM FieldTechnician t WHERE t.country=:country AND t.isActive = true");
    	query.setParameter("country", country);
    	@SuppressWarnings("unchecked")
		List<FieldTechnician> result = query.getResultList();
        return result;
    	
    }


}
