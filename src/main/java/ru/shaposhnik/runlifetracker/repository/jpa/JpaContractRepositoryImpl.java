/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ru.shaposhnik.runlifetracker.model.Contract;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.UserRolesEnum;
import ru.shaposhnik.runlifetracker.repository.ContractRepository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class JpaContractRepositoryImpl implements ContractRepository {

    @PersistenceContext
    private EntityManager em;


    @SuppressWarnings("unchecked")
	@Override
    public List<Contract> queryAll() {
        Query query = em.createQuery("SELECT c FROM Contract c");
        List<Contract> result = query.getResultList();
        return result;
    }

    // It seems that there is an error: field argument is not used!!!
    @SuppressWarnings("unchecked")
    @Override
    public List<Contract> query4Field(Field field) {
        Query query = em.createQuery("SELECT c FROM Contract c");
  //      query.setParameter("field", field);
		List<Contract> result = query.getResultList();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Contract> query4User(User user) {
    	
    	Query query;
    	if(user.getRole()==UserRolesEnum.ADMIN)
    		query=em.createQuery("SELECT c from Contract c");
    	else {
    		query = em.createQuery("SELECT DISTINCT c FROM Contract c, Field f WHERE c MEMBER OF f.contracts "+
        								"AND EXISTS (SELECT 'HasRights' FROM User u WHERE u=:user AND f MEMBER OF u.fields)");
    		query.setParameter("user", user);
    	};
		List<Contract> result = query.getResultList();
        return result;
    }
    
    
    
    @Override
    public Contract findContractById(int id) {
        return this.em.find(Contract.class, id);
    }

    @Override
    @Transactional
    public void saveContract(Contract contract) {
       this.em.merge(contract);
    } 

}
