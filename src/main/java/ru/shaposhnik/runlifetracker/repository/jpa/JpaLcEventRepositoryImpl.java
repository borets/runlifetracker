/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shaposhnik.runlifetracker.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;
import ru.shaposhnik.runlifetracker.exceptions.UnauthorizedException;

import ru.shaposhnik.runlifetracker.model.LcEvent;

import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.repository.LcEventRepository;
import ru.shaposhnik.runlifetracker.model.Installation;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class JpaLcEventRepositoryImpl implements LcEventRepository {

    @PersistenceContext
    private EntityManager em;


        
    @Override
    public List<LcEvent> findLcEvent4Installation(Installation installation, User user) {
    	Query query = em.createQuery("SELECT t FROM LcEvent t WHERE t.installation="+installation.getId());
        @SuppressWarnings("unchecked")
		List<LcEvent> result = query.getResultList();
        return result;
    }
    
    @Override
    public LcEvent findLcEventById(Long id, User user) {
        return this.em.find(LcEvent.class, id);
    }
    
        
    @Override
    @Transactional
    public LcEvent saveLcEvent(LcEvent lcEvent, User user) throws UnauthorizedException, LogicValidationException {

    	if (!user.hasRightOnfield(lcEvent.getInstallation().getWell().getField()) ||
    			!lcEvent.getRoles().contains(user.getRole()))
    		throw new UnauthorizedException("Access rights violation");
    	    	
    	lcEvent.checkEvent();
    	
    	if (lcEvent.getId()==0){
    		lcEvent.setCreatedBy(user.getEmail());
    	}
    	lcEvent.setUpdatedBy(user.getEmail());
    	return this.em.merge(lcEvent);
    	
    } 

    @Override
    @Transactional
    public void deleteLcEvent(LcEvent lcEvent, User user) throws UnauthorizedException,LogicValidationException {
    		
    	if (!user.hasRightOnfield(lcEvent.getInstallation().getWell().getField()) ||
    			!lcEvent.getRoles().contains(user.getRole()))
    		throw new UnauthorizedException("Access rights violation");
    	
    	
    	lcEvent.checkDelete();
       	this.em.remove(em.contains(lcEvent) ? lcEvent : em.merge(lcEvent));	
  
    } 
    
}