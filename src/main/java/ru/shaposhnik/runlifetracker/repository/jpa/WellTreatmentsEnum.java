/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package ru.shaposhnik.runlifetracker.model;


public enum WellTreatmentsEnum {

	NODATA("No data",true),
	AFTERDRILLING("After drilling",true),
	CONVERTED("Converted from other methods", true),
	AFTERACID("After acidizing",true),
	AFTERFRAC("After fracturing",true),
	AFTEROTHER("After other well treatment",true),
	AFTERFAILURE("Equipment replacement-after failure",true),
	OTHER("Equipment replacement-other reason",true);


	private String label;
	private boolean enabled;

	private WellTreatmentsEnum(String l, boolean b) {
		this.label = l;
		this.enabled=b;
	}

	
	public String getLabel(){
		return label;
	}
	
	public boolean getEnabled(){
		return enabled;
	}
	
	
}