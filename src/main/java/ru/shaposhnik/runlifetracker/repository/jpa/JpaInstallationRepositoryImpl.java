
package ru.shaposhnik.runlifetracker.repository.jpa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ru.shaposhnik.runlifetracker.model.Contract;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.model.Installation;
import ru.shaposhnik.runlifetracker.model.LcEvent;
import ru.shaposhnik.runlifetracker.model.SavedFile;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.UserRolesEnum;
import ru.shaposhnik.runlifetracker.model.Well;
import ru.shaposhnik.runlifetracker.repository.InstallationRepository;
import ru.shaposhnik.runlifetracker.exceptions.UnauthorizedException;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class JpaInstallationRepositoryImpl implements InstallationRepository {

    @PersistenceContext
    private EntityManager em;


    @Override
    public List<Installation> queryAll(User user) {
    	Query query;
    	if(user.getRole()==UserRolesEnum.ADMIN)
    		query = em.createQuery("SELECT t FROM Installation t ORDER BY t.instDate ASC");
    	else {
    		query = em.createQuery("SELECT t FROM Installation t WHERE t.well.field IN :fieldlist ORDER BY t.instDate ASC");
    		query.setParameter("fieldlist",user.getFields());
    	}
        @SuppressWarnings("unchecked")
		List<Installation> result = query.getResultList();
        return result;
    }
    
    public List<Installation> queryAll(Well well, User user) throws DataAccessException{
		Query query;
		if(user.getRole()==UserRolesEnum.ADMIN)
			query = em.createQuery("SELECT t FROM Installation t WHERE t.well=:well ORDER BY t.instDate ASC");
		else {
			query = em.createQuery("SELECT t FROM Installation t WHERE t.well=:well AND t.well.field IN :fieldlist ORDER BY t.instDate ASC");
			query.setParameter("fieldlist", user.getFields());
		}
		query.setParameter("well",well);
		@SuppressWarnings("unchecked")
		List<Installation> result = query.getResultList();
        return result;    	
    }
    
 public List<Installation> queryAll(Contract contract, User user) throws DataAccessException{
	
	 	Query query;
	 	if(user.getRole()==UserRolesEnum.ADMIN){
	 		query = em.createQuery("SELECT t FROM Installation t WHERE t.contract=:contract ORDER BY t.instDate ASC");
	 	} else {
	 		query = em.createQuery("SELECT t FROM Installation t WHERE t.contract=:contract AND t.well.field IN :fieldlist ORDER BY t.instDate ASC");
	 		query.setParameter("fieldlist",user.getFields());
	 	}
	 
    	query.setParameter("contract",contract);
        @SuppressWarnings("unchecked")
		List<Installation> result = query.getResultList();
        return result;    	
    }

    public List<String> availableCountries (User user) throws DataAccessException {
    	Query query;
    	if(user.getRole()==UserRolesEnum.ADMIN){
    		query = em.createQuery("SELECT DISTINCT t.contract.client.country FROM Installation t");
    	} else {
    		query = em.createQuery("SELECT DISTINCT t.contract.client.country FROM Installation t WHERE t.well.field IN :fieldlist");
    		query.setParameter("fieldlist",user.getFields());
    	}
    	@SuppressWarnings("unchecked")
		List<String> result = query.getResultList();
        return result;
    }
    
    
    public List<Installation> queryCountry(String country, User user) throws DataAccessException{
    	Query query;
    	if(user.getRole()==UserRolesEnum.ADMIN){
    		query = em.createQuery("SELECT t FROM Installation t WHERE t.contract.client.country=:country ORDER BY t.instDate ASC");
    	} else {
    		query = em.createQuery("SELECT t FROM Installation t WHERE t.contract.client.country=:country AND t.well.field IN :fieldlist ORDER BY t.instDate ASC");
    		query.setParameter("fieldlist",user.getFields());
    	}
    	query.setParameter("country",country);
        @SuppressWarnings("unchecked")
		List<Installation> result = query.getResultList();
        return result;    	
    }
    
    
    @Override
    public Installation findInstallationById(int id) {
        return this.em.find(Installation.class, id);
    }
    
    @Override
    public Installation findInstallationById(User user, int id) 
    	throws UnauthorizedException {
        
    	Installation installation=this.em.find(Installation.class, id);
    	if (user.hasRightOnfield(installation.getWell().getField())){
    		return installation;
    	}
    	
    	throw new UnauthorizedException("User does not have right to see this installation");
    }
    
    

    @Override
    @Transactional
    public Installation saveInstallation(Installation installation) {
       return this.em.merge(installation);   
    } 
    
    @Override
    @Transactional
    public void deleteInstallation(Installation installation, User user ) 
    		throws UnauthorizedException, DataAccessException {
    	
    	if (!user.hasRightOnfield(installation.getWell().getField()) ||
				(user.getRole()!=UserRolesEnum.ADMIN && user.getRole()!=UserRolesEnum.APPING)){
	
    		throw new UnauthorizedException("User does not have rights to change installations from this field!");
    	}
    	
    	this.em.remove(findInstallationById(installation.getId()));
    	
    }
    
    
    
    @Override
    @Transactional
    public Installation saveInstallation(Installation installation, User user) throws UnauthorizedException {
       
    	if (installation.getId()!=0 && findInstallationById(installation.getId()).getWell().getId()!=installation.getWell().getId()){
    		System.out.println("It is forbidden to change the well for the existing installation");
    		throw new UnauthorizedException("It is forbidden to change the well for the existing installation");
    	}
    	
    	if (!user.hasRightOnfield(installation.getWell().getField()) ||
    				(user.getRole()!=UserRolesEnum.ADMIN && user.getRole()!=UserRolesEnum.APPING)){
    	
    		throw new UnauthorizedException("User does not have rights to change installations from this field!");
    	}
    	
    	installation.setUpdatedBy(user.getUsername());
    	if(installation.getId() == 0){
    		installation.setCreatedBy(user.getUsername());
    	} else {
    		installation.setCreatedDate(this.findInstallationById(user, installation.getId()).getCreatedDate());
    		installation.setCreatedBy(this.findInstallationById(user, installation.getId()).getCreatedBy());
    	}
    	return this.em.merge(installation);
    } 
    
    
    public List<Installation> querySelection(Contract contract, Field field, Class<? extends LcEvent> eventType, Date dateFrom, Date dateTo, User user)
    		throws DataAccessException {
    	    	
    	Query query;
    	String whereClause="";
    	if(field != null)
    		whereClause=whereClause+" AND w.field=:field";
    	if(contract != null)
    		whereClause=whereClause+" AND i.contract=:contract";

    	if(user.getRole()==UserRolesEnum.ADMIN)
    		query = em.createQuery("SELECT i FROM Installation i INNER JOIN i.well w INNER JOIN i.lcEvents e WHERE TYPE(e)=:eventType AND e.eventDate>=:dateFrom AND e.eventDate<=:dateTo"+whereClause+" ORDER BY i.instDate ASC");
    	else {
    		query = em.createQuery("SELECT i FROM Installation i INNER JOIN i.well w INNER JOIN i.lcEvents e WHERE TYPE(e)=:eventType AND e.eventDate>=:dateFrom AND e.eventDate<=:dateTo AND i.well.field IN :fieldlist"+whereClause+" ORDER BY i.instDate ASC");
    		query.setParameter("fieldlist",user.getFields());
    	}	
    	query.setParameter("eventType",eventType);
    	  	
    	if(contract != null)    	
    		query.setParameter("contract",contract);
       	if(field != null)
       		query.setParameter("field", field);
    	query.setParameter("dateFrom",dateFrom);
    	query.setParameter("dateTo",dateTo);
        @SuppressWarnings("unchecked")
		List<Installation> result = query.getResultList();
        return result;
    	
    	
    }
    
    public List<Installation> queryWaiting4(Contract contract, Class<? extends LcEvent> eventType, Date laFecha, User user)
    		throws DataAccessException {
    	
    	List<Installation> result = new ArrayList<Installation>();
    	
    	List<Installation> installations = queryAll(contract, user);
    	
    	for (Installation i : installations){
    		if(i.getLcEvent(eventType) != null && i.getLcEvent(eventType).isWaitAt(laFecha))
    				result.add(i);
    	}
    		
        return result;
    }
    
    @Override
    @Transactional
    public SavedFile saveFile(SavedFile f) throws DataAccessException{
    	return this.em.merge(f); 
    }
    
    @Override
    public List<SavedFile> savedFiles(Installation i) throws DataAccessException{
    	
    	Query query = em.createQuery("SELECT f FROM SavedFile f WHERE f.installation=:installation");
    	
    	query.setParameter("installation",i);
        @SuppressWarnings("unchecked")
		List<SavedFile> result = query.getResultList();
        return result; 	
    }
    
    public SavedFile findFileById(int id){
        return this.em.find(SavedFile.class, id);
    }
    
    @Transactional
    public void deleteFileByInst(Installation inst){
    	
    	for (SavedFile file : savedFiles(inst)){
    		this.em.remove(findFileById(file.getId()));
    	}
    }

}
