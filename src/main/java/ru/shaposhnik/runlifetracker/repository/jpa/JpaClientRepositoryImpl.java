package ru.shaposhnik.runlifetracker.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ru.shaposhnik.runlifetracker.model.Client;
import ru.shaposhnik.runlifetracker.repository.ClientRepository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class JpaClientRepositoryImpl implements ClientRepository {

    @PersistenceContext
    private EntityManager em;


    @Override
    public List<Client> queryAll() {
        Query query = em.createQuery("SELECT c FROM Client c");
        @SuppressWarnings("unchecked")
		List<Client> result = query.getResultList();
        return result;
    }
    
    
    @Override
    public Client findClientById(int id) {
        return this.em.find(Client.class, id);
    }

    @Override
    @Transactional
    public void saveClient(Client client) {
       this.em.merge(client);
    } 

}
