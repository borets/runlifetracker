package ru.shaposhnik.runlifetracker.repository;

import java.util.List;
import org.springframework.dao.DataAccessException;
import ru.shaposhnik.runlifetracker.model.Contract;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.model.User;

public interface ContractRepository {

	List<Contract> queryAll();
	
	List<Contract> query4Field(Field field);
	
	List<Contract> query4User(User user);
	
    Contract findContractById(int id) throws DataAccessException;

    void saveContract(Contract contract);
    
}
