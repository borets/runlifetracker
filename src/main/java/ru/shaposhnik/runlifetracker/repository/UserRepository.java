package ru.shaposhnik.runlifetracker.repository;

import java.util.List;
import org.springframework.dao.DataAccessException;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.Field;

public interface UserRepository {

	List<User> queryAll();
	
    User findUserById(int id) throws DataAccessException;

    void saveUser(User user);
    
    List<Field> findFields4User(User user);

    User findUserByEmail(String email) throws DataAccessException;
}
