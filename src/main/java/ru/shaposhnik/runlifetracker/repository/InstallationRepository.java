package ru.shaposhnik.runlifetracker.repository;

import java.util.Date;
import java.util.List;
import org.springframework.dao.DataAccessException;
import ru.shaposhnik.runlifetracker.model.Contract;
import ru.shaposhnik.runlifetracker.model.Field;
import ru.shaposhnik.runlifetracker.model.Installation;
import ru.shaposhnik.runlifetracker.model.LcEvent;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.model.SavedFile;
import ru.shaposhnik.runlifetracker.model.Well;
import ru.shaposhnik.runlifetracker.exceptions.UnauthorizedException;

public interface InstallationRepository {

	List<Installation> queryAll(User user) throws DataAccessException;
	
	List<Installation> queryAll(Well well, User user) throws DataAccessException;
	
	List<Installation> queryCountry(String country, User user) throws DataAccessException;
	
	List<Installation> querySelection(Contract contract, Field field, Class<? extends LcEvent> eventtype, Date dateFrom, Date dateTo, User user) throws DataAccessException;
	
    Installation findInstallationById(int id)
    		throws DataAccessException;

    Installation findInstallationById(User user, int id) 
    		throws UnauthorizedException, DataAccessException;

    List<String> availableCountries (User user) throws DataAccessException;
    
    Installation saveInstallation(Installation installation)
    		throws DataAccessException;
    
    Installation saveInstallation(Installation installation, User user)
    		throws UnauthorizedException, DataAccessException;
    
    void deleteInstallation(Installation installation, User user ) 
    		throws UnauthorizedException, DataAccessException;
    
    List<Installation> queryWaiting4(Contract contract,  Class<? extends LcEvent> eventType, Date laFecha, User user)
    		throws DataAccessException;
    
    SavedFile saveFile(SavedFile f) throws DataAccessException;
    List<SavedFile> savedFiles(Installation i) throws DataAccessException;
    SavedFile findFileById(int id);
    void deleteFileByInst(Installation i);
}
