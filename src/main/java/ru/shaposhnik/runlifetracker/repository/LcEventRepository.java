package ru.shaposhnik.runlifetracker.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;
import ru.shaposhnik.runlifetracker.exceptions.LogicValidationException;
import ru.shaposhnik.runlifetracker.exceptions.UnauthorizedException;
import ru.shaposhnik.runlifetracker.model.LcEvent;
import ru.shaposhnik.runlifetracker.model.Installation;
import ru.shaposhnik.runlifetracker.model.User;

public interface LcEventRepository {

    LcEvent findLcEventById(Long id, User user) throws UnauthorizedException,DataAccessException;
    
    List<LcEvent> findLcEvent4Installation(Installation installation, User user) throws UnauthorizedException ;

    LcEvent saveLcEvent(LcEvent lcEvent, User user) throws UnauthorizedException,LogicValidationException;
    
    void deleteLcEvent(LcEvent lcEvent, User user) throws UnauthorizedException,LogicValidationException;
    
}
