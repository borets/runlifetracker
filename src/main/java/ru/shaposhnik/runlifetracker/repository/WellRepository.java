package ru.shaposhnik.runlifetracker.repository;

import java.util.List;
import org.springframework.dao.DataAccessException;
import ru.shaposhnik.runlifetracker.model.Well;
import ru.shaposhnik.runlifetracker.model.User;
import ru.shaposhnik.runlifetracker.exceptions.UnauthorizedException;

public interface WellRepository {

	List<Well> queryAll() throws DataAccessException;
	
	List<Well> query4User(User user) throws DataAccessException;
	
    Well findWellById(int id) throws DataAccessException;

    void saveWell(Well well, User user) throws DataAccessException, UnauthorizedException;

}
