package ru.shaposhnik.runlifetracker.repository;

import java.util.List;
import org.springframework.dao.DataAccessException;
import ru.shaposhnik.runlifetracker.model.Client;

public interface ClientRepository {

	List<Client> queryAll();
	
    Client findClientById(int id) throws DataAccessException;

    void saveClient(Client client);
    
}
