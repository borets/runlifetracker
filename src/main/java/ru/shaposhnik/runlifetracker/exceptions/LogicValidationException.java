package ru.shaposhnik.runlifetracker.exceptions;

public class LogicValidationException extends Exception {

	private static final long serialVersionUID = 1L;
		
    public LogicValidationException(String message) {
        super(message);
    }

    public LogicValidationException(String message, Throwable throwable) {
        super(message, throwable);
    }

}