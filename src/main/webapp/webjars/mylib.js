// This function prints csv in a table
function showRawCSV(url,tableEl){
    d3.csv(url,
            function(csv) {

                var table1=d3.select(tableEl);
                table1.selectAll("*").remove();

                var thead1=table1.append("thead"),
                    tbody1=table1.append("tbody");
                 
                var columns1=d3.keys(csv[0]);

                thead1.append("tr")
                        .selectAll("th")
                        .data(columns1)
                        .enter()
                        .append("th")
                        .text(function(column){return column;});

                var rows=tbody1.selectAll("tr").data(csv).enter().append("tr");
                 
                var cells=rows.selectAll("td").data(
                        function(row){
                            return columns1.map(
                                    function(column){
                                        return { column: column, value: row[column]};
                                    })
                        })
                        .enter().append("td").text(function(d){return d.value;});
            });
}

function drawPivotedTable(settings,csv){
         // Preparing data 
         var totals1=d3.nest().key(settings.getKeyPivoted)
                        .sortKeys(d3.ascending)
                        .rollup(function(d){return {val: d3.sum(d,settings.getValue)}})
                        .map(csv);
                                
         var totals2=d3.nest()
                        .key(settings.getKeyLevel1).sortKeys(d3.ascending)
                        .key(settings.getKeyPivoted)
                        .rollup(function(d){return {val: d3.sum(d,settings.getValue)}})
                        .map(csv);

         var data=d3.nest()
                        .key(settings.getKeyLevel1).sortKeys(d3.ascending)
                        .key(settings.getKeyLevel2).sortKeys(d3.ascending)
                        .key(settings.getKeyPivoted)
                        .rollup(function(d){return {val: d3.sum(d,settings.getValue)}})
                        .map(csv);
    
        // Preparing the 2nd level total data
        d3.map(data).forEach(
            function(key,value){
                if (d3.map(value).size()>1)
                    //No need to add total if there is only 1 line
                    value["Total,0"]=totals2[key];
            });
        // Preparing the 1st level total data
        if (d3.map(data).size()>1){
            var g={};
            g[",0"]=totals1;
            data["TOTAL,0"]=g;
        }
        var table=$(settings.el);

        table.empty();

        // Preparing caption
        var caption=document.createElement("caption");
        caption.innerHTML=settings.caption();
        table.append($(caption));
       
        // Preparing column headers
        table.append("<thead><tr></tr></thead>");
        var headerEl=table.find("thead > tr");
        headerEl.append("<th>"+settings.colNameLevel1+"</th>"); 
        headerEl.append("<th>"+settings.colNameLevel2+"</th>"); 
        d3.map(totals1).forEach(
            function(key,value){
                headerEl.append("<th style='text-align:center'>"+settings.colNamePivoted(key)+"</th>");
            }); 

        var tbodyEl=document.createElement("tbody");
        $(settings.el).append(tbodyEl);
        d3.map(data).forEach(
            function(rowKey1,rowValues1){
                var key1PassIndicator=true;
                d3.map(rowValues1).forEach(
                    function(rowKey2,rowValues2){
                        id1=rowKey1.split(",")[1];
                        id2=rowKey2.split(",")[1];
                        var rowEl=document.createElement("tr");
                        $(tbodyEl).append(rowEl);
                        // Creating the line titles cells
                        if (key1PassIndicator){
                            var cellEl=document.createElement("td");
                            cellEl.innerHTML=rowKey1.split(",")[0];
                            cellEl.setAttribute("rowspan",d3.map(rowValues1).size());
                            $(rowEl).append(cellEl);
                            key1PassIndicator=false;
                        }
                        cellEl=document.createElement("td");
                        cellEl.innerHTML=rowKey2.split(",")[0];
                        // Marking totals with classes
                        if (id1==0){
                            $(rowEl).attr("class","totals1");
                        } else if(id2==0){
                            $(rowEl).attr("class","totals2");
                        };
                        
                        $(rowEl).append(cellEl);
                          d3.map(totals1).forEach(
                                function(colKey,colValue){
                                    cellEl=document.createElement("td");
                                    if (typeof rowValues2[colKey] !== "undefined")
                                        cellEl.innerHTML=settings.cellValue(id1,id2,colKey,rowValues2[colKey].val);
                                        cellEl.setAttribute("style","text-align:center");
                                    $(rowEl).append(cellEl);
                                });
                });
            });

}

